#!/bin/sh
#prepare some variables we need later
date=`date +%d-%m-%Y`
time=`date +%H:%M:%S`

if [ -n "$1" ]
then
	cd $1
else
	cd ..
fi

#copy over codes (and make unique)
`cat /var/covide_files/codes.txt | sort | uniq | grep '.' > /tmp/covide_codes.txt`

for codes in `cat /tmp/covide_codes.txt`; do

	#check if lockfile exists. If so, bail out
	if [ -f "/var/storage/linux/covide_email_${codes}.pid" ]; then
		echo "covide_email_${codes}.pid already found! skipping..."
	else
		#create lockfile
		touch /var/storage/linux/covide_email_${codes}.pid

		url=`cat /var/covide_files/${codes}-url.txt | cut -d '/' -f 3`
		nice sudo -u fcgi /usr/bin/php5 index.php --host=${url} --cron_email

		rm /var/storage/linux/covide_email_${codes}.pid
	fi

done

