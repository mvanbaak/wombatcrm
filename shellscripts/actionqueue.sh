#!/bin/sh

wgetcommand="wget -q --connect-timeout=15 --read-timeout=300 --dns-timeout=30 --no-check-certificate -o /tmp/.wgetout -O /tmp/.actionqueudata"

cat /var/covide_files/codes.txt | sort | uniq | grep '.' > /tmp/covide_codes.txt

for codes in `cat /tmp/covide_codes.txt`; do
	adres=/var/covide_files/${codes}-base-url.txt
	echo "Processing queue for ${adres}"
	
	if [ -f ${adres} ]
	then
	        url=`cat ${adres}`"?mod=actionqueue&action=execute"
		${wgetcommand} ${url}
	fi
done

