#!/bin/sh
DIRECTORIES=`ls -d /var/covide_files/* | grep -v '.txt' | cut -d '/' -f 4`
for a in $DIRECTORIES
do
	if [ -d /var/covide_files/$a ]
	then
		omindex -p --db /var/storage/linux/xapian/data/$a --url /email /var/covide_files/$a/email/
		omindex -p --db /var/storage/linux/xapian/data/$a --url /maildata /var/covide_files/$a/maildata/
		omindex -p --db /var/storage/linux/xapian/data/$a --url /bestanden /var/covide_files/$a/bestanden/
	fi
done
