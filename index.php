<?php
/**
 * Covide Groupware-CRM
 *
 * Covide Groupware-CRM is the solutions for all groups off people
 * that want the most efficient way to work to together.
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @author Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @author Stephan vd Haar <svdhaar@users.sourceforge.net>
 * @copyright Copyright 2000-2009 Covide BV
 * @package Covide
 */
	// AVG bug.
	if ($_SERVER["HTTP_USER_AGENT"] == "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;1813)") {
		echo "Processing stopped. You are AVG 8.0 webscanner and are known to create dos-attack like patterns.";
		die();
	}
	/* set default timezone */
	$tz = @date_default_timezone_get();
	date_default_timezone_set($tz);

	/* handle some common php bugs */
	require("common/handle_php_bugs.php");
	handle_php_bugs();

	/* xml function classes */
	require("common/functions_xml.php");

	/* autoloader */
	require("common/autoload.php");

	/* little exception for cronjob fetching */
	if (is_null(Wombat_Util::arrayGet($_SERVER, 'SERVER_NAME', null)) && $argc > 0) {
		$host   = "";
		$action = "cron";
		$param  = array();
		$param["user_id"] = -1;

		foreach ($argv as $v) {
			$v = explode("=", $v);
			/* get hostname */
			switch ($v[0]) {
				case "--host":
					$host = $v[1];
					break;
				case "--convert":
					$param["convert"] = $v[1];
					$action = "convert_db";
					break;
				case "--password":
					$param["password"] = md5($v[1]);
					break;
				case "--no-output":
					$param["no-output"] = true;
					break;
				case "--cron_calendar":
					$action = "cron_calendar";
					break;
				case "--cron_email":
					$action = "cron_email";
					break;
				case "--cron_other":
					$action = "cron_other";
					break;
				case "--user":
					$param["user_id"] = $v[1];
					break;
				case "--funambol":
					if ($v[1] == "no")
						$param["funambol"] = "disabled";
					elseif ($v[1] == "single")
						$param["funambol"] = "single";
					else
						$param["funambol"] = "normal";
					break;
			}
		}
		if (!$host)
			die("Parameter --host=<hostname> not specified\n\n");

		$_SERVER["SERVER_NAME"] = $host;
		$_cron = 1;

		$_REQUEST["mod"]    = "user";
		$_REQUEST["action"] = $action;
		$_REQUEST["param"]  = $param;
	}

	/* scan for virusses */
	require("common/handle_virus_scan.php");
	handle_virus_scan();

	/* create covide class */
	if(!isset($skip_session_init))
		$skip_session_init = false;

	$covide =& new Covide($skip_session_init);

	/* if devmode is enabled, attach our own error handler */
	if ($covide->devmode == 1) {
		require_once("common/error_handler.php");
		set_error_handler("covide_error_handler");
	}

	$db =& $covide->db;
	if (Wombat_Util::arrayGet($_REQUEST, 'fixdb', 0) == 1) {
		fix_db();
		die();
	}

	/* detect if we need to run cms instead of covide crm */
	if(!isset($skip_run_module))
		$skip_run_module = false;
	if(!isset($skip_run_module_cms))
		$skip_run_module_cms = false;
	if(!isset($_cron))
		$_cron = false;

	if ($covide->license["has_cms"] && count($_POST)==0 && (!$_SERVER["QUERY_STRING"] || strpos($_SERVER["QUERY_STRING"], "gclid") !== false)
		&& !$skip_run_module && !$skip_run_module_cms && !$_cron)
		$run_cms_module = 1;

	/* send some headers */
	require("common/headers.php");

	/* apply compression */
	if (1 == 0 && $enable_gzip && !$_cron) {
		ob_start('ob_gzhandler');
	 	ob_implicit_flush(0);
	} else {
		ob_start();
		ob_implicit_flush(0);
	}
	/* if cms */
	if ($run_cms_module) {
		require("site.php");
		exit();
	}

	/* external file loader */
	if ($_REQUEST["load_external_file"]) {
		$covide->load_file($_REQUEST["load_external_file"]);

	} elseif (!$skip_run_module && !$skip_run_module_cms) {
		$covide->detect_mobile($mobile_agents);

		$covide->output_xhtml = 1;
		/*
		 * If the force_ssl license var is set that value will be sent to force_ssl.
		 * If not, level 2 will be used (which does offer SSL, with the
		 * box ticked by default.
		 * @see Covide constants starting with SSLMODE_ in classes/covide/default.php
		 */
		$covide->force_ssl(Wombat_Util::arrayGet($covide->license, 'force_ssl', Covide::SSLMODE_OFFERSSL));
		$covide->run_module(Wombat_Util::arrayGet($_REQUEST, 'mod', 'dashboard'));
		exit();
	}
?>
