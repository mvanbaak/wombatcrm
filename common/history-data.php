<?php
/**
 * Covide History module
 *
 * @author Tom Albers <toma@kovoks.nl>
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @copyright Copyright 2010 KovoKs BV
 * @package Covide
 */

session_start();
require("history.php");

// To clean the history.
//$_SESSION['history']='';

$history = new History();
#$history->setStorePoint( "Overzicht cal", "cal", "index.php?whatever=this3");
echo History::printStorePointsTable( $history->getStorePoints() );

echo "<br><br>Last one: ";
echo $history->getMostRecentStorePointUrl();
?>
