<?php
/**
 * Covide History module
 *
 * @author Tom Albers <kovoks@kovoks.nl>
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @copyright Copyright 2010 KovoKs BV
 * @package Covide
 */

/* 
 * Class that stores history points. In the code use:
 * @code
 * $h = new History();
 * $h->setStorePoint( "Nice description", "Module", "Url");
 * @endcode
 * You can retrieve all store points with getStorePoints() or
 * when you only need the url getMostRecentStorePointUrl().
*/
Class History {
	public $history = array();

	/* Constructor */
        public function __construct() {
		$this->history = $_SESSION['history'];
	}

	/* Sets a store point */
	public function setStorePoint( $desc, $module, $url='') {
		/* use current url is none is given. */
		if (empty($url) )
			$url = $_SERVER["REQUEST_URI"];

		/* remove old history items with same url. */
		if (!empty($this->history)) {
			foreach ($this->history as $date=>$item) {
				if ($item["url"] == $url) {
					unset($this->history[$date]);
				}
			}
		}

		/* add the new one. */
		$t = strval($this->getTimeStamp());
		$this->history[$t] = array(	"description" => $desc, 
						"module" => $module, 
						"url" => $url );
		$this->save();
	} 

	/* Returns an array with store points */
	public function getStorePoints() {
		return $this->history;
	}

	/* Returns the url of the most recent store point and tries
           not to give back the current page.*/
	public function getMostRecentStorePointUrl() {
		$currentpage = $_SERVER["REQUEST_URI"];
		$data = $this->getStorePoints();
		if ( $data == "" )
			return;

		krsort($data);
		$item = reset($data);
		while ($item["url"] == $currentpage)
			$item = next($data);
		return $item["url"];
	}

	/* Returns the js-code of the most recent store point and tries
           not to give back the current page.*/
	public function getMostRecentStorePointJs() {
		return sprintf("javascript: this.location.href='%s';" , 
				$this->getMostRecentStorePointUrl());
	}

	/* Returns the store points of a certain module */
	public function getStorePointsByModule( $module ) {
		$data = $this->getStorePoints();
		krsort($data);
		foreach ($data as $date=>$item)
			if ($item["module"] == $module)
				$data_new[$date] = $item;
		return $data_new;
	}

	private function save() {
		$_SESSION["history"] = $this->history;
	}
	
	static private function getTimeStamp() {
		return microtime(true);
	}
	
	static private function getTime( $timestamp ) {
		return intval($timestamp);
	}

	/* Prints a nice table, mostly for debugging purposes */
	static public function printStorePointsTable( $data ) {
		if (empty($data))
			return;
		krsort($data);
		echo "<table border=1 cellpadding=4>";
		echo "<tr><th>".gettext("Time")."</th>";
		echo "    <th>".gettext("Module")."</th>";
		echo "    <th>".gettext("Page")."</th>";
		echo "</tr>";
		foreach( $data as $date=>$item ) {
			echo "<tr>";
			echo "<td>".date("d-m-Y H:i:s", History::getTime( $date ))."</td>";
			echo "<td>".$item["module"]."</td>";
			echo "<td><a href=\"".$item["url"]."\" target=\"_new\">";
			echo "".$item["description"]."</a></td>";
			echo "</tr>";
		}
		echo "</table>";
	}
}
