<?php
/**
 * Some small helpers
 *
 * @author Tom Albers <kovoks@kovoks.nl>
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @copyright Copyright 2010 KovoKs BV
 * @package Covide
 */

/* Convert br's and p's to newlines, for example 
   if you switch from html to text in textareas */
function brp2nl ($str) {
    return preg_replace(array("/<p[^>]*>/iU","/<\/p[^>]*>/iU","/<br[^>]*>/"),
                        array("","\n","\n"),
                        $str);
}

