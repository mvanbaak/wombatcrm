<?php
/**
 * Covide Groupware-CRM Reeleezee_data
 *
 * Covide Groupware-CRM is the solutions for all groups off people
 * that want the most efficient way to work to together.
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @author Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright Copyright 2000-2010 Covide BV
 * @package Covide
 */
/**
 * Reeleezee_webservice class
 *
 * @author Michiel van Baak
 */
class Reeleezee_webservice extends SoapClient {

	private static $classmap = array(
		'Import' => 'Reeleezee_import',
		'ImportResponse' => 'Reeleezee_importResult',
	);

	public function __construct($wsdl = "reeleezee.wsdl", $options = array()) {
		$wsdl = dirname(__FILE__)."/".$wsdl;
		foreach(self::$classmap as $key => $value) {
			if(!isset($options['classmap'][$key])) {
				$options['classmap'][$key] = $value;
			}
		}
		parent::__construct($wsdl, $options);
	}
	/**
	 * @param Import $parameters
	 * @return ImportResponse
	 */
	public function Import(Reeleezee_import $parameters) {
		return $this->__soapCall('Import', array($parameters), array(
			'uri' => 'http://reeleezee.com/webservices/',
			'soapaction' => ''
			)
		);
	}
}
?>