<?php
/**
 * Covide Groupware-CRM
 *
 * Covide Groupware-CRM is the solutions for all groups off people
 * that want the most efficient way to work to together.
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @author Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright Copyright 2000-2010 Covide BV
 * @package Covide
 */
/**
 * Reeleezee import object
 * @author mvanbaak
 *
 */
 class Reeleezee_import {
	public $userName; // string
	public $password; // string
	public $xmlDocument; // base64Binary
	public $checkValidityOnly; // boolean
}
?>