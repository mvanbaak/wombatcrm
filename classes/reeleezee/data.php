<?php
/**
 * Covide Groupware-CRM Reeleezee_data
 *
 * Covide Groupware-CRM is the solutions for all groups off people
 * that want the most efficient way to work to together.
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @author Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright Copyright 2000-2010 Covide BV
 * @package Covide
 */
/**
 * Reeleezee data object
 * @author mvanbaak
 *
 */
class Reeleezee_data {
	/**
	 * Return trailing XML data such as taxonomy version etc
	 *
	 * @return string Xml
	 */
	private function _generateXmlHeader() {
		$data = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
		$data .= "<Reeleezee version=\"1.12\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.reeleezee.nl/taxonomy/1.13/Reeleezee.xsd\" xmlns=\"http://www.reeleezee.nl/taxonomy/1.12\">\r\n";
		$data .= "\t<Import AutoIgnoreEnabled=\"true\" AutoTrimEnabled=\"true\">\r\n";
		$data .= "\t\t<ExportInfo>\r\n";
		$data .= "\t\t\t<Name>Covide export</Name>\r\n";
		$data .= "\t\t\t<Source>Reeleezee administratie Reeleezee Taxonomy</Source>\r\n";
		$data .= "\t\t\t<CreateDateTime>".date("c")."</CreateDateTime>\r\n";
		$data .= "\t\t</ExportInfo>\r\n";
		return $data;
	}
	/**
	 * Generate Customer/Vendor XML entry
	 * @param string $type 'customer' or 'vendor'
	 * @param array $address_record
	 * @param array $accountinfo Optional array with accountinfo
	 * @return string Xml
	 */
	private function _generateXmlNode($type, $address_record, $accountinfo = array()) {
		$data = "";
		if ($type == 'customer') {
			if (array_key_exists("customerID_format", $accountinfo) && strlen($accountinfo["customerID_format"]) > 0) {
				$customerID = sprintf($accountinfo["customerID_format"], $address_record["debtor_nr"]);
			} else {
				$customerID = $address_record["debtor_nr"];
			}
			$data .= "\t\t\t<Customer ID=\"".$customerID."\">\r\n";
		} else {
			if (array_key_exists("vendorID_format", $accountinfo) && strlen($accountinfo["vendorID_format"]) > 0) {
				$vendorID = sprintf($accountinfo["vendorID_format"], $address_record["debtor_nr"]);
			} else {
				$vendorID = $address_record["debtor_nr"];
			}
			$data .= "\t\t\t<Vendor ID=\"".$vendorID."\">\r\n";
		}
		$data .= "\t\t\t\t<FullName>".substr(trim(str_replace("&", "&amp;", $address_record["companyname"])), 0, 48)."</FullName>\r\n";
		$data .= "\t\t\t\t<SearchName>".substr(trim(str_replace("&", "&amp;", $address_record["companyname"])), 0, 48)."</SearchName>\r\n";
		$data .= "\t\t\t\t<Code xsi:nil=\"true\" />\r\n";
		if ($address_record["pobox"]) {
			$data .= "\t\t\t\t<DefaultAddress>Delivery</DefaultAddress>\r\n";
		} else {
			$data .= "\t\t\t\t<DefaultAddress>Office</DefaultAddress>\r\n";
		}
		$data .= "\t\t\t\t<LanguageCode>nl</LanguageCode>\r\n";
		if ($address_record["business_phone_nr"]) {
			$data .= "\t\t\t\t<PhoneNumber>".str_replace(".", "", trim($address_record["business_phone_nr"]))."</PhoneNumber>\r\n";
		} else {
			$data .= "\t\t\t\t<PhoneNumber xsi:nil=\"true\" />\r\n";
		}
		if ($address_record["business_fax_nr"]) {
			$data .= "\t\t\t\t<FaxNumber>".str_replace(".", "", trim($address_record["business_fax_nr"]))."</FaxNumber>\r\n";
		} else {
			$data .= "\t\t\t\t<FaxNumber xsi:nil=\"true\" />\r\n";
		}
		if ($address_record["business_email"]) {
			$data .= "\t\t\t\t<EmailAddress>".trim($address_record["business_email"])."</EmailAddress>\r\n";
		} else {
			$data .= "\t\t\t\t<EmailAddress xsi:nil=\"true\" />\r\n";
		}
		$data .= "\t\t\t\t<WebsiteAddress xsi:nil=\"true\" />\r\n";
		$data .= "\t\t\t\t<Comment xsi:nil=\"true\" />\r\n";
		if ($address_record["bankaccount"]) {
			$data .= "\t\t\t\t<BankAccountNumber>".trim($address_record["bankaccount"])."</BankAccountNumber>\r\n";
		} else {
			$data .= "\t\t\t\t<BankAccountNumber xsi:nil=\"true\" />\r\n";
		}
		$data .= "\t\t\t\t<Branch xsi:nil=\"true\" />\r\n";
		$data .= "\t\t\t\t<AddressList>\r\n";
		$data .= "\t\t\t\t\t<Address Type=\"Office\">\r\n";
		if ($address_record["business_address"]) {
			$data .= "\t\t\t\t\t\t<Street>".trim($address_record["business_address"])."</Street>\r\n";
		} else {
			$data .= "\t\t\t\t\t\t<Street xsi:nil=\"true\" />\r\n";
		}
		$data .= "\t\t\t\t\t\t<Number xsi:nil=\"true\"/>\r\n";
		if ($address_record["business_zipcode"]) {
			$data .= "\t\t\t\t\t\t<Zipcode>".trim($address_record["business_zipcode"])."</Zipcode>\r\n";
		} else {
			$data .= "\t\t\t\t\t\t<Zipcode xsi:nil=\"true\" />\r\n";
		}
		if ($address_record["business_city"]) {
			$data .= "\t\t\t\t\t\t<City>".trim($address_record["business_city"])."</City>\r\n";
		} else {
			$data .= "\t\t\t\t\t\t<City xsi:nil=\"true\" />\r\n";
		}
		$data .= "\t\t\t\t\t\t<CountryCode>NL</CountryCode>\r\n";
		$data .= "\t\t\t\t\t</Address>\r\n";
		if ($address_record["pobox"]) {
			$data .= "\t\t\t\t\t<Address Type=\"Delivery\">\r\n";
			$data .= "\t\t\t\t\t\t<Street>POSTBUS</Street>\r\n";
			$data .= "\t\t\t\t\t\t<Number>".$address_record["pobox"]."</Number>\r\n";
			if ($address_record["pobox_zipcode"]) {
				$data .= "\t\t\t\t\t\t<Zipcode>".trim($address_record["pobox_zipcode"])."</Zipcode>\r\n";
			} else {
				$data .= "\t\t\t\t\t\t<Zipcode xsi:nil=\"true\" />\r\n";
			}
			if ($address_record["pobox_city"]) {
				$data .= "\t\t\t\t\t\t<City>".trim($address_record["pobox_city"])."</City>\r\n";
			} else {
				$data .= "\t\t\t\t\t\t<City xsi:nil=\"true\" />\r\n";
			}
			$data .= "\t\t\t\t\t\t<CountryCode>NL</CountryCode>\r\n";
			$data .= "\t\t\t\t\t</Address>\r\n";
		} else {
			$data .= "\t\t\t\t\t<Address Type=\"Delivery\" xsi:nil=\"true\" />\r\n";
		}
		$data .= "\t\t\t\t</AddressList>\r\n";
		$data .= "\t\t\t\t<ContactPersonList />\r\n";
		if ($type == 'customer') {
			$data .= "\t\t\t</Customer>\r\n";
		} else {
			$data .= "\t\t\t</Vendor>\r\n";
		}
		return $data;
	}
	/**
	 * Return trailing XML data to close the XML document
	 *
	 * @return string Xml
	 */
	private function _generateXmlFooter() {
		$data = "\t</Import>\r\n";
		$data .= "</Reeleezee>\r\n";
		return $data;
	}
	/**
	 * Generate reeleezee taxonomy xml string
	 *
	 * @param array $address_records The relation info in an array
	 * @param array $accountinfo Optional array with accountinformation such as debtor_nr formatting
	 * @return string The xml data to use
	 */
	public function generateXml($address_records, $accountinfo = array()) {
		$data = $this->_generateXmlHeader();
		$data .= "\t\t<CustomerList>\r\n";
		foreach ($address_records as $address_record) {
			if ((int)$address_record["debtor_nr"]> 0 && $address_record["is_customer"] == 1) {
				$data .= $this->_generateXmlNode('customer', $address_record, $accountinfo);
			}
		}
		$data .= "\t\t</CustomerList>\r\n";
		$data .= "\t\t<VendorList>\r\n";
		foreach ($address_records as $address_record) {
			if ((int)$address_record["debtor_nr"] > 0 && $address_record["is_supplier"] == 1) {
				$data .= $this->_generateXmlNode('supplier', $address_record, $accountinfo);
			}
		}
		$data .= "\t\t</VendorList>\r\n";
		$data .= $this->_generateXmlFooter();
		return $data;
	}
	/**
	 * Get the reeleezee username and password from the database
	 * @return array
	 */
	public function getAccountInfo() {
		$sql = "SELECT * FROM reeleezee_account_info";
		$res = sql_query($sql);
		$row = sql_fetch_assoc($res);
		return $row;
	}
}
?>