<?php
/**
 * Covide Groupware-CRM Reeleezee_data
 *
 * Covide Groupware-CRM is the solutions for all groups off people
 * that want the most efficient way to work to together.
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @author Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright Copyright 2000-2010 Covide BV
 * @package Covide
 */
/**
 * Reeleezee import result object
 * @author mvanbaak
 *
 */
class Reeleezee_importResult {
	public $ImportResult; // base64Binary
}
?>