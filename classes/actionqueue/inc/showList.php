<?php
/**
 * Covide ActionQueue module
 *
 * @author Tom Albers <kovoks@kovoks.nl>
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @copyright Copyright 2010 Covide BV
 * @package Covide
 */

	if (!class_exists("ActionQueue")) {
		exit("no class definition found");
	}

	$actionqueue_data = new ActionQueue_data();

	/* start output handler */
	$output = new Layout_output();
	$output->layout_page(gettext("Action Queue"));

	$data = $actionqueue_data->getQueue( $_SESSION["user_id"] );

	$settings = array(
                "title"    => gettext("Queue"),
                "subtitle" => gettext("List of tasks that will be performed")
        );

	$venster = new Layout_venster($settings);
	$venster->addVensterData();

	/* New view, add the data */
	$view = new Layout_view();
        $view->addData($data);
	$view->addMapping(gettext("Actions"), "%%data_actions", "right");
	$view->addMapping(gettext("ID"), "%id");
	$view->addMapping(gettext("Minimum Time"), "%mintime_visible");
	$view->addMapping(gettext("Action"), "%action_visible");
	$view->addMapping(gettext("Current state"), "%state_visible");
	$view->addMapping(gettext("Remarks"), "%msg");
	$view->addMapping(gettext("Depends on"), "%dependson");

	$view->defineComplexMapping("data_actions", array(
		 array(
                        "type"  => "action",
                        "src"   => "delete",
                        "alt"   => gettext("delete"),
                        "link"  => array("javascript: deleteQueueItem('", "%id", "');")
                )
	));

	$venster->addCode( $view->generate_output() );
	$output->load_javascript(self::include_dir."showList.js");
	$output->addCode( $venster->generate_output() );
	$output->layout_page_end();
	$output->exit_buffer();
?>
