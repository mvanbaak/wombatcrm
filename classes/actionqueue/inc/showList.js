/**
 * Covide ActionQueue module
 *
 * @author Tom Albers <kovoks@kovoks.nl>
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @copyright Copyright 2010 Covide BV
 * @package Covide
 */

function deleteQueueItem(id) {
	var cf = confirm( gettext("Are you sure you want to delete the item from the queue, including all items that depend on this item?") );
        if (cf == true) {
		document.location.href = '?mod=actionqueue&action=delete&id='+id;
	}
}
