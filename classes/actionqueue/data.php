<?php
/**
 * Covide ActionQueue module
 *
 * @author Tom Albers <kovoks@kovoks.nl>
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @copyright Copyright 2010 Covide BV
 * @package Covide
 */

Class ActionQueue_data {

	/* constants */
	const include_dir = "classes/actionqueue/inc/";
	const class_name = "actionqueue_data";

	/**
	 * ActionQueue Item Action
	 */
	const ACTION_MAIL_SEND = 1;
	/**
	 * ActionQueue Item State
	 */
	const STATE_READY      = 1;
	const STATE_DONE       = 2;
	const STATE_DELETED    = 3;
	const STATE_PROCESSING = 4;

	/* methods */
	public function __construct() {
	}

	/**
         * Get a list of states
         *
         * @return array The states with numberic rep. as array key
         */
        public static function getStateArray() {
			$prior = array(
				self::STATE_READY      => gettext("Ready"),
				self::STATE_DONE       => gettext("Done"),
				self::STATE_DELETED    => gettext("Deleted"),
				self::STATE_PROCESSING => gettext("Processing")
			);
			return $prior;
        }

	/**
         * Get the user visible string of a state
         * @p state the numeric state
	 * @return the user visible string
         */
	public function getState( $state ) {
		$arr = $this->getStateArray();
		return $arr[ $state ];
	}

	/**
         * Get an array of all actions, user visible string
	 */
	public function getActionArray() {
		$actions = array(
			self::ACTION_MAIL_SEND => gettext("Sent mail")
		);
		return $actions;
	}

	/*
         * Get the user visible string of an action
         * @p action the numeric action
	 * @return the user visible string
	 */
	public function getAction( $action ) {
		$arr = $this->getActionArray();
		return $arr[ $action ];
	}

	/**
	 * Get the queue for a user
         */
	public function getQueue( $userid ) {
		$q = sprintf("select *, from_unixtime(mintime) as mintime_visible
			      from actionqueue where user_id = %d order by id asc", $userid);
                $res = sql_query($q);
                $att = array();
                while ($row = sql_fetch_assoc($res)) {
			$row["state_visible"] = $this->getState($row["state"]);
			$row["action_visible"] = $this->getAction($row["action"]);
			$att[] = $row;
		}
		return $att;
	}

	/**
    	 * Delete a job and the jobs that depend on that job.
         */
	public function delete( $id, $userid ) {
		if ($id > 0 && $userid > 0) {
			$q = sprintf("update actionqueue set state=%d where
					user_id = %d and dependson = %d", self::STATE_DELETED, $userid, $id);
			sql_query($q);
			$q = sprintf("update actionqueue set state=%d where
					user_id = %d and id = %d", self::STATE_DELETED, $userid, $id );
			sql_query($q);
		}
	}

	/**
	 * Adds a job to the queue.
	 */
	public function add( $userid, $action, $mintime, $data, $msg, $dependson=0 ) {
		$q = sprintf("insert into actionqueue VALUES ('', '%d', '%d', '%d', '%s', 1, '%s', '%d')",
				$userid, $action, $mintime, $data, $msg, $dependson);
		sql_query($q);
	}

	/**
	 * Executes all jobs in the queue.
	 */
	public function execute() {
		while ( $this->executeJob() ) { };
	}


	/* Private functions */


	/**
         * Executes the first job from the queue.
	 * Returns false when there was nothing to do.
         */
	private function executeJob() {
		$q = sprintf("select * from actionqueue where state=%d and mintime <= now()
			      order by id asc limit 1", self::STATE_READY);
		$res = sql_query($q);
		if (sql_num_rows($res)) {
                        $row = sql_fetch_assoc($res);
			echo "Executing job ".$row["id"];
			$this->changeState( $row["id"], self::STATE_PROCESSING );
			switch ($row["action"]) {
	                        case self::ACTION_MAIL_SEND : $result = $this->sendEmail($row);
	                                 break;
				default:
					 $result = "Unknown action";
			}

			if (!empty($result)) {
				$this->setError($row["id"],$result);
			} else {
				$this->setCompleted($row["id"]);
			}

			return true;

		} else {
			return false;
		}
	}

	private function setError( $id, $error ) {
		echo "Error " . $id." ".$error;
		if ($id > 0) {
			$this->changeState( $id, self::STATE_DELETED );
			$q = sprintf("update actionqueue set msg=concat(msg,' Deleted due to error: %s')
				 	where id=%d", $error, $id);
			sql_query($q);

			// delete childs
			$q = sprintf("update actionqueue set state=%d where dependson=%d", self::STATE_DELETED, $id);
			sql_query($q);
		}
	}

	private function setCompleted( $id ) {
		echo "Completed " . $id;
		if ($id > 0) {
			$this->changeState( $id, self::STATE_DONE );
			$q = sprintf("update actionqueue set msg=concat(msg,' Completed') where id=%d", $id);
			sql_query($q);

			// Set childs free
			$q = sprintf("update actionqueue set dependson=0 where dependson=%d", $id);
			sql_query($q);
		}
	}

	private function changeState( $id, $state ) {
		if ($id > 0 && $state > 0) {
			$q = sprintf("update actionqueue set state=%d where id=%d", $state, $id);
			sql_query($q);
		}
	}

	private function sendEmail( $job ) {
		$data = unserialize($job["data"]);
		$email_data = new Email_data();
		$res = $email_data->sendMailComplexQueue( $data["id"], $data["return"],
				$data["skip_sender_rewrite"], $data["skip_gmail"], $job["user_id"] );
		return $res;
	}
}
?>
