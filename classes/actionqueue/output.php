<?php
/**
 * Covide ActionQueue module
 *
 * @author Tom Albers <kovoks@kovoks.nl>
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @copyright Copyright 2010 Covide BV
 * @package Covide
 */

Class ActionQueue_output {

	/* constants */
	const include_dir = "classes/actionqueue/inc/";
	const include_dir_main = "classes/html/inc/";

	const class_name = "actionqueue";

	/* variables */
	private $output;

	/* methods */

	/* __construct */
	public function __construct() {
		$this->output="";
	}

	/* showList */
	public function showList() {
		require(self::include_dir."showList.php");
	}

}
