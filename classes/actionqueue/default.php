<?php
/**
 * Covide ActionQueue module
 *
 * @author Tom Albers <kovoks@kovoks.nl>
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @copyright Copyright 2010 Covide BV
 * @package Covide
 */

Class ActionQueue {

	/* constants */
	const include_dir = "classes/actionqueue/inc/";
	/* variables */

	/* methods */

    /**
     * 	__construct. Provides a framework in which actions can be queued.  
     *
     * Framework which allowes random actions to be exectud in a queue like shape.
     * It holds an actionitem, which can be:
     * 1) to sent an email placed in the Outbox.
     * It holds an state, which can be:
     * 1) Ready 2) Done 3) Deleted 4) Processing
     *
     */
	public function __construct() {

		switch ($_REQUEST["action"]) {

			case "show" :
				$actionqueue_output = new ActionQueue_output();
				$actionqueue_output->showList();
				break;
			case "delete" :
				$data = new ActionQueue_data();
				$data->delete( $_REQUEST["id"], $_SESSION["user_id"] );

                                $actionqueue_output = new ActionQueue_output();
                                $actionqueue_output->showList();
                                break;
			case "execute" :
				$data = new ActionQueue_data();
				$data->execute();
				break;
			default:
				$actionqueue_output = new ActionQueue_output();
				$actionqueue_output->showList();
				break;

		}

	}
}
?>
