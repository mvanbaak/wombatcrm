<?php
/**
 * Covide Finance module
 *
 * @author Stephan vd Haar <svdhaar@users.sourceforge.net>
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @copyright Copyright 2000-2007 Covide BV
 * @package Covide
 */

Class Finance {
	/* function __construct {{{  */
	public function __construct() {
			
		//Check if this user has access to this module
		$user_data = new User_data();
		$user_data->getUserdetailsById($_SESSION["user_id"]);
		if (!$user_data->checkAccess("finance")) {
			$output = new Layout_output();
			$output->addCode(gettext("Sorry, you have been denied access to this module."));
			echo $output->generate_output();
			exit();
		}

		$finance_output = new Finance_output();
		$finance_output->toonwelkom();
		break;
	}
	/* }}} */
}
