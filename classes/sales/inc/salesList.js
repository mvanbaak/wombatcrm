function blader(page) {
	document.getElementById('velden').start.value = page;
	document.getElementById('velden').submit();
}
function sales_delete(id) {
	document.getElementById('id').value = id;
	document.getElementById('velden').action.value = 'delete';
	document.getElementById('velden').submit();
}
function submitform() {
	document.getElementById('start').value = 0;
	document.getElementById('velden').submit();
}
function selectRel(id, str) {
	str += ' <a href="javascript: void(0);" onclick="javascript: removeRel();" title="remove contact">[X]</a>';
	document.getElementById('searchaddress_id').value = id;
	document.getElementById('layer_relation').innerHTML = str;
}
function removeRel() {
	document.getElementById('searchaddress_id').value = '';
	document.getElementById('layer_relation').innerHTML = 'none';
}
function selectProject(id, str) {
	str += ' <a href="javascript: void(0);" onclick="javascript: removeProject();" title="remove user">[X]</a>';
	document.getElementById('searchproject_id').value = id;
	document.getElementById('layer_projectname').innerHTML = str;
}
function removeProject() {
	document.getElementById('searchproject_id').value = '';
	document.getElementById('layer_projectname').innerHTML = 'none';
}
function pickProject() {
	var deb = document.getElementById('searchaddress_id').value;
	popup('?mod=project&action=searchProject&deb='+deb, 'searchproject')
}
function exportSale() {
	document.getElementById('export_field').value = '1';
	document.getElementById('velden').submit();
	document.getElementById('export_field').value = '0';
}
function exportSaleXML() {
	document.getElementById('export_xml').value = '1';
	document.getElementById('velden').submit();
	document.getElementById('export_xml').value = '0';
}
function download(id) {
	if (id!=0) {
		url = 'index.php?dl=1&mod=filesys&action=fdownload&id='+id;
		document.location.href=url;
	}
}
function show(div_name) {
	hidden = $('#hidden'+div_name).val();
	if ( hidden == 1 ) {
		/* if hidden display the div */
		$('#'+div_name).show();
		$('#hidden'+div_name).val(0);
		$('#hide_img_'+div_name).show();
		$('#show_img_'+div_name).hide();
	} else {
		$('#'+div_name).hide();
		$('#hidden'+div_name).val(1);
		$('#show_img_'+div_name).show();
		$('#hide_img_'+div_name).hide();
	}
	if ($('#search_user_id') != '') {
		var data = $('#searchform').serialize();
		$.ajax({
			type: 'POST',
			url: 'index.php',
			data: data,
			success: function(html){}
		});
	}
}
function sort_saleslist(salesperson_id, sort_column) {
	document.getElementById('sort_column'+salesperson_id).value = sort_column;
	var status = document.getElementById('sort_status'+salesperson_id).value;
	reload_salestable("scrolltable".salesperson_id, status, salesperson_id);
}
function reload_salestable(column_id, status, salesperson_id) {
	var options = document.getElementById('salesoptions').value;
	var sort = document.getElementById('sort_column'+salesperson_id).value;
	document.getElementById('sort_status'+salesperson_id).value = status;
	var ret = loadXMLContent('index.php?mod=sales&action=generate_sales_list&status='+status+'&salesperson_id='+salesperson_id+'&options='+options+'&reload_flag=1&sort='+sort);
	document.getElementById(column_id).innerHTML = '';
	document.getElementById(column_id).innerHTML = ret;
}

$("input[name^='yaxis']").click(function() {
	reload_barchart();
});

function reload_barchart() {
	var val = [];
	$("input[name^='yaxis']").each(function() {
		if (this.checked) {
			val.push(this.value);
		}
	});

	var yaxis = val.join(',');
	var xaxis = $("input[name^='xaxis']:checked").val();
	document.getElementById('searchxaxis').value = xaxis;
	document.getElementById('searchyaxis').value = yaxis;
	var options = document.getElementById('salesoptions').value;
	var status = document.getElementById('salesstatus').value;
	url = 'index.php?mod=sales&action=generate_bar_chart&xaxis='+xaxis+'&yaxis='+yaxis+'&options='+options+'&reload_flag=1';
	if (status!='') {
		if (status!='total') {
			url += '&status='+status;
		}
		display_totals('bar_chart');
	}
	var ret = loadXMLContent(url);
	document.getElementById('barchart_td').innerHTML = '';
	document.getElementById('barchart_td').innerHTML = ret;
	ddtabmenu.showsubmenu('general_tabs', ddtabmenu['general_tabs'+'-dselected']);
}

function display_totals(chart) {
	if (chart=='bar_chart') {
		var options = document.getElementById('salesoptions').value;
		var status = document.getElementById('salesstatus').value;
		var url = 'index.php?mod=sales&action=display_bar_totals&options='+options;
		if (status!='total') {
			url += '&status='+status;
		}
		var ret = loadXMLContent(url);
		document.getElementById('totals_div').innerHTML = '';
		document.getElementById('totals_div').innerHTML = ret;
	} else	if (chart=='pie_chart') {
		var pie_data = document.getElementById('pie_data').value;
		loadXML('index.php?mod=sales&action=display_pie_totals&chart_data='+pie_data);
	}
}

function display_salesmen_totals(salesperson_id) {
	var pie_data = document.getElementById('pie_data'+salesperson_id).value;
	var ret = loadXMLContent('index.php?mod=sales&action=display_salesmen_pie_totals&salesperson_id='+salesperson_id+'&chart_data='+pie_data);
	document.getElementById('totals_pie_div'+salesperson_id).innerHTML = ret;
}

function remove_search() {
	loadXML('index.php?mod=sales&action=delete_search');
	setTimeout('location.href= "index.php?mod=sales"', 1000);
}

function sales_info(id) {
	$('#inforight').css("width", "600px");
	url = 'index.php?mod=sales&action=show_info&id='+id;
	loadXML(url);
}

/* Functions used for todo */

function toonInfo(id) {
	loadXML('index.php?mod=todo&action=show_info&id='+id);
}

function contactInfo(id) {
	url = 'index.php?mod=address&action=relcard&id='+id;
	popup(url, 'contact', 0, 0, 1);
}

function todo_project(id) {
	url = 'index.php?mod=project&action=showinfo&master=0&id='+id;
	popup(url, 'todoproject', 0, 0, 1);
}

/* Functions used for todo */
