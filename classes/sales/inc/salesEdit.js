function sales_save() {
	document.getElementById('velden').action.value = 'save';
	document.getElementById('velden').submit();
}

function OLDselectRel(id, str) {
	document.getElementById('salesaddress_id').value = id;
	document.getElementById('layer_relation').innerHTML = str;
}
function selectProject(id, str) {
	document.getElementById('salesproject_id').value = id;
	document.getElementById('layer_projectname').innerHTML = str;
}
function pickProject() {
	var address_id = document.getElementById('salesaddress_id').value;
	popup('?mod=project&action=searchProject&actief=1&deb='+address_id, 'searchproject', 650, 500, 1);
}

function removeRel(id, ltarget, lspan) {
	var el_target = document.getElementById(ltarget);
	var el_span   = document.getElementById(lspan);

	var tg = el_target.value.replace(/^,/g,'').split(',');
	var sp = el_span.innerHTML.split(/<li/gi);

	for (i=0;i<tg.length;i++) {
		if (tg[i] == id) {
			tg.splice(i,1);
			if (navigator.appVersion.indexOf("MSIE")!=-1) {
				sp.splice(i,1);
			} else {
				sp.splice(i+1,1);
			}
		}
	}
	if (tg.count == 0) {
		el_span.innerHTML = '';
	} else {
		if (navigator.appVersion.indexOf("MSIE")!=-1) {
			el_span.innerHTML = '<LI' + sp.join('<LI');
		} else {
			el_span.innerHTML = sp.join('<LI');
		}
	}
	el_target.value = tg.join(',');
}

function selectRel(addressid, str) {
	/* retrieve hidden field and span contents */
	var el_address = document.getElementById('salesaddress_id');
	var el_span    = document.getElementById('searchrel');

	/* retrieve id's */
	var relations = el_address.value;
	relations = relations.replace(/\|/g, ',');

	/* sometimes the first element is empty */
	relations = relations.replace(/^,/g, '');

	/* split by comma */
	relations = relations.split(',');

	var list = el_span.innerHTML;

	var found = 0;
	for (i=0;i<relations.length;i++) {
		if (relations[i]==addressid) {
			found = 1;
		}
	}
	if (found==0) {
		/* add to array */
		relations[i] = addressid;
		list = list.concat("<li class='enabled'>");
		list = list.concat("<a href=\"javascript: removeRel('"+addressid+"', 'salesaddress_id', 'searchrel');\">", str, "</a>");
	}
	el_span.innerHTML = list;
	el_address.value = relations.join(',');
	rel_complete_initial = 0;
}

function add_attachment_covide(ids) {
	var ret = loadXMLContent('?mod=sales&action=upload_list&id=' + ids + '&var=' + attachment_popup);
	document.getElementById('sales' + attachment_popup + '_file').value = ids;
	document.getElementById(attachment_popup + '_file_div').innerHTML = ret;
	attachment_popup = "";
}

function attachment_delete( file_var ) {
	document.getElementById('sales' + file_var + '_file').value = 0;
	document.getElementById(file_var + '_file_div').innerHTML = '';
}

function download(id) {
	url = 'index.php?dl=1&mod=filesys&action=fdownload&id='+id;
	document.location.href=url;
}

function add_sales_todo() {
	var sales_id = document.getElementById('id').value;
	var action_day = document.getElementById('salestimestamp_action_day').value;
	var action_month = document.getElementById('salestimestamp_action_month').value;
	var action_year = document.getElementById('salestimestamp_action_year').value;
	if (action_day==0 || action_day==0 || action_day==0) {
		alert("Please enter the action date.");
	} else {
		popup('?mod=todo&action=edit_todo&sales_id='+sales_id+'&action_day='+action_day+'&action_month='+action_month+'&action_year='+action_year+'&hide=1');
	}
}

/* Functions used for todo */

function todo_delete(id, pop, xml) {
	url = 'index.php?mod=todo&action=delete_todo&todoid='+id;
	if (confirm(gettext("Are you sure you want to flag this todo as done?"))) {
		if (xml) {
			loadXML(url);
			document.location.href=document.location.href;
		} else {
			if (pop) {
				popup(url + '&hide=1');
			} else {
				document.location.href = url;
			}
		}
	}
}

function todo_edit(id, pop) {
	url = 'index.php?mod=todo&action=edit_todo&todoid='+id;
	if (pop) {
		popup(url + '&hide=1');
	} else {
		document.location.href = url;
	}
}

function todo_to_cal(id) {
	url = 'index.php?mod=calendar&action=edit&id=0&todoid='+id;
	popup(url, 'plantodo', 0, 0, 1);
}

function todo_to_reg(id) {
	url = 'index.php?mod=calendar&action=reg_input&todo_id='+id;
	popup(url, 'regtodo', 0, 0, 1);
}

function toonInfo(id) {
	loadXML('index.php?mod=todo&action=show_info&id='+id);
}

function contactInfo(id) {
	url = 'index.php?mod=address&action=relcard&id='+id;
	popup(url, 'contact', 0, 0, 1);
}

function todo_project(id) {
	url = 'index.php?mod=project&action=showinfo&master=0&id='+id;
	popup(url, 'todoproject', 0, 0, 1);
}

/* Functions used for todo */