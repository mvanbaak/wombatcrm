<?php
/**
 * Covide Groupware-CRM Sales data class
 *
 * Covide Groupware-CRM is the solutions for all groups off people
 * that want the most efficient way to work to together.
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @author Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @author Stephan vd Haar <svdhaar@users.sourceforge.net>
 * @copyright Copyright 2000-2009 Covide BV
 * @package Covide
 */
Class Sales_data {

	/* variables */
	private $pagesize = 20;
	private $_last_query = "";
	public $sales_status = Array();

	/* methods */

	public function __construct() {
		$this->pagesize = $GLOBALS["covide"]->pagesize;
		$this->sales_status = array(gettext("Pending"), gettext("Close to decision"), gettext("Closed - won"), gettext("Closed - lost"));
	}
	/* }}} */
	/* salesDelete{{{ */
	/**
	 * Delete the selected record from db
	 *
	 * @param int $id The id of sales item to be deleted
	 *
	 * @return void
	 */
	public function salesDelete($id) {
		$q = sprintf("delete from sales where id = %d", $id);
		sql_query($q);
	}

	public function getSalesBySearch($options="", $start=0) {
		$data = array();
		$start = (int)$start;

		$user_data = new User_data();
		$user_perm = $user_data->getUserdetailsById($_SESSION["user_id"]);
		$address = new Address_data();
		$project = new Project_data();

		$timestamp_fields = array(
			"action",
			"proposal",
			"order",
			"invoice",
			"prospect"
		);
		$status = $this->sales_status;

		/* prepare a basic permissions subquery if this user is not a global sales manager */
		if (!$user_perm["xs_salesmanage"] && !$user_perm["xs_limited_salesmanage"]) {
			$groups = $user_data->getUserGroups($_SESSION["user_id"]);
			if (count($groups) > 0) {
				$regex_syntax = sql_syntax("regex");
				$sq = " AND ( 1=0 ";
				foreach ($groups as $g) {
					$g = "G".$g;
					$sq.= " OR sales.users ".$regex_syntax." '(^|\\\\,)". $g."(\\\\,|$)' ";
				}
				$sq.= " OR sales.users ".$regex_syntax." '(^|\\\\,)". (int)$_SESSION["user_id"]."(\\\\,|$)' ";
				$sq.= sprintf(" OR sales.user_sales_id = %d ", $_SESSION["user_id"]);
				$sq.= ") ";
			}
		}

		$like = sql_syntax("like");

		if ($options["id"]) {
			$q_count = sprintf("select count(*) from sales where id = %d", $options["id"]);
			$q = sprintf("select sales.*, address.companyname from sales left join address ON address.id = sales.address_id where sales.id = %d", $options["id"]);
		} else {
			$q_count = "select count(*) from sales";
			$q_total = "select sum(total_sum) as total, sum(expected_score) as score from sales";
			$q = "";
			/* active/in_active */
			if ($options["in_active"]) {
				$q.= " where (sales.is_active IN (1, 0) or sales.is_active is null) ";
			} else {
				$q.= " where (sales.is_active = 1) ";
			}
			/* starttime and endtime */
			if ($options["starttime"] && $options["endtime"]) {
				$q.= sprintf(" and ( sales.timestamp_prospect between %1\$d and %2\$d 
								or sales.timestamp_proposal between %1\$d and %2\$d
								or sales.timestamp_order between %1\$d and %2\$d
								or sales.timestamp_invoice between %1\$d and %2\$d
								or sales.timestamp_action between %1\$d and %2\$d ) "
							, $options["starttime"], $options["endtime"]);
			}
			/* status */
			if ($options["status"]!="") {
				$q.= sprintf(" and sales.status = %d ", $options["status"]);
			}
			/* user_id */
			if ($options["user_id"]) {
				$q.= sprintf(" and user_sales_id = %d ", $options["user_id"]);
			}
			/* address_id */
			if ($options["address_id"]>0) {
				$regex_syntax = sql_syntax("regex");
				$q.= sprintf(" and (sales.address_id = %1\$d OR sales.multirel ".$regex_syntax." '(^|\\\\,)%1\$d(\\\\,|$)')", $options["address_id"]);
			}
			/* projectid */
			if ($options["project_id"]>0) {
				$q.= sprintf(" and sales.project_id = %d", $options["project_id"]);
			}
			/* classifications */
			if ($options["classification"]) {
				$regex_syntax = sql_syntax("regex");
				$regex_op = " and (";
				$classifications = explode("|", substr(substr($options["classification"], 1, strlen($options["classification"])), 0, -1));
				foreach ($classifications as $cla) {
					$q.= $regex_op." (sales.classification ".$regex_syntax." '(^|\\\\|)". $cla ."(\\\\||$)') ";
					$regex_op = "or";
				}
				$q.= ") ";
			}
			/* Add users permission query */
			if ($sq) {
				$q .= $sq;
			}

			/* text search */
			if ($options["text"]) {
				$q.= sprintf(" and (subject %1\$s '%%%2\$s%%' or sales.description %1\$s '%%%2\$s%%') ", $like, $options["text"]);
			}
			$q_count.=$q;

			$q = "select sales.*, address.companyname, users.username, project.name AS h_project from sales left join address ON address.id = sales.address_id left join users ON users.id = sales.user_sales_id left join project ON project.id = sales.project_id ".$q;

			if (!$options["sort"]) {
				$q.= " order by timestamp_action desc";
			} else {
				$q.= " order by ".sql_filter_col($options["sort"]);
			}
		}

		$res = sql_query($q_count);
		$data["count"] = sql_result($res,0);

		if ($options["no_limit"]) {
			$res = sql_query($q);
		} else {
			$res = sql_query($q, "", $start, $this->pagesize);
		}
		$this->_last_query = $q;

		while ($row = sql_fetch_assoc($res)) {
			if($row["project_id"] || $row["project_id"] != 0) {
				$row["has_project"] = 1;
				$row["project_name"] = $project->getProjectNameById($row["project_id"]);
			}
			if($row["address_id"] || $row["address_id"] != 0) { $row["has_address"] = 1; }
			$row["h_address"] = $row["companyname"];

			foreach ($timestamp_fields as $ts) {
				if ($row["timestamp_".$ts]) {
					$row["h_timestamp_".$ts] = date("d-m-Y", $row["timestamp_".$ts]);
				} else {
					$row["h_timestamp_".$ts] = "--";
				}
			}
			foreach ($status as $k => $v) {
				if ($row["status"] == $k) {
					$row["status_text"] = $v;
					break;
				}
			}
			/* put currency in normal notation */
			/* the en_US is different */
			/* TODO: this should be done a better way */
			$row["orig_sum"] = $row["total_sum"];
			if ($_SESSION["language"] != "en_US") {
				$row["total_sum"] = "&euro;&nbsp;".number_format($row["total_sum"],2,",",".");
			} else {
				$row["total_sum"] = "&euro;&nbsp;".number_format($row["total_sum"],2,".");
			}

			/* Handle if user can perform actions (such as edit or delete) */
			if (!$user_perm["xs_salesmanage"]) {
				$row["check_actions"] = 0;
				/* User has no rights, maybe users has assigned items */
				if ($_SESSION["user_id"] == $row["user_sales_id"]) {
					$row["check_actions"] = 1;
				}
			} else {
				/* User has global sales permssions always allow actions */
				$row["check_actions"] = 1;
			}

			/* Handle multirel data */
			if ($row["multirel"]) {
				$multirels = explode(",", $row["multirel"]);
				foreach ($multirels as $rel) {
					$multicompany[] = $address->getAddressNameById($rel);
				}
				$row["all_address_ids"] = $row["address_id"].','.$row["multirel"];
				$row["all_address_names"] = $row["companyname"].", ".implode(", ", $multicompany);
				/* Unsetting the company names, important */
				unset($multicompany);
			} else {
				$row["all_address_ids"] = $row["address_id"];
				$row["all_address_names"] = $row["companyname"];
			}

			$data["data"][] = $row;
		}
		return $data;
	}
	/* }}} */
	/* geTotals{{{ */
	/**
	 * Calculate and fetch the data for the current/selected salesperson
	 *
	 * @return array The scores and amount
	 */
	public function getTotals() {
		$totals = array();
		$q = $this->_last_query;
		if ($q) {
			$res = sql_query($q);
			while ($row = sql_fetch_assoc($res)) {
				$totals["score"][]   = $row["expected_score"];
				$totals["sum"][]     = $row["total_sum"];
				$totals["average"][] = ($row["total_sum"] * $row["expected_score"]/100);
				/* Sum price for items with status 'pending' */
				if ($row["status"]==0) {
					$totals["forecast"][] = ($row["total_sum"] * $row["expected_score"]/100);
				}
				/* Sum price for items with status 'closed-to-decision' */
				if ($row["status"]==1) {
					$totals["ctd"][] = ($row["total_sum"] * $row["expected_score"]/100);
				}
				/* Sum price for items with status 'closed-won' */
				if ($row["status"]==2) {
					$totals["realised"][] = $row["total_sum"];
				}
			}

			if ($totals) {
				$totals["score"]   = (array_sum($totals["score"]) / count($totals["score"]));
				$totals["sum"]     = array_sum($totals["sum"]);
				$totals["average"] = array_sum($totals["average"]);
				if(is_array($totals["forecast"])) {
					$totals["forecast"] = array_sum($totals["forecast"]);
				} else {
					$totals["forecast"] = 0;
				}
				if(is_array($totals["ctd"])) {
					$totals["forecast"] += array_sum($totals["ctd"]);
				}
				if(is_array($totals["realised"])) {
					$totals["realised"] = array_sum($totals["realised"]);
				} else {
					$totals["realised"] = 0;
				}

				$totals["items_total"] = $totals["forecast"] + $totals["realised"];
				$totals["average"]     = number_format($totals["average"],2,",",".");
				$totals["score"]       = (int)$totals["score"];
				$totals["sum"]         = number_format($totals["sum"],2,",",".");
				$totals["forecast"]    = number_format($totals["forecast"],2,",",".");
				$totals["realised"]    = number_format($totals["realised"],2,",",".");
				$totals["items_total"] = number_format($totals["items_total"],2,",",".");
			}
		}
		return $totals;
	}

	public function getSalesById($id, $address_id = 0, $project_id = 0) {
		if ($id) {
			return $this->getSalesBySearch(array("id"=>$id));
		} else {
			$array["data"][0]["is_active"]=1;
			$array["data"][0]["user_sales_id"] = $_SESSION["user_id"];
			$array["data"][0]["address_id"]    = $address_id;
			$array["data"][0]["project_id"]    = $project_id;
			return $array;
		}
	}
	/* }}} */
	/* saveItem{{{ */
	/**
	 * Store the data of a sales item into the db
	 *
	 * @return void
	 */
	public function saveItem() {
		$sales = $_REQUEST["sales"];
		$id    = $_REQUEST["id"];

		$timestamp_fields = array(
			"action",
			"proposal",
			"order",
			"invoice",
			"prospect"
		);

		/* get address_id's into an array */
		$addresses = explode(",", $sales["address_id"]);
		/* strip empty items */
		$addresses = array_unique($addresses);
		foreach ($addresses as $k=>$v) {
			if (!$v) {
				unset($addresses[$k]);
			}
		}
		sort($addresses);
		/* put first id we find in address_id. If more are found set multirel to the remaining values */
		$sales["address_id"] = $addresses[0];
		if (count($addresses) > 1) {
			unset($addresses[0]);
			$sales["multirel"] = implode(",", $addresses);
		}

		$sales["expected_score"] = (int)$sales["expected_score"];
		if ($sales["expected_score"] > 100)
			$sales["expected_score"] = 100;
		elseif ($sales["expected_score"] < 0)
			$sales["expected_score"] = 0;


		$fields["subject"]        = array("s",$sales["subject"]);
		$fields["description"]    = array("s",$sales["description"]);
		$fields["address_id"]     = array("d",$sales["address_id"]);
		$fields["project_id"]     = array("d",$sales["project_id"]);
		$fields["is_active"]      = array("d",$sales["is_active"]);
		$fields["user_sales_id"]  = array("d",$sales["user_sales_id"]);
		$fields["expected_score"] = array("d",$sales["expected_score"]);
		$fields["total_sum"]      = array("d",$sales["total_sum"]);
		$fields["classification"] = array("s",$sales["classification"]);
		$fields["users"]          = array("s",$sales["users"]);
		$fields["multirel"]       = array("s",$sales["multirel"]);
		$fields["status"]	     = array("d",$sales["status"]);
		$fields["proposal_file"]  = array("d",$sales["proposal_file"]);
		$fields["invoice_file"]   = array("d",$sales["invoice_file"]);

		foreach ($timestamp_fields as $ts) {
			if ($sales["timestamp_".$ts."_day"] && $sales["timestamp_".$ts."_month"] && $sales["timestamp_".$ts."_year"]) {
				$time = mktime(0,0,0, $sales["timestamp_".$ts."_month"], $sales["timestamp_".$ts."_day"], $sales["timestamp_".$ts."_year"]);
			} else {
				$time = 0;
			}
			$fields["timestamp_".$ts] = array("d", $time);
		}

		if (!$id) {
			$keys = array();
			$vals = array();
			foreach ($fields as $k=>$v) {
				$keys[] = $k;
				if ($v[0]=="s") {
					$vals[]="'".addslashes( $v[1] )."'";
				} else {
					$vals[]=(int)$v[1];
				}
			}
			$keys = implode(",",$keys);
			$vals = implode(",",$vals);

			$q = sprintf("insert into sales (%s) values (%s)", $keys, $vals);
			sql_query($q);
			$new_id = sql_insert_id("sales");
			$q = sprintf("update todo set sales_id = %d where user_id = %d and sales_id=0 and is_sales_todo=1", $new_id, $_SESSION["user_id"]);
			sql_query($q);
		} else {

			$vals = array();
			foreach ($fields as $k=>$v) {
				if ($v[0]=="s") {
					$vals[$k]="'".addslashes( $v[1] )."'";
				} else {
					$vals[$k]=(int)$v[1];
				}
			}
			$q = "update sales set user_id_modified = ".$_SESSION["user_id"];
			foreach ($vals as $k=>$v) {
				$q.= sprintf(", %s = %s ", $k, $v);
			}
			$q.= sprintf(" where id = %d", $id);
			sql_query($q);

		}
		$output = new Layout_output();
		$output->layout_page("sales", 1);
		$output->start_javascript();
		$output->addCode("
			if (opener && opener.document.getElementById('velden')) {
				opener.document.getElementById('velden').submit();
			} else {
				if (parent) {
					parent.document.location.href=parent.document.location.href;
				}
				closepopup();
			}
		");
		$output->end_javascript();
		$output->layout_page_end();
		$output->exit_buffer();
	}
	/* }}} */
	/* getAllSalesPerson {{{ */
	/**
	 * Fetch all the salesperson that fall under the current salesperson
	 *
	 * @param array $options To filter the result
	 *
	 * @return array the salesmen's ids
	 */
	public function getAllSalesPerson($options = array()) {
		$user_data = new User_data();
		$user_perm = $user_data->getUserdetailsById($_SESSION["user_id"]);

		$sql = "	SELECT DISTINCT users.id, users.username, IFNULL(X.tot_sales, 0) tot_sales FROM users
					LEFT JOIN sales ON users.id = sales.user_sales_id
					LEFT JOIN (SELECT COUNT(*) tot_sales, user_sales_id FROM sales WHERE is_active=1 ";
		if ($options["starttime"] && $options["endtime"]) {
			$sql.= sprintf(" and ( timestamp_prospect between %1\$d and %2\$d
								or timestamp_proposal between %1\$d and %2\$d
								or timestamp_order between %1\$d and %2\$d
								or timestamp_invoice between %1\$d and %2\$d
								or timestamp_action between %1\$d and %2\$d ) "
							, $options["starttime"], $options["endtime"]);
		}
		$sql .= "	GROUP BY user_sales_id) X ON sales.user_sales_id = X.user_sales_id
				WHERE users.is_active=1 ";

		if (!$options["user_id"]) {
			if ($user_perm["xs_salesmanage"] || $user_perm["xs_limited_salesmanage"]) {
				$sql .= " AND X.tot_sales > 0 ";
			}
		}

		if (!$user_perm["xs_salesmanage"] && !$user_perm["xs_limited_salesmanage"]) {
			$sql .= sprintf(" AND users.id = %d ", $_SESSION["user_id"]);
		} else if ($user_perm["xs_limited_salesmanage"] == 1 && !$options["generalinfo_flag"]) {
			$sql .= sprintf(" AND users.parent_sales_manager = %d OR users.id = %d ", $_SESSION["user_id"], $_SESSION["user_id"]);
		}

		/* user_id */
		if ($options["user_id"]) {
      $toUnset = array();
      $user_ids = explode(",", $options["user_id"]);
      for ($i = 0; $i < sizeof($user_ids); $i++) {
        if (empty($user_ids[$i])) {
          $toUnset[] = $i;
        }
      }
      foreach ($toUnset as $i) {
        unset($user_ids[$i]);
      }
      
			$sql.= sprintf(" and sales.user_sales_id IN ( %s ) ", implode(",",$user_ids));
		}
		//echo "<pre>".$sql."</pre>";
		$res = sql_query($sql);
		$people = array();
		while ($row = sql_fetch_assoc($res)) {
			$people[] = array( "id" => $row["id"], "name" => $row["username"], "total_sales" => $row["tot_sales"]);
		}
		return $people;
	}
	/* }}} */
	/* getMonths{{{ */
	/**
	 * Fetch all the months within the specified start and end dates
	 *
	 * @param int $startdate Timestamp of the from date
	 * @param int $enddate Timestamp of the till date
	 *
	 * @return array the months
	 */
	public function getMonths($startDate, $endDate) {
		$time1  = strtotime($startDate);
		$time2  = strtotime($endDate);
		$my     = date("mY", $time2);
		$months = array();

		$months[] = array( "title" => date("M y", $time1), "date" => strtotime(date("Y-m-1", $time1)));
		while($time1 < $time2) {
			$time1 = strtotime(date("Y-m-1", $time1)." +1 month");
			if(date("mY", $time1) != $my && ($time1 < $time2)) {
				$months[] = array( "title" => date("M y", $time1), "date" => strtotime(date("Y-m-1", $time1)));
			}
		}
		$months[] = array( "title" => date("M y", $time2), "date" => strtotime(date("Y-m-1", $time2)));
		$time1 = strtotime(date("Y-m-1", $time2)." +1 month");
		$months[] = array( "title" => date("M y", $time1), "date" => strtotime(date("Y-m-1", $time1)));
		return $months;
	}
	/* }}} */
	/* getQuarters{{{ */
	/**
	 * Fetch the quarters within the specified start and end dates
	 *
	 * @param int $startdate Timestamp of the from date
	 * @param int $enddate Timestamp of the till date
	 *
	 * @return array the quarters
	 */
	public function getQuarters($startDate, $endDate) {
		$q = array ( 0 => "Q1", 1 => "Q2", 2 => "Q3", 3 => "Q4" );
		$time1 = strtotime($startDate);
		$time2 = strtotime($endDate);
		$quarters = array();
		do {
			$month = date("m", $time1);
			$year2 = date("y", $time1);
			$year4 = date("Y", $time1);
			$qDate = strtotime($year4 . "-".(((int)(($month -1)/ 3) * 3) + 1)."-1");
			$quarters[] = array("title" => $q[(int)($month / 3)]. " ".$year2, "date" => $qDate);
			$time1 = strtotime(date("Y-m-1", $time1) . " +3 month");
		} while ($time1 < $time2);
		$month = date("m", $time1);
		$year2 = date("y", $time1);
		$year4 = date("Y", $time1);
		$qDate = strtotime($year4 ."-".(((int)(($month -1)/ 3) * 3) + 1)."-1");
		$quarters[] = array("title" => $q[(int)($month / 3)]." " .$year2, "date" => $qDate);
		return $quarters;
	}
	/* }}} */
	/* getBarChartData{{{ */
	/**
	 * Fetch data to display the bar chart under general information
	 *
	 * @param int   $xAxis The x-axis option selected by user
	 * @param int   $yAxis The y-axis option selected by user
	 * @param array $options To filter the result
	 *
	 * @return array The data for each column on the bar chart
	 */
	function getBarChartData($xAxis, $yAxis, $options = array(), &$highest_amount, &$xScale) {
		/* Get all sales people */
		$options["generalinfo_flag"] = 1;
		$people = $this->getAllSalesPerson($options);

		/* Determine X scale and compute where clause. */
		switch ($xAxis) {
			case 0:
				$salesName = $where = array();
				$count = count($people);

				for ($i=0; $i<$count; $i++) {
					$salesName[$i+1] = $people[$i]["name"];
					$where[] = "user_sales_id = ".$people[$i]["id"];
				}
				$xScale = implode('|', $salesName);
				break;
			case 1:
				$months = $this->getMonths(date("Y-m-d", $options["starttime"]), date("Y-m-d", $options["endtime"]));
			       $monthNames = $where = array();
			       $count = count($months);

				for ($i=0; $i<$count-1; $i++) {
					$monthNames[$i+1] = $months[$i]["title"];
					$where[] = sprintf("timestamp_prospect BETWEEN %d AND %d", $months[$i]["date"], $months[$i+1]["date"]);
				}
				$xScale = implode('|', $monthNames);
				break;
			case 2:
				$quarters = $this->getQuarters(date("Y-m-d",$options["starttime"]), date("Y-m-d",$options["endtime"]));
				$quarterNames = $where = array();
				$count = count($quarters);

				for ($i=0; $i<$count-1; $i++) {
				    $quarterNames[$i + 1] = $quarters[$i]["title"];
				    $where[] = sprintf("timestamp_prospect BETWEEN %d AND %d", $quarters[$i]["date"], $quarters[$i+1]["date"]);
				}
				$xScale = implode( '|', $quarterNames);
				break;
		}

		if ($_REQUEST["reload_flag"] == 1) {
			$yAxis = explode(",", $_REQUEST["yaxis"]);
		}
		if (count($yAxis) == 0) {
			$yAxis[0] = 0;
		}
		$count = count($yAxis);
		$yStr = "";
		$columns = array();

		for ($i = 0; $i < $count; $i++ ) {
			/* Define computation columns. */
			switch ($yAxis[$i]) {
				case 1:
					$columns[$i][0] = array("where" => "1", "column" => sprintf("IFNULL(SUM(total_sum), 0) AS column_%d", $i));
					break;
				case 2:
					$columns[$i][0] = array("where" => "status = 0", "column" => sprintf("IFNULL(SUM((total_sum * (expected_score/100))), 0) AS column_%d", $i), "total_flag" => 1);
					$columns[$i][1] = array("where" => "status = 1", "column" => sprintf("IFNULL(SUM((total_sum * (expected_score/100))), 0) AS column_%d", $i), "total_flag" => 1);
					break;
				case 3:
					$columns[$i][0] = array("where" => "status = 2", "column" => sprintf("IFNULL(SUM(total_sum), 0) AS column_%d", $i));
					break;
				case 4:
					$columns[$i][0] = array("where" => "status = 0", "column" => sprintf("IFNULL(SUM((total_sum * (expected_score/100))), 0) AS column_%d", $i), "total_flag" => 1);
					$columns[$i][1] = array("where" => "status = 1", "column" => sprintf("IFNULL(SUM((total_sum * (expected_score/100))), 0) AS column_%d", $i), "total_flag" => 1);
					$columns[$i][2] = array("where" => "status = 2", "column" => sprintf("IFNULL(SUM(total_sum), 0) AS column_%d", $i), "total_flag" => 1);
					break;
				case 0:
				default :
					$columns[$i][0] = array("where" => "1", "column" => sprintf("COUNT(*) AS column_%d", $i));
					break;
			}
		}

		/* Get data from db. */
		$values = $old_column = array();
		$i = 0;
		$like = sql_syntax("like");
		foreach ($where as $clause) {
			$total_val = "";
			foreach ($columns as $k => $v) {
				$total_sum = 0;
				for ($j=0; $j < count($v); $j++ ) {
					$sql = sprintf("SELECT %s FROM sales WHERE %s AND %s ", $v[$j]["column"], $v[$j]["where"], $clause);
					if ($options["starttime"] && $options["endtime"] && $xAxis==0) {
						$sql.= sprintf(" and (timestamp_prospect >= %1\$d AND timestamp_prospect <= %2\$d
								or timestamp_proposal >= %1\$d AND timestamp_proposal <= %2\$d
								or timestamp_order >= %1\$d AND timestamp_order <= %2\$d
								or timestamp_invoice >= %1\$d AND timestamp_invoice <= %2\$d
								or timestamp_action >= %1\$d AND timestamp_action <= %2\$d ) "
							, $options["starttime"], $options["endtime"]);
					}
					/* active/in_active */
					if ($options["in_active"]) {
						$sql .= " AND (sales.is_active IN (1, 0) or sales.is_active is null) ";
					} else {
						$sql .= " AND (sales.is_active = 1) ";
					}
					/* status */
					if ($options["status"]!="") {
						$sql .= sprintf(" AND status = %d ", $options["status"]);
					}
					/* user_id */
					 if ($options["salesman_id"]) {
						$sql .= sprintf(" AND user_sales_id = %d ", $options["salesman_id"]);
					}
					/* address_id */
					if ($options["address_id"]>0) {
						$regex_syntax = sql_syntax("regex");
						$sql .= sprintf(" AND (sales.address_id = %1\$d OR sales.multirel ".$regex_syntax." '(^|\\\\,)%1\$d(\\\\,|$)')", $options["address_id"]);
					}
					/* projectid */
					if ($options["project_id"]>0) {
						$sql .= sprintf(" AND sales.project_id = %d", $options["project_id"]);
					}
					/* classifications */
					if ($options["classification"]) {
						$regex_syntax = sql_syntax("regex");
						$regex_op = " AND (";
						$classifications = explode("|", substr(substr($options["classification"], 1, strlen($options["classification"])), 0, -1));
						foreach ($classifications as $cla) {
							$sql .= $regex_op." (sales.classification ".$regex_syntax." '(^|\\\\|)". $cla ."(\\\\||$)') ";
							$regex_op = "or";
						}
						$sql .= ") ";
					}
					/* text search */
					if ($options["text"]) {
						$sql .= sprintf(" AND (subject %1\$s '%%%2\$s%%' or sales.description %1\$s '%%%2\$s%%') ", $like, $options["text"]);
					}

					$res = sql_query($sql);
					$row = sql_fetch_assoc($res);

					$total_sum += $row["column_".$k];
				}

				if ($i != $k) {
					$values[$k] .= ",".$total_sum;
				} else {
					$values[$i] = $total_sum;
				}

				$highest_amount = ($highest_amount > $total_sum) ? $highest_amount : $total_sum;
				$i++;
			}
		}

		return $values;
	}
	/* }}} */
	/* saveSearch{{{ */
	/**
	 * Store the search wizard's data into the db
	 *
	 * @return void
	 */
	public function saveSearch() {
		$startTime = mktime(0,0,0, $_REQUEST["search"]["from_month"], $_REQUEST["search"]["from_day"], $_REQUEST["search"]["from_year"]);
		$endTime = mktime(0,0,0, $_REQUEST["search"]["till_month"], $_REQUEST["search"]["till_day"], $_REQUEST["search"]["till_year"]);

		$fields["from_timestamp"]	= array("s", $startTime);
		$fields["till_timestamp"]	= array("d", $endTime);
		$fields["search_user_id"]	= array("s", $_REQUEST["search"]["user_id"]);
		$fields["address_id"]	= array("d", $_REQUEST["search"]["address_id"]);
		$fields["project_id"]	= array("d", $_REQUEST["search"]["project_id"]);
		$fields["classification"]	= array("s", $_REQUEST["search"]["classification"]);
		$fields["xaxis"]		= array("d", $_REQUEST["search"]["xaxis"]);
		$fields["yaxis"]		= array("s", $_REQUEST["search"]["yaxis"]);
		$fields["inactive_flag"]	= array("d", $_REQUEST["search"]["in_active"]);
		$fields["hidden_status"]	= array("s", serialize($_REQUEST["hidden"]));

		$sales = $this->getSalesSavedSearch();
		if (!$_REQUEST["search_user_id"] && $sales=="") {
			$fields["user_id"]	= array("d", $_SESSION["user_id"]);
			$keys = array();
			$vals = array();
			foreach ($fields as $k=>$v) {
				$keys[] = $k;
				if ($v[0]=="s") {
					$vals[]="'".addslashes( $v[1] )."'";
				} else {
					$vals[]=(int)$v[1];
				}
			}
			$keys = implode(",",$keys);
			$vals = implode(",",$vals);

			$q = sprintf("insert into sales_search (%s) values (%s)", $keys, $vals);
			sql_query($q);
		} else {
			$vals = array();
			foreach ($fields as $k=>$v) {
				if ($v[0]=="s") {
					$vals[$k] = "'".addslashes( $v[1] )."'";
				} else {
					$vals[$k] = (int)$v[1];
				}
			}
			$q = sprintf("update sales_search set user_id = %d ", $_REQUEST["search_user_id"]);
			foreach ($vals as $k=>$v) {
				$q .= sprintf(", %s = %s ", $k, $v);
			}
			$q .= sprintf(" where user_id = %d ", $_REQUEST["search_user_id"]);
			sql_query($q);
		}
	}
	/* }}} */
	/* getSalesSavedSearch{{{ */
	/**
	 * Fetch data to for search wizard to filter the result
	 *
	 * @return array Search data
	 */
	public function getSalesSavedSearch() {
		$sql = sprintf("select * from sales_search where user_id = %d ", $_SESSION["user_id"]);
		$rs = sql_query($sql);
		$row = sql_fetch_array($rs);
		return $row;
	}

	public function deleteSearch() {
		$sql = sprintf("delete from sales_search where user_id = %d ", $_SESSION["user_id"]);
		sql_query($sql);
	}
	/* }}} */
	/* saveSortAndSelect {{{ */
	/**
	 * Saves the fields and sort order array into the db
	 *
	 * @param array $data field & sort array
	 */
	public function saveSortAndSelect($data) {
		/* strip empty ones */
		foreach ($data as $field=>$sort) {
			if ($sort) {
				$sortdata[$field] = $sort;
			}
		}
		/* order them by sort order and maintain indices */
		if ($sortdata) {
			krsort($sortdata);  // order by key (name)
			asort($sortdata);   // order by value (sort order)
		}

		$sql_field = "default_sales_fields";
		/* update users settings */
		$sql = sprintf("UPDATE users SET %s = '%s' WHERE id = %d", $sql_field, serialize($sortdata), $_SESSION["user_id"] );
		$result = sql_query($sql);

		/* close the popup */
		$output = new Layout_output();
			$output->addCode($sql);
			$output->start_javascript();
				$output->addCode("
					if (parent) {
						parent.location.href = parent.location.href;
						var t = setTimeout('closepopup();', 100);
					}
				");
			$output->end_javascript();
		$output->exit_buffer();
	}
	/* }}} */
}
?>
