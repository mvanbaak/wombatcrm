<?php
/**
 * Covide Groupware-CRM Sales output class
 *
 * Covide Groupware-CRM is the solutions for all groups off people
 * that want the most efficient way to work to together.
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @author Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @author Stephan vd Haar <svdhaar@users.sourceforge.net>
 * @copyright Copyright 2000-2010 Covide BV
 * @package Covide
 */
Class Sales_output {
	/* constants */
	const include_dir = "classes/sales/inc/";

	/* variables */

	/* methods */
	public function salesEdit($options = array()) {

		$output = new Layout_output();
		if ($options["noiface"] == 1 || $_REQUEST["noiface"] == 1) {
			$output->layout_page(gettext("sales"), 1);
		} else {
			$output->layout_page(gettext("sales"));
		}

		$output->addTag("form", array("id"=>"velden", "action"=>"index.php", "method"=>"post"));

		/* generate nice window */
		$venster = new Layout_venster(array("title"=>gettext("sales"), "subtitle"=>gettext("edit")));
		$venster->addVensterData();

		if ($options["note_id"]) {
			$note_data = new Note_data();
			$note_info = $note_data->getNoteById($options["note_id"]);
			$data = array("subject"=>$note_info["subject"], "description"=>$note_info["body"], "is_active"=>1, "timestamp_prospect"=>$note_info["timestamp"], "address_id"=>$note_info["address_id"]);
			$sales[0] = $data;
			$output->addHiddenField("sales[fromnotes]", "1");
		} else {
			$sales_data = new Sales_data();
			$sales_info = $sales_data->getSalesById($_REQUEST["id"], $_REQUEST["address_id"], $_REQUEST["project_id"]);
			$sales = &$sales_info["data"];
		}
		#print_r($sales);

		/* get relation name */
		$address = new Address_data();
		if ((int) $sales[0]["address_id"] && !$sales[0]["multirel"]) {
			$relname = $address->getAddressNameById($sales[0]["address_id"]);
		} else {
			$relname = "";
		}
		/* see if we need to do some magic on the selected addresses */
		if ($sales[0]["multirel"]) {
			$address_ids = explode(",", $sales[0]["multirel"]);
			$address_ids[] = $sales[0]["address_id"];
			sort($address_ids);
			$multirel = array();
			foreach ($address_ids as $aid) {
				$multirel[$aid] = $address->getAddressNameById($aid);
			}
			unset($address_ids);
			unset($sales[0]["address_id"]);
			$relname = "";
		} else {
			$multirel = array($sales[0]["address_id"]=>$address->getAddressNameById($sales[0]["address_id"]));
			unset($sales[0]["address_id"]);
			$relname = "";
		}

		$tbl = new layout_table();
		/* subject */
		$tbl->addTableRow();
		$tbl->addTableData("", "header");
		$tbl->addCode(gettext("title"));
		$tbl->endTableData();
		$tbl->addTableData("", "data");
		$tbl->addTextField("sales[subject]", $sales[0]["subject"]);
		$tbl->endTableData();
		$tbl->endTableRow();
		/* description */
		$tbl->addTableRow();
		$tbl->addTableData("", "header");
		$tbl->addCode(gettext("description"));
		$tbl->endTableData();
		$tbl->addTableData("", "data");
		$tbl->addTextArea("sales[description]", $sales[0]["description"], array("style"=>"width: 300px; height: 100px;"));
		$tbl->endTableData();
		$tbl->endTableRow();

		/* classifications */
		$tbl->addTableRow();
		$tbl->addTableData("", "header");
		$tbl->addCode(gettext("classification"));
		$tbl->endTableData();
		$tbl->addTableData("", "data");
		$tbl->addHiddenField("sales[classification]", $sales[0]["classification"]);
		$classification = new Classification_output();
		$tbl->addCode($classification->classification_selection("salesclassification", $sales[0]["classification"]));
		$tbl->endTableData();
		$tbl->endTableRow();

		/* active */
		$tbl->addTableRow();
		$tbl->addTableData("", "header");
		$tbl->addCode(gettext("active"));
		$tbl->endTableData();
		$tbl->addTableData("", "data");
		$tbl->addCheckbox("sales[is_active]", 1, $sales[0]["is_active"]);
		$tbl->endTableData();
		$tbl->endTableRow();

		$timestamp_fields = array("action"=>gettext("action"), "prospect"=>gettext("prospect"), "proposal"=>gettext("quote"), "order"=>gettext("order/commission"), "invoice"=>gettext("invoice"), );

		$sales_status = $sales_data->sales_status;

		$days = array("--");
		for ($i = 1; $i <= 31; $i++) {
			$days[$i] = $i;
		}
		$months = array("--");
		for ($i = 1; $i <= 12; $i++) {
			$months[$i] = $i;
		}
		$years = array("--");
		for ($i = 2003; $i <= date("Y") + 5; $i++) {
			$years[$i] = $i;
		}

		/* make a view object for todo records */
		$todo_data = new Todo_data();
		$todolist = $todo_data->getTodoBySaleId($_REQUEST["id"]);

		$calendar = new Calendar_output();
		foreach ($timestamp_fields as $k=>$v) {
			/* dates */
			$tbl->addTableRow();
			$tbl->addTableData("", "header");
			$tbl->addCode($v);
			$tbl->endTableData();
			$tbl->addTableData("", "data");

			if ($sales[0]["timestamp_".$k] > 0) {
				/* For new sales item */
				if ($k == "action" && count($todolist) > 0) {
					list($current_day, $current_month, $current_year) = split("-", substr($todolist[0]["humanend"], 0, strpos($todolist[0]["humanend"], " ")));
				} else {
					$current_day = date("d", $sales[0]["timestamp_".$k]);
					$current_month = date("m", $sales[0]["timestamp_".$k]);
					$current_year = date("Y", $sales[0]["timestamp_".$k]);
				}
			} else {
				/* For existing sales item */
				if ($k == "prospect") {
					$current_day = date("d");
					$current_month = date("m");
					$current_year = date("Y");
				} else if ($k == "action") {
					list($current_day, $current_month, $current_year) = split("-", substr($todolist[0]["humanend"], 0, strpos($todolist[0]["humanend"], " ")));
				}  else {
					$current_day = $current_month = $current_year = "";
				}
			}
			$tbl->addSelectField("sales[timestamp_".$k."_day]", $days, $current_day);
			$tbl->addSelectField("sales[timestamp_".$k."_month]", $months, $current_month);
			$tbl->addSelectField("sales[timestamp_".$k."_year]", $years, $current_year);
			$tbl->addCode($calendar->show_calendar("document.getElementById('salestimestamp_".$k."_day')", "document.getElementById('salestimestamp_".$k."_month')", "document.getElementById('salestimestamp_".$k."_year')"));

			if ($k == "proposal" || $k == "invoice") {
				$tbl->insertAction("attachment", gettext("add attachment from covide filesystem"), "javascript: attachment_popup='".$k."'; popup('?mod=filesys&subaction=add_attachment', 'attachment', 950, 550, 1)");
				$tbl->addTag("div", array("id"=>$k."_file_div"));
				if ($sales[0][$k."_file"]) {
					$this->upload_list($sales[0][$k."_file"], $k, $tbl);
				}
				$tbl->endTag("div");
			} else if ($k == "action") {
				$tbl->insertAction("go_todo", gettext("add todo"), "javascript: add_sales_todo(); ");
				/* Associated todo's */
				$tbl->addTableRow();
				$tbl->insertTableData(gettext("todo"), "", "header");
				$tbl->addTableData("", "data");

				/* make a view object for our records */
				$view = new Layout_view();
				$view->addData($todolist);
				$view->addSubMapping("%%complex_subject", "%overdue");
				$view->addMapping(gettext("date"), "%humanstart");
				$view->addMapping(gettext("end date"), "%humanend");
				$view->addMapping(gettext("contact"), "%%complex_relname");
				if ($GLOBALS["covide"]->license["has_project"] || $GLOBALS["covide"]->license["has_project_declaration"]) {
					$view->addMapping(gettext("project"), "%%complex_project");
					$view->defineComplexMapping("complex_project", array(array("type"=>"link", "link"=>array("javascript: todo_project(", "%project_id", ");"), "text"=>"%project_name")));
				}
				$view->addMapping(gettext("actions"), "%%complex_actions", "right");
				$view->defineComplexMapping("complex_subject", array(array("type"=>"action", "src"=>"important", "check"=>"%is_alert"), array("text"=>"[A] ", "check"=>"%is_active"), array("text"=>"[P] ", "check"=>"%is_passive"), array("text"=>array(" (", "%priority", ") ")), array("type"=>"link", "link"=>array("javascript: toonInfo(", "%id", ");"), "text"=>"%subject")));
				$view->defineComplexMapping("complex_relname", array(array("type"=>"link", "link"=>array("javascript: contactInfo(", "%address_id", ");"), "text"=>"%relname")));
				$view->defineComplexMapping("complex_actions", array(array("type"=>"action", "src"=>"info", "alt"=>gettext("information"), "link"=>array("javascript: toonInfo(", "%id", ");")), array("type"=>"action", "src"=>"ok", "alt"=>gettext("mark as done"), "link"=>array("javascript: todo_delete(", "%id", ", false, true);"), "check"=>"%is_current"), array("type"=>"action", "src"=>"edit", "alt"=>gettext("change:"), "link"=>array("javascript: todo_edit(", "%id", ", 1);"), "check"=>"%is_current"), array("type"=>"action", "src"=>"go_calendar", "alt"=>gettext("plan in calendar"), "link"=>array("javascript: todo_to_cal(", "%id", ");"), "check"=>"%is_current"), array("type"=>"action", "src"=>"calendar_reg_hour", "alt"=>gettext("register hours"), "link"=>array("javascript: todo_to_reg(", "%id", ");"), "check"=>"%project_id")));
				/* end of view, add it to the window buffer */

				if ($count > 5) {
					$style = "height: 200px; overflow: auto;";
				}
				$tbl->addTag("div", array("id"=>"sales_tododiv", "style"=>$style));
				$count = count($todolist);
				$tbl->addCode($view->generate_output());
				$tbl->endTag("div");
				$tbl->endTableData();
				$tbl->endTableRow();
			}
			$tbl->endTableData();
			$tbl->endTableRow();
		}
		$tbl->addHiddenField("sales[proposal_file]", $sales[0]["proposal_file"]);
		$tbl->addHiddenField("sales[invoice_file]", $sales[0]["invoice_file"]);

		/* status */
		$tbl->addTableRow();
		$tbl->addTableData("", "header");
		$tbl->addCode(gettext("status"));
		$tbl->endTableData();
		$tbl->addTableData("", "data");
		$tbl->addSelectField("sales[status]", $sales_status, $sales[0]["status"]);
		$tbl->endTableData();
		$tbl->endTableRow();
		/* user */
		$tbl->addTableRow();
		$tbl->addTableData("", "header");
		$tbl->addCode(gettext("user"));
		$tbl->endTableData();
		$tbl->addTableData("", "data");
		$tbl->addHiddenField("sales[user_sales_id]", $sales[0]["user_sales_id"]);
		$useroutput = new User_output();
		$tbl->addCode($useroutput->user_selection("salesuser_sales_id", $sales[0]["user_sales_id"], 0, 0, 0));
		$tbl->endTableData();
		$tbl->endTableRow();
		/* access */
		$tbl->addTableRow();
		$tbl->insertTableData(gettext("readonly access"), "", "header");
		$tbl->addTableData("", "data");
		$tbl->addHiddenField("sales[users]", $sales[0]["users"]);
		$useroutput = new User_output();
		//$tbl->addCode( $useroutput->user_selection("projectusers", $projectinfo["users"], 1, 0, 1, 0, 1) );
		$tbl->addCode($useroutput->user_selection("salesusers", $sales[0]["users"], array("multiple"=>1, "inactive"=>1, "groups"=>1, "confirm"=>1)));
		unset($useroutput);
		$tbl->endTableData();
		$tbl->endTableRow();
		/* score */
		$tbl->addTableRow();
		$tbl->addTableData("", "header");
		$tbl->addCode(gettext("expected score in %"));
		$tbl->endTableData();
		$tbl->addTableData("", "data");
		$tbl->addTextField("sales[expected_score]", $sales[0]["expected_score"]);
		$tbl->endTableData();
		$tbl->endTableRow();
		/* total sum */
		$tbl->addTableRow();
		$tbl->addTableData("", "header");
		$tbl->addCode(gettext("total costs in &euro;"));
		$tbl->endTableData();
		$tbl->addTableData("", "data");
		$tbl->addTextField("sales[total_sum]", $sales[0]["orig_sum"]);
		$tbl->endTableData();
		$tbl->endTableRow();

		/* relation (multiple) */
		$tbl->addTableRow();
		$tbl->insertTableData(gettext("contact"), "", "header");
		$tbl->addTableData("", "data");
		$tbl->addHiddenField("sales[address_id]", $sales[0]["address_id"]);
		$tbl->insertTag("span", $relname, array("id"=>"searchrel"));
		$tbl->addSpace(1);
		$tbl->insertAction("edit", gettext("change:"), "javascript: popup('?mod=address&action=searchRel', 'searchrel', 0, 0, 1);");
		$tbl->endTableData();
		$tbl->endTableRow();

		/* relation
		 $address = new Address_data();
		 $address_info = $address->getAddressNameByID($sales[0]["address_id"]);
		 $tbl->addTableRow();
		 $tbl->insertTableData(gettext("contact").": ", "", "header");
		 $tbl->addHiddenField("sales[address_id]", $sales[0]["address_id"]);
		 $tbl->addTableData("", "data");
		 $tbl->insertTag("span", $address_info, array("id"=>"layer_relation"));
		 $tbl->insertAction("edit", gettext("edit"), "javascript: popup('?mod=address&action=searchRel', 'search_address', 0, 0, 1);");
		 $tbl->endTableRow();
		 */


		/* project */
		$project = new Project_data();
		$project_info = $project->getProjectNameByID($sales[0]["project_id"]);

		$tbl->addTableRow();

		$tbl->endTableRow();
		$tbl->insertTableData(gettext("project").": ", "", "header");

		$tbl->addHiddenField("sales[project_id]", $sales[0]["project_id"]);
		$tbl->addTableData("", "data");
		$tbl->insertTag("span", $project_info, array("id"=>"layer_projectname"));
		$tbl->insertAction("edit", gettext("edit"), "javascript: pickProject();");
		$tbl->endTableRow();

		/* actions */
		$tbl->addTableRow();
		$tbl->addTableData(array("colspan"=>2), "header");
		$tbl->addSpace(2);
		$tbl->insertAction("save", gettext("save"), "javascript: sales_save();");
		$tbl->endTableData();
		$tbl->endTableRow();

		$tbl->endTable();
		$venster->addCode($tbl->generate_output());

		$venster->endVensterData();

		$output->addTag("form", array("id"=>"velden", "method"=>"post", "action"=>"index.php"));
		$output->addHiddenField("mod", "sales");
		$output->addHiddenField("action", "");
		$output->addHiddenField("id", $_REQUEST["id"]);

		$output->addCode($venster->generate_output());

		$output->endTag("form");

		$output->load_javascript(self::include_dir."salesEdit.js");
		/* do some more magic with the rel field if necessary */
		if (is_array($multirel)) {
			$output->start_javascript();
			$output->addCode("addLoadEvent( update_relsearch() );\n");
			$output->addCode("function update_relsearch() { \n");
			foreach ($multirel as $i=>$n) {
				if ($i) {
					$output->addCode("\n");
					$output->addCode("selectRel($i, \"$n\");");
				}
			}
			$output->addCode("\n}\n");
			$output->end_javascript();
		}
		$output->layout_page_end();
		$output->exit_buffer();
	}

	public function generate_list() {
		$start = (int) $_REQUEST["start"];
		$user_data = new User_data();
		$user_info = $user_data->getUserDetailsById($_SESSION["user_id"]);

		if ($_REQUEST["productId"]) {
			$_REQUEST["search"]["project_id"] = $_REQUEST["productId"];
		} else {
			$_REQUEST["search"]["project_id"] = $_REQUEST["search"]["project_id"];
		}
		$options = array("text"=>$_REQUEST["search"]["text"], "sort"=>$_REQUEST["sort"], "user_id"=>$_REQUEST["search"]["user_id"], "address_id"=>$_REQUEST["search"]["address_id"], "project_id"=>$_REQUEST["search"]["project_id"], "classification"=>$_REQUEST["search"]["classification"], "in_active"=>$_REQUEST["search"]["in_active"]);
		$sales_data = new Sales_data();
		$data = $sales_data->getSalesBySearch($options, $_REQUEST["start"], $_REQUEST["sort"]);
		$totals = $sales_data->getTotals();

		/* XML data export */
		if ($_REQUEST["export_xml"]) {
			$conversion = new Layout_conversion;
			$string = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<export>\n";
			if (!is_array($data["data"])) {
				$string .= "<item>".gettext("no items found")."</item>";
			} else {
				foreach ($data["data"] as $dat) {
					$string .= "<item>\n";
					$string .= sprintf("<%1\$s>%2\$s</%1\$s>\n", "salesitem", str_replace("&", "&amp;", $conversion->str2utf8($dat["subject"])));
					$string .= sprintf("<%1\$s>%2\$s</%1\$s>\n", "description", str_replace("&", "&amp;", $conversion->str2utf8($dat["description"])));
					$string .= sprintf("<%1\$s>%2\$s</%1\$s>\n", "contact", str_replace("&", "&amp;", $conversion->str2utf8($dat["companyname"])));
					$string .= sprintf("<%1\$s>%2\$s</%1\$s>\n", "project", htmlentities($conversion->str2utf8($dat["h_project"]), ENT_NOQUOTES, "UTF-8"));
					$string .= sprintf("<%1\$s>%2\$s</%1\$s>\n", "prospect", htmlentities($dat["h_timestamp_prospect"], ENT_NOQUOTES, "UTF-8"));
					$string .= sprintf("<%1\$s>%2\$s</%1\$s>\n", "quote", htmlentities($dat["h_timestamp_proposal"], ENT_NOQUOTES, "UTF-8"));
					$string .= sprintf("<%1\$s>%2\$s</%1\$s>\n", "order", htmlentities($dat["h_timestamp_order"], ENT_NOQUOTES, "UTF-8"));
					$string .= sprintf("<%1\$s>%2\$s</%1\$s>\n", "invoice", htmlentities($dat["h_timestamp_invoice"], ENT_NOQUOTES, "UTF-8"));
					$string .= sprintf("<%1\$s>%2\$s</%1\$s>\n", "user", htmlentities($conversion->str2utf8($dat["username"]), ENT_NOQUOTES, "UTF-8"));
					$string .= sprintf("<%1\$s>%2\$s</%1\$s>\n", "score", htmlentities($dat["expected_score"], ENT_NOQUOTES, "UTF-8"));
					$string .= sprintf("<%1\$s>%2\$s</%1\$s>\n", "price", htmlentities(number_format($dat["orig_sum"], 2, ",", "."), ENT_NOQUOTES, "UTF-8"));
					$string .= "</item>\n";
				}
			}
			$string .= "</export>\n";

			header("Content-Transfer-Encoding: binary");
			header("Content-Type: text/xml; charset=UTF-8");

			if (strstr($_SERVER["HTTP_USER_AGENT"], "MSIE 5.5")) {
				header("Content-Disposition: filename=salesitems.xml"); //msie 5.5 header bug
			} else {
				header("Content-Disposition: attachment; filename=salesitems.xml");
			}
			echo $string;
			exit();
		} else if ($_REQUEST["export_field"]) {
			/* CSV data export */
			$conversion = new Layout_conversion();
			$csv = array();
			$csv[] = gettext("salesitem");
			$csv[] = gettext("contact");
			$csv[] = gettext("project");
			$csv[] = gettext("prospect");
			$csv[] = gettext("quote");
			$csv[] = gettext("order/commission");
			$csv[] = gettext("invoice");
			$csv[] = gettext("user");
			$csv[] = gettext("score");
			$csv[] = gettext("price");
			$csvdata = $conversion->generateCSVRecord($csv);
			unset($csv);

			if (is_array($data["data"])) {
				foreach ($data["data"] as $dat) {
					$csv = array();
					$csv[] = $dat["subject"];
					$csv[] = $dat["companyname"];
					$csv[] = $dat["h_project"];
					$csv[] = $dat["h_timestamp_prospect"];
					$csv[] = $dat["h_timestamp_proposal"];
					$csv[] = $dat["h_timestamp_order"];
					$csv[] = $dat["h_timestamp_invoice"];
					$csv[] = $dat["username"];
					$csv[] = $dat["expected_score"];
					$csv[] = number_format($dat["orig_sum"], 2, ",", ".");
					$csvdata .= $conversion->generateCSVRecord($csv);
					unset($csv);
				}
			}
			header("Content-Transfer-Encoding: binary");
			header("Content-Type: text/plain; charset=UTF-8");

			if (strstr($_SERVER["HTTP_USER_AGENT"], "MSIE 5.5")) {
				header("Content-Disposition: filename=salesitems.csv"); //msie 5.5 header bug
			} else {
				header("Content-Disposition: attachment; filename=salesitems.csv");
			}
			echo $csvdata;
			exit();
		} else {

			$view = new Layout_view();
			$view->addData($data["data"]);

			$output = new Layout_output();
			$output->layout_page(gettext("Sales")." ".gettext("overview"));

			$output->addTag("form", array("id"=>"velden", "action"=>"index.php", "method"=>"post"));
			// Export CSV
			$output->addHiddenField("export_field", "0");
			// Export XML
			$output->addHiddenField("export_xml", "0");

			/* generate nice window */
			$venster = new Layout_venster(array("title"=>gettext("Sales"), "subtitle"=>gettext("overview")));

			/* menu items */
			if ($_REQUEST["search"]["address_id"] > 0)
				$and = "&address_id=".$_REQUEST["search"]["address_id"];
			$venster->addMenuItem(gettext("new"), "javascript: popup('?mod=sales&hide=1&action=edit".$and."');", "", 0);
			if ($_SESSION["locale"] == "nl_NL") {
				$venster->addMenuItem(gettext("help (wiki)"), "http://wiki.covide.nl/Sales", array("target"=>"_blank"), 0);
			}
			$venster->generateMenuItems();
			$venster->addVensterData();

			$tbl = new layout_table(array("cellspacing"=>1, "cellpadding"=>1));
			$tbl->addTableRow();
			$tbl->addTableData();

			$hdr = new Layout_table(array("cellspacing"=>1, "cellpadding"=>1,
				//"style"       => "border: 1px solid #666;"
				));
			$hdr->addTableRow();
			$hdr->addTableData(array("style"=>"vertical-align: bottom;"));

			/* get users */
			$useroutput = new User_output();
			$hdr->addCode(gettext("user").": ");
			$hdr->endTableData();
			$hdr->addTableData(array("style"=>"vertical-align: bottom;"));

			$hdr->addHiddenField("search[user_id]", $_REQUEST["search"]["user_id"]);
			$hdr->addCode($useroutput->user_selection("searchuser_id", $_REQUEST["search"]["user_id"], 0, 0, 0, 0));

			$hdr->insertAction("forward", gettext("search"), "javascript: submitform();");
			$hdr->endTableData();
			$hdr->addTableData(array("style"=>"vertical-align: bottom;"));

			/* relation*/
			$address = new Address_data();
			$address_info = $address->getAddressNameByID($_REQUEST["search"]["address_id"]);

			/* address id */
			$hdr->addCode(gettext("contact").": ");
			$hdr->endTableData();
			$hdr->addTableData(array("style"=>"vertical-align: bottom;"));
			$hdr->addHiddenField("search[address_id]", $_REQUEST["search"]["address_id"]);
			$hdr->insertTag("span", $address_info, array("id"=>"layer_relation"));
			$hdr->insertAction("edit", gettext("edit"), "javascript: popup('?mod=address&action=searchRel', 'search_address');");

			$hdr->endTableData();
			$hdr->addTableData(array("style"=>"vertical-align: bottom;"));

			/* projectname*/
			$project = new Project_data();
			$project_info = $project->getProjectNameByID($_REQUEST["search"]["project_id"]);

			/* project id */
			$hdr->addCode(gettext("project").": ");
			$hdr->endTableData();
			$hdr->addTableData(array("style"=>"vertical-align: bottom;"));
			$hdr->addHiddenField("search[project_id]", $_REQUEST["search"]["project_id"]);
			$hdr->insertTag("span", $project_info, array("id"=>"layer_projectname"));
			$hdr->insertAction("edit", gettext("edit"), "javascript: pickProject();");

			$hdr->endTableData();

			/* classifaction search */
			$hdr->addTableData(array("style"=>"vertical-align: bottom;"));
			$hdr->addCode(gettext("classification").": ");
			$hdr->endTableData();

			$hdr->addTableData(array("style"=>"vertical-align: bottom;"));
			$hdr->addHiddenField("search[classification]", $_REQUEST["search"]["classification"]);
			$classification = new Classification_output();
			$classification_selected = ($_REQUEST["search"]["classification"]) ? $_REQUEST["search"]["classification"] : '';
			$hdr->addCode($classification->classification_selection("searchclassification", $classification_selected));
			$hdr->endTableData();

			$hdr->addTableData(array("style"=>"vertical-align: bottom;"));
			/* search */
			$hdr->addCode(gettext("search").": ");
			$hdr->endTableData();
			$hdr->addTableData(array("style"=>"vertical-align: bottom;"));
			$hdr->addTextField("search[text]", $_REQUEST["search"]["text"]);
			$hdr->insertAction("forward", gettext("search"), "javascript: submitform();");

			$hdr->endTableData();
			$hdr->addTableData(array("style"=>"vertical-align: bottom;"));
			/* active */
			$hdr->addCode(gettext("show all inactive sales items").": ");
			$hdr->endTableData();
			$hdr->addTableData(array("style"=>"vertical-align: bottom;"));
			$hdr->addCheckBox("search[in_active]", "1", $_REQUEST["search"]["in_active"]);
			$hdr->insertAction("forward", gettext("search"), "javascript: submitform();");

			$hdr->endTableData();
			$hdr->endTableRow();
			$hdr->endTable();
			$tbl->addCode($hdr->generate_output());
			$tbl->addTag("br");
			$tbl->endTableData();
			$tbl->endTableRow();
			$tbl->endTable();

			$venster->addCode($tbl->generate_output());
			$spacer = new Layout_output();
			$spacer->addSpace(15);

			/* add the mappings (columns) we needed */
			$view->addMapping(gettext("salesitem"), "%subject");
			$view->addMapping(gettext("description"), "%description");
			$view->addMapping(gettext("contact"), "%%complex_address");
			$view->addMapping(gettext("project"), "%%complex_project");
			$view->addMapping(gettext("action"), "%h_timestamp_action");
			$view->addMapping(gettext("prospect"), "%h_timestamp_prospect");
			$view->addMapping(gettext("quote"), "%%complex_proposal");
			$view->addMapping(gettext("order/commission"), "%h_timestamp_order");
			$view->addMapping(gettext("invoice"), "%%complex_invoice");

			$view->addMapping(gettext("user"), "%username");
			$view->addMapping(gettext("status"), "%status_text");
			$view->addMapping(gettext("score"), array("%expected_score", "&#037;"), "right");
			$view->addMapping(gettext("price"), "%total_sum", "right");
			$view->addMapping($spacer->generate_output(), "%%complex_actions");
			unset($spacer);

			/* define sort columns */
			$view->defineSortForm("sort", "velden");
			$view->defineSort(gettext("action"), "timestamp_action");
			$view->defineSort(gettext("prospect"), "timestamp_prospect");
			$view->defineSort(gettext("quote"), "timestamp_proposal");
			$view->defineSort(gettext("order/commission"), "timestamp_order");
			$view->defineSort(gettext("invoice"), "timestamp_invoice");
			$view->defineSort(gettext("contact"), "companyname");
			$view->defineSort(gettext("salesitem"), "subject");
			$view->defineSort(gettext("price"), "total_sum");
			$view->defineSort(gettext("score"), "expected_score");
			$view->defineSort(gettext("user"), "username");
			$view->defineSort(gettext("status"), "status");
			$view->defineSort(gettext("project"), "h_project");
			$view->defineSort(gettext("description"), "description");

			$view->defineComplexMapping("complex_project", array(array("type"=>"action", "src"=>"folder_open", "check"=>"%has_project"), array("type"=>"link", "text"=>"%h_project", "link"=>array("?mod=project&action=showhours&id=", "%project_id"), "check"=>"%has_project")));
			$view->defineComplexMapping("complex_subject", array(array("type"=>"link", "link"=>array("javascript: popup('index.php?mod=sales&hide=1&action=edit&id=", "%id", "', 'salesedit', 0, 0, 1);"), "text"=>"%subject")));
			$view->defineComplexMapping("complex_address", array(array("type"=>"action", "src"=>"addressbook", "check"=>"%has_address"), array("type"=>"multilink", "link"=>array("index.php?mod=address&action=relcard&id=", "%all_address_ids"), "text"=>"%all_address_names", "check"=>"%all_address_ids"), array("text"=>$tbl->addTag("br"), "check"=>"%has_address")));

			$view->defineComplexMapping("complex_proposal", array(array("type"=>"link", "text"=>"%h_timestamp_proposal", "link"=>array("javascript: download(", "%proposal_file", ");"))));

			$view->defineComplexMapping("complex_invoice", array(array("type"=>"link", "text"=>"%h_timestamp_invoice", "link"=>array("javascript: download(", "%invoice_file", ");"))));

			$view->defineComplexMapping("complex_actions", array(array("type"=>"action", "src"=>"edit", "alt"=>gettext("edit"), "link"=>array("javascript: popup('index.php?mod=sales&hide=1&action=edit&id=", "%id", "', 'salesedit', 0, 0, 1);"), "check"=>"%check_actions"), array("type"=>"action", "src"=>"delete", "alt"=>gettext("delete"), "link"=>array("?mod=sales&action=delete&id=", "%id"), "confirm"=>gettext("Are you sure you want to delete this item?"), "check"=>"%check_actions")));

			$venster->addCode($view->generate_output());

			$paging = new Layout_paging();
			$paging->setOptions($start, $data["count"], "javascript: blader('%%');");
			$venster->addCode($paging->generate_output());
			$venster->insertAction("file_download", gettext("export as")." CSV", 'javascript: exportSale();');
			$venster->insertAction("file_xml", gettext("export as")." XML", 'javascript: exportSaleXML();');

			$tbl = new Layout_table();
			$tbl->addTableRow();
			$tbl->insertTableData(gettext("total number of orders/commissions").":", "", "header");
			$tbl->insertTableData($totals["count"], "", "header");
			$tbl->insertTableData(gettext("avg score").":", "", "header");
			$tbl->insertTableData($totals["score"]."%", "", "header");
			$tbl->insertTableData(gettext("total sum").":", "", "header");
			$tbl->insertTableData("&euro; ".$totals["sum"], "", "header");
			$tbl->insertTableData(gettext("	total").":", "", "header");
			$tbl->insertTableData("&euro; ".$totals["average"], "", "header");
			$tbl->endTableRow();
			$tbl->endTable();
			$venster->addTag("br");
			$venster->addCode($tbl->generate_output());

			$venster->endVensterData();

			$output->addHiddenField("mod", "sales");
			$output->addHiddenField("action", "");
			$output->addHiddenField("sort", $_REQUEST["sort"]);
			$output->addHiddenField("id", "");
			$output->addHiddenField("start", $start);

			$output->addCode($venster->generate_output());

			$output->endTag("form");

			$output->load_javascript(self::include_dir."salesList.js");

			$GLOBALS["covide"]->history->setStorePoint( gettext("Sales overview"), "sales" );

			$output->layout_page_end();
			$output->exit_buffer();
		}
	}

	public function salesSave() {
		$data = new Sales_data();
		$data->saveItem();

		$output = new Layout_output();
		$output->start_javascript();
		if ($_REQUEST["sales"]["fromnotes"]) {
			$output->addCode("parent.location.href = parent.location.href;");
		} else {
			$output->addCode("parent.document.getElementById('velden').submit();");
		}
		$output->addCode("closepopup();");
		$output->end_javascript();
		$output->exit_buffer();
	}
	/* show_info {{{ */
	/**
	 * Show salesitem in infolayer, to be used with the javascript function loadXML
	 *
	 * @param int $id The salesitem id to show
	 *
	 * @return void
	 */
	public function show_info($id) {
		$sales_data = new Sales_data();
		$salesinfo_tmp = $sales_data->getSalesById($id);
		$salesinfo = $salesinfo_tmp["data"][0];

		$fields[gettext("prospect")] = $salesinfo["h_timestamp_prospect"];
		$fields[gettext("quote")] = $salesinfo["h_timestamp_proposal"];
		$fields[gettext("order/commission")] = $salesinfo["h_timestamp_order"];
		$fields[gettext("invoice")] = $salesinfo["h_timestamp_invoice"];
		$fields[gettext("price")] = $salesinfo["total_sum"];
		$fields[gettext("score")] = $salesinfo["expected_score"];
		$fields[gettext("status")] = $salesinfo["status_text"];
		$fields[gettext("subject")] = $salesinfo["subject"];
		$fields[gettext("description")] = nl2br($salesinfo["description"]);
		$fields[gettext("relation")] = $salesinfo["all_address_names"];
		if ($GLOBALS["covide"]->license["has_project"] || $GLOBALS["covide"]->license["has_project_declaration"])
			$fields[gettext("project")] = $salesinfo["project_name"];

		$table = new Layout_table();

		foreach ($fields as $k=>$v) {
			$table->addTableRow();
			$table->insertTableData($k, "", "header");
			if ($k=="quote" || $k=="invoice") {
				$ts = ($k=="quote" ? "proposal" : $k);
				$table->addTableData("", array("class" => "data"));
					$table->addTag("a", array("href"=>"javascript: download(".$salesinfo[$ts."_file"].");"));
						$table->addCode($salesinfo["h_timestamp_".$ts]);
					$table->endTag("a");
				$table->endTableData();
			} else {
				$table->insertTableData($v, "", "data");
			}
			$table->endTableRow();
		}



		/* make a view object for todo records */
		$todo_data = new Todo_data();
		$todolist = $todo_data->getTodoBySaleId($id);

		/* make a view object for our records */
		$view = new Layout_view();
		$view->addData($todolist);
		$view->addSubMapping("%%complex_subject", "%overdue");
		$view->addMapping(gettext("date"), "%humanstart");
		$view->addMapping(gettext("end date"), "%humanend");
		$view->addMapping(gettext("contact"), "%%complex_relname");
		if ($GLOBALS["covide"]->license["has_project"] || $GLOBALS["covide"]->license["has_project_declaration"]) {
			$view->addMapping(gettext("project"), "%%complex_project");
			$view->defineComplexMapping("complex_project", array(array("type"=>"link", "link"=>array("javascript: todo_project(", "%project_id", ");"), "text"=>"%project_name")));
		}
		$view->defineComplexMapping("complex_subject", array(array("type"=>"action", "src"=>"important", "check"=>"%is_alert"), array("text"=>"[A] ", "check"=>"%is_active"), array("text"=>"[P] ", "check"=>"%is_passive"), array("text"=>array(" (", "%priority", ") ")), array("type"=>"link", "link"=>array("javascript: toonInfo(", "%id", ");"), "text"=>"%subject")));
		$view->defineComplexMapping("complex_relname", array(array("type"=>"link", "link"=>array("javascript: contactInfo(", "%address_id", ");"), "text"=>"%relname")));
		/* end of view, add it to the window buffer */

		if ($count > 5) {
			$style = "height: 200px; overflow: auto;";
		}

		$table->addTableRow();
			$table->insertTableData("todo", "", "header");
			$table->addTableData();
				$table->addTag("div", array("style"=>$style));
					$count = count($todolist);
					$table->addCode($view->generate_output());
				$table->endTag("div");
			$table->endTableData();
		$table->endTableRow();

		$table->addTableRow();
			$table->insertTableData("actions", "", "header");
			$table->addTableData();
				$table->insertAction("edit", gettext("edit"), sprintf("javascript: popup('index.php?mod=sales&hide=1&action=edit&id=%d', 'salesedit', 0, 0, 1);", $id));
				$table->insertAction("delete", gettext("delete"), sprintf("javascript: if(confirm('%s')) { location.href='?mod=sales&action=delete&id=%d'; }", gettext("Are you sure you want to delete this item?"), $id));
			$table->endTableData();
		$table->endTableRow();

		$table->endTable();
		$buf = addslashes(preg_replace("/(\r|\n)/si", "", $table->generate_output()));
		echo sprintf("infoLayer('%s');", $buf);
	}
	/* }}} */
	/* upload_list {{{ */
	/**
	 * Show list of attached files for the sales item
	 *
	 * @return void
	 */
	public function upload_list($id, $variable, $output = null) {
		$exit = false;
		//create code for attachments
		if ($output == null) {
			$output = new Layout_output();
			$conversion = new Layout_conversion();
			$exit = true;
		}
		$output->insertAction("delete", gettext("delete attachment"), "javascript: attachment_delete('".$variable."');");
		$output->insertAction("file_download", gettext("download attachment"), "javascript: download(".$id.");");

		$file_data = new Filesys_data();
		$fileInfo = $file_data->getFileById($id);
		$fileInfo["size"] = $file_data->parseSize($fileInfo["size"]);
		$output->addCode(sprintf(" %s (%s)", $fileInfo["name"], $fileInfo["size"]));
		$output->addTag("br");

		if ($exit) {
			$output->exit_buffer();
		}
	}
	/* }}} */
	/* generate_sales_list {{{ */
	/**
	 * Show list of sales items of a particular sales person
	 *
	 * @param int $salesperson_id The sales person id whose sales items are to be fetched
	 * @param array $options To filter the result
	 *
	 * @return void
	 */
	public function generate_sales_list($salesperson_id, $options = array()) {
		if ($_REQUEST["reload_flag"] && $_REQUEST["reload_flag"] == 1) {
			$options = unserialize(stripslashes($_REQUEST["options"]));
		}
		if ($_REQUEST["productId"]) {
			$_REQUEST["search"]["project_id"] = $_REQUEST["productId"];
		} else {
			$_REQUEST["search"]["project_id"] = $_REQUEST["search"]["project_id"];
		}

		$options["sort"] = $_REQUEST["sort"];
		$options["status"] = $_REQUEST["status"];
		$options["user_id"] = $salesperson_id;
		$sales_data = new Sales_data();
		$data = $sales_data->getSalesBySearch($options, $_REQUEST["start"], $_REQUEST["sort"]);
		$totals = $sales_data->getTotals();
		$user_data = new User_data();
		$user_info = $user_data->getUserDetailsById($_SESSION["user_id"]);

		//$output = new Layout_output();
		$header_arr = array("subject"=>"salesitem", "companyname"=>"contact", "h_project"=>"project", "timestamp_action"=>"action", "timestamp_prospect"=>"prospect", "timestamp_proposal"=>"quote", "timestamp_order"=>"order/commission", "timestamp_invoice"=>"invoice", "status"=>"status", "expected_score"=>"score", "total_sum"=>"price");
		foreach ($header_arr as $k => $v) {
			if ($_REQUEST["sort"] != "" && (strpos($_REQUEST["sort"], $k) !== false)) {
				if (strpos($_REQUEST["sort"], "|desc") !== false) {
					$header_sort[$k] = $k."|asc";
				} else {
					$header_sort[$k] = $k."|desc";
				}
				$italic_text[$k] = "font-style: italic";
			} else {
				$header_sort[$k] = $k."|asc";
				$italic_text[$k] = "";
			}
		}

		/* add the mappings (columns) we needed */
		$field_sort = unserialize($user_info["default_sales_fields"]);
		/* table scroll */

		$salesmen_table = new Layout_table(array("id"=>$salesperson_id."tableid", "width"=>"100%", "border"=>"0", "class"=>"salesmen_table_data", "cellspacing"=>"1", "cellpadding"=>"3"));
			$salesmen_table->addTag("thead");
				$salesmen_table->addTableRow();
					$colspan_count = 1;
					if (is_array($field_sort)) {
						foreach ($field_sort as $k => $v) {
							$colspan_count++;
							$key = array_search($k, $header_arr);
							$salesmen_table->insertTableHeader('<a style="'.$italic_text[$key].'" href="javascript: void(0);" onclick="javascript: sort_saleslist(\''.$salesperson_id.'\', \''.$header_sort[$key].'\');">'.gettext($k).'</a>', array("class"=>"list_header_center text_center", "style"=>"padding:20x !important;"));
						}
					} else {
						foreach ($header_arr as $k => $v) {
							$colspan_count++;
							$salesmen_table->insertTableHeader('<a style="'.$italic_text[$k].'" href="javascript: void(0);" onclick="javascript: sort_saleslist(\''.$salesperson_id.'\', \''.$header_sort[$k].'\');">'.gettext($v).'</a>', array("class"=>"list_header_center text_center", "style"=>"padding:20x !important;"));
						}
					}
				$salesmen_table->endTableRow();
			$salesmen_table->endTag("thead");

			$salesmen_table->addTag("tbody");
        if (is_array($field_sort)) {
			$arr = $field_sort;
		} else {
			$arr = $header_arr;
        }
        $items = 0;
		if (is_array($data["data"])) {
			foreach ($data["data"] as $d) {
	              $items ++;
					$salesmen_table->addTableRow(array("class"=>"list_record","onmouseover"=>"setBgColor(this, 1);", "onmouseout"=>"setBgColor(this, 0);"));
						foreach ($arr as $k => $v) {
							if (is_array($field_sort)) {
								$k = array_search($k, $header_arr);
							}
							$salesmen_table->addTableData(array("class"=>"list_data_clean valign_top"));
							switch ($k) {
								case "subject":
									$salesmen_table->addTag("a", array("href"=>"javascript: sales_info(".$d["id"].");"));
									$salesmen_table->addCode($d[$k]);
									$salesmen_table->endTag("a");
									break;
								case "companyname":
									$contact_ids = explode(",", $d["all_address_ids"]);
									$contact_names = explode(",", $d["all_address_names"]);
									foreach ($contact_ids as $k => $v) {
										$salesmen_table->addTag("a", array("href"=>"index.php?mod=address&action=relcard&id=".$v));
										$salesmen_table->addCode($contact_names[$k]);
										$salesmen_table->endTag("a");
									}
									break;
									case "h_project":
									$salesmen_table->addTag("a", array("href"=>"index.php?mod=project&action=showhours&id=".$d["project_id"]));
									$salesmen_table->addCode($d[$k]);
									$salesmen_table->endTag("a");
									break;
								case "timestamp_proposal":
									$salesmen_table->addTag("a", array("href"=>"javascript: download('".$d["proposal_file"]."');"));
									$salesmen_table->addCode($d["h_".$k]);
									$salesmen_table->endTag("a");
									break;
								case "timestamp_action":
									$todo_data = new Todo_data();
									$todolist = $todo_data->getTodoBySaleId($d["id"]);
									$salesmen_table->addTag("a",array("href"=>"javascript: toonInfo(".$d["id"].");"));
									$salesmen_table->addCode($d["h_".$k]);
									$salesmen_table->endTag("a");
									break;
								case "timestamp_invoice":
									$salesmen_table->addTag("a", array("href"=>"javascript: download('".$d["invoice_file"]."');"));
									$salesmen_table->addCode($d["h_".$k]);
									$salesmen_table->endTag("a");
									break;
								case "timestamp_prospect":
								case "timestamp_order":
									$salesmen_table->addCode($d["h_".$k]);
									break;
								case "status":
									$salesmen_table->addCode($d["status_text"]);
									break;
								case "expected_score":
									$salesmen_table->addCode($d[$k]." %");
									break;
								default:
									$salesmen_table->addCode($d[$k]);
									break;
							}
							$salesmen_table->endTableData();
						}
					$salesmen_table->endTableRow();
				}
			} else {
			$salesmen_table->addTableRow(array("class"=>"list_record","onmouseover"=>"setBgColor(this, 1);", "onmouseout"=>"setBgColor(this, 0);"));
			$salesmen_table->addTableData(array("class"=>"list_data_clean valign_top"));
					$salesmen_table->addCode(gettext("No sales item"));
				$salesmen_table->endTableData();
			$salesmen_table->endTableRow();
		}
		$salesmen_table->endTag("tbody");
		$salesmen_table->endTable();

		if ($_REQUEST["reload_flag"] && $_REQUEST["reload_flag"] == 1) {
			return $salesmen_table->exit_buffer();
		} else {
			return $salesmen_table->generate_output();
		}
	}
	/* }}} */
	/* generate_chart {{{ */
	/**
	 * Display charts for all salesperson
	 *
	 * @return void
	 */
	public function generate_chart() {
		$output = new Layout_output();
		$output->layout_page(gettext("Sales")." ".gettext("overview"));
		$output->load_javascript(self::include_dir."tabmenu.js");
		$venster = new Layout_venster(array("title"=>gettext("Sales"), "subtitle"=>gettext("overview")));

		/* menu items */
		if ($_REQUEST["search"]["address_id"] > 0) {
			$and = "&address_id=".$_REQUEST["search"]["address_id"];
		}
		$venster->addMenuItem(gettext("new"), "javascript: popup('?mod=sales&hide=1&action=edit".$and."');", "", 0);
		if ($_SESSION["locale"] == "nl_NL") {
			$venster->addMenuItem(gettext("help (wiki)"), "http://wiki.covide.nl/Sales", array("target"=>"_blank"), 0);
		}
		$venster->addMenuItem(gettext("select and sort"), "javascript: popup('index.php?mod=sales&action=selectSort', 800, 600, 1);");

		$venster->generateMenuItems();
		$venster->addVensterData();
		$sales_data = new Sales_data();

		/* generate some date arrays */
		$days = array();
		for ($i = 1; $i <= 31; $i++) {
			$days[$i] = $i;
		}

		$months = array();
		for ($i = 1; $i <= 12; $i++) {
			$months[$i] = $i;
		}

		$years = array();
		for ($i = 2000; $i <= date("Y")+1; $i++) {
			$years[$i] = $i;
		}

		$search_values = $sales_data->getSalesSavedSearch();
		if ($_REQUEST["search"]["from_year"]) {
			$startTime = mktime(0, 0, 0, $_REQUEST["search"]["from_month"], $_REQUEST["search"]["from_day"], $_REQUEST["search"]["from_year"]);
			$endTime = mktime(0, 0, 0, $_REQUEST["search"]["till_month"], $_REQUEST["search"]["till_day"], $_REQUEST["search"]["till_year"]);
		} else if ($search_values) {
			$startTime = $search_values["from_timestamp"];
			$endTime = $search_values["till_timestamp"];
		} else {
			/* Get start and end dates of current year */
			$startTime = mktime(0, 0, 0, 1, 1, date('Y'));
			$endTime = mktime(0, 0, 0, 1, 1, date('Y')+1);
		}

		$search_values = $sales_data->getSalesSavedSearch();
		if ($search_values && !$_REQUEST["search"] && !$_REQUEST["action"]) {
			$options = array("text"=>$search_values["search_text"], "user_id"=>$search_values["search_user_id"], "address_id"=>$search_values["address_id"], "project_id"=>$search_values["project_id"], "classification"=>$search_values["classification"], "starttime"=>$startTime, "endtime"=>$endTime);
			$hidden_status = unserialize($search_values["hidden_status"]);
		} else {
			$user_search = split(",", $_REQUEST["search"]["user_id"]);
			foreach ($user_search as $k => $v) {
				if ($v == "") {
					unset($user_search[$k]);
				}
			}
			$_REQUEST["search"]["user_id"] = implode(",", $user_search);
			$options = array("text"=>$_REQUEST["search"]["text"], "user_id"=>$_REQUEST["search"]["user_id"], "address_id"=>$_REQUEST["search"]["address_id"], "project_id"=>$_REQUEST["search"]["project_id"], "classification"=>$_REQUEST["search"]["classification"], "in_active"=>$_REQUEST["search"]["in_active"], "starttime"=>$startTime, "endtime"=>$endTime);
			$hidden_status = ($_REQUEST["hidden"] ? $_REQUEST["hidden"] : array());
		}
		foreach ($hidden_status as $k => $v) {
			$hidden[$k] = $v;
		}

		/* Initialize calendar. */
		$calendar = new Calendar_output();

		/* Get all sales people */
		$people = $sales_data->getAllSalesPerson($options);

		$venster->addTag("br");
		$venster->addTag("div", array("class"=>"sales_div"));

		/* Setup the search form */
		$venster->addTag("form", array("id"=>"searchform", "action"=>"index.php", "method"=>"post"));

		$venster->addHiddenField("mod", "sales");
		$venster->addHiddenField("action", "search");
		$venster->addHiddenField("search[xaxis]", $search_values["xaxis"]);
		$venster->addHiddenField("search[yaxis]", $search_values["yaxis"]);
		if ($search_values) {
			$venster->addHiddenField("search_user_id", $search_values["user_id"]);
		}
		$venster->addHiddenField("hidden[general_div]", ($hidden ? $hidden["general_div"] : 0));
		$venster->addHiddenField("hidden[search_table]", ($hidden ? $hidden["search_table"] : 0));
		for ($i = 0; $i < count($people); $i++) {
			$value = ($hidden ? ($hidden["user_div".$people[$i]["id"]]!="" ? $hidden["user_div".$people[$i]["id"]] : 0) : 0);
			$venster->addHiddenField("hidden[user_div".$people[$i]["id"]."]", $value);
		}

		/* BOF Search Form Block */
		$venster->addTag("div", array("class" => "borderall"));
			$venster->addTag("div", array("class"=>"showhide"));
				if ($hidden["search_table"]) {
					$show_img = "";
					$hide_img = "display: none";
				} else {
					$show_img = "display: none";
					$hide_img = "";
				}
				$venster->addTag("span", array("id"=>"show_img_search_table", "style"=>$show_img));
				$venster->insertAction("arrowShow", gettext("show"), "javascript: show('search_table');");
				$venster->endTag("span");
				$venster->addTag("span", array("id"=>"hide_img_search_table", "style"=>$hide_img));
				$venster->insertAction("arrowHide", gettext("hide"), "javascript: show('search_table');");
				$venster->endTag("span");
				$venster->addSpace(1);
				$venster->addTag("strong");
				$venster->addCode(gettext("Search"));
				$venster->endTag("strong");
			$venster->endTag("div");

			$venster->addTag("div", array("style"=>"clear:both;"));
			$venster->endTag("div");

			$table = new Layout_table(array("id"=>"search_table", "width"=>"100%", "cellpadding"=>5, "cellspacing"=>5, "class"=>"search_tbl" ));
				$table->addTableRow();
					$table->addTableData(array("align"=>"center", "width"=>"25%"));

						$dates_table = new Layout_table();
							$dates_table->addTableRow();
								$dates_table->addTableData("", "data");
									$dates_table->addTag("strong");
										$dates_table->addCode(gettext("From")." : ");
									$dates_table->endTag("strong");
								$dates_table->endTableData();
								$dates_table->addTableData("", "data");
									$dates_table->addSelectField("search[from_day]", $days, ($_REQUEST["search"]["from_day"] ? $_REQUEST["search"]["from_day"] : date("d", $startTime)));
									$dates_table->addSelectField("search[from_month]", $months, ($_REQUEST["search"]["from_month"] ? $_REQUEST["search"]["from_month"] : date("m", $startTime)));
									$dates_table->addSelectField("search[from_year]", $years, ($_REQUEST["search"]["from_year"] ? $_REQUEST["search"]["from_year"] : date("Y", $startTime)));
									$dates_table->addCode($calendar->show_calendar("document.getElementById('searchfrom_day')", "document.getElementById('searchfrom_month')", "document.getElementById('searchfrom_year')"));
								$dates_table->endTableData();
							$dates_table->endTableRow();
							$dates_table->addTableRow();
								$dates_table->addTableData("", "data");
									$dates_table->addTag("strong");
										$dates_table->addCode(gettext("Till")." : ");
									$dates_table->endTag("strong");
								$dates_table->endTableData();
								$dates_table->addTableData(array(), "data");
									$dates_table->addSelectField("search[till_day]", $days, ($_REQUEST["search"]["till_day"] ? $_REQUEST["search"]["till_day"] : date("d", $endTime)));
									$dates_table->addSelectField("search[till_month]", $months, ($_REQUEST["search"]["till_month"] ? $_REQUEST["search"]["till_month"] : date("m", $endTime)));
									$dates_table->addSelectField("search[till_year]", $years, ($_REQUEST["search"]["till_year"] ? $_REQUEST["search"]["till_year"] : date("Y", $endTime)));
									$dates_table->addCode($calendar->show_calendar("document.getElementById('searchtill_day')", "document.getElementById('searchtill_month')", "document.getElementById('searchtill_year')"));
								$dates_table->endTableData();
							$dates_table->endTableRow();
							$dates_table->addTableRow();
								$dates_table->addTableData(array("colspan"=>"2", "style"=>"padding: 10px;"));
									$inactive_check = ($search_values["inactive_flag"] ? $search_values["inactive_flag"] : 0);
									$dates_table->addCheckBox("search[in_active]", 1, $inactive_check);
									$dates_table->addSpace(1);
									$dates_table->addTag("label", array("for"=>"searchin_active"));
									$dates_table->addTag("strong");
										$dates_table->addCode(gettext("show inactive sales items"));
									$dates_table->endTag("strong");
									$dates_table->endTag("label");
								$dates_table->endTableData();
							$dates_table->endTableRow();
						$dates_table->endTable();
						$table->addCode($dates_table->generate_output());

					$table->endTableData();

					$user_data = new User_data();
					$user_perm = $user_data->getUserdetailsById($_SESSION["user_id"]);
					if($user_perm["xs_limited_salesmanage"] || $user_perm["xs_salesmanage"]) {
						$table->addTableData(array("align"=>"center", "width"=>"20%", "valign"=>"top", "class"=>"search_td"));
							$users_table = new Layout_table(array("class"=>"search_users_tbl"));
								$users_table->addTableRow();
									$users_table->addTableData(array("class"=>"search_users_td"));
									/* get users */
										$useroutput = new User_output();
										$users_table->addTag("div", array("class" => "search_users_div"));
											$users_table->addTag("strong");
												$users_table->addCode(gettext("User")." : ");
											$users_table->endTag("strong");
										$users_table->endTag("div");
									$users_table->endTableData();
									$users_table->addTableData();
										$user_id = ($_REQUEST["search"]["user_id"] ? $_REQUEST["search"]["user_id"] : $search_values["search_user_id"]);
										$users_table->addHiddenField("search[user_id]", $user_id);
											$users_table->addCode($useroutput->user_selection("searchuser_id", $user_id, 1, 0, 0, 0));
										$users_table->endTableData();
								$users_table->endTableRow();
							$users_table->endTable();
							$table->addCode($users_table->generate_output());
						$table->endTableData();
					}

					$table->addTableData(array("align"=>"center", "width"=>"15%", "valign"=>"top", "class"=>"search_td"));
						/* BOF project */
						$project = new Project_data();
						$project_id = ($_REQUEST["search"]["project_id"] ? $_REQUEST["search"]["project_id"] : $search_values["project_id"]);
						$project_info = $project->getProjectNameByID($project_id);
						if ($project_info != "" && $project_info != "none") {
							$prj_output = new Layout_output();
							$prj_output->addTag(a, array("href"=>"javascript: void(0);", "onclick"=>"javascript: removeProject();", "title"=>"remove contact"));
							$prj_output->addCode(" [X]");
							$prj_output->endTag(a);
							$project_info .= $prj_output->generate_output();
						}

						$table->addTag("strong");
							$table->addCode(gettext("Project")." : ");
						$table->endTag("strong");
						$table->addSpace(4);
						$table->addHiddenField("search[project_id]", $project_id);
						$table->insertTag("span", $project_info, array("id"=>"layer_projectname"));
						$table->insertAction("edit", gettext("edit"), "javascript: pickProject();");
						/* EOF project */

						$table->addTag("p");
							$table->addSpace(2);
						$table->endTag("p");

						/* BOF contact */
						$address = new Address_data();
						$address_id = ($_REQUEST["search"]["address_id"] ? $_REQUEST["search"]["address_id"] : $search_values["address_id"]);
						$address_info = $address->getAddressNameByID($address_id);
						if ($address_info != "" && $address_info != "none") {
							$addr_output = new Layout_output();
							$addr_output->addTag(a, array("href"=>"javascript: void(0);", "onclick"=>"javascript: removeRel();", "title"=>"remove contact"));
							$addr_output->addCode(" [X]");
							$addr_output->endTag(a);
							$address_info .= $addr_output->generate_output();
						}

						$table->addTag("strong");
							$table->addCode(gettext("Contact")." : ");
						$table->endTag("strong");
						$table->addSpace(4);
						$table->addHiddenField("search[address_id]", $address_id);
						$table->insertTag("span", $address_info, array("id"=>"layer_relation"));
						$table->insertAction("edit", gettext("edit"), "javascript: popup('?mod=address&action=searchRel', 'search_address');");
						/* EOF contact */

					$table->endTableData();

					$table->addTableData(array("align"=>"center", "width"=>"20%", "valign"=>"top", "class"=>"search_td padd_td"));

						/* BOF classification */
						$table->addTag("div", array("class"=>"classification_div"));
							$table->addTag("div", array("class"=>"fl_left"));
								$table->addTag("strong");
									$table->addCode(gettext("Classification")." : ");
								$table->endTag("strong");
								$classification = ($_REQUEST["search"]["classification"] ? $_REQUEST["search"]["classification"] : $search_values["classification"]);
								$table->addHiddenField("search[classification]", $classification);
							$table->endTag("div");
							$table->addTag("div", array("style"=>"float: left"));
								$classification = new Classification_output();
								if ($search_values && !$_REQUEST["search"]["classification"]) {
									$classification_selected = ($search_values["classification"]) ? $search_values["classification"] : '';
								} else {
									$classification_selected = ($_REQUEST["search"]["classification"]) ? $_REQUEST["search"]["classification"] : '';
								}
								$table->addCode($classification->classification_selection("searchclassification", $classification_selected));
							$table->endTag("div");
							$table->addTag("div", array("class"=>"clear"));
							$table->endTag("div");
						$table->endTag("div");
						/* EOF classification */

					$table->endTableData();

					$table->addTableData(array("align"=>"center", "width"=>"20%", "valign"=>"top", "class"=>"search_td padd_td"));

						$inn_table = new Layout_table();
							if ($search_values) {
								$inn_table->addTableRow();
									$inn_table->addTableData();
										$inn_table->addTag("strong");
											$inn_table->addCode(gettext("Remove search filter"));
										$inn_table->endTag("strong");
									$inn_table->endTableData();
									$inn_table->addTableData();
										$inn_table->insertAction("delete", gettext("remove search"), "javascript: remove_search();");
									$inn_table->endTableData();
								$inn_table->endTableRow();
								$inn_table->addTableRow();
									$inn_table->addTableData();
										$inn_table->addSpace(1);
									$inn_table->endTableData();
								$inn_table->endTableRow();
							}
							$inn_table->addTableRow();
								$inn_table->addTableData();
									$inn_table->addTag("strong");
										$inn_table->addCode(gettext("Search"));
									$inn_table->endTag("strong");
								$inn_table->endTableData();
								$inn_table->addTableData();
									$inn_table->insertAction("forward", gettext("search"), "javascript: document.getElementById('searchform').submit();");
								$inn_table->endTableData();
							$inn_table->endTableRow();
						$inn_table->endTable();
						$table->addCode($inn_table->generate_output());

				$table->endTableRow();
			$table->endTable();

			$venster->addCode($table->generate_output());

		$venster->endTag("div");
		/* EOF Search Form Block */

		$venster->endTag("form");
		$venster->addTag("br");
		$venster->addTag("br");

		$user_data = new User_data();
		$user_perm = $user_data->getUserdetailsById($_SESSION["user_id"]);

		/* BOF General Information Block */
		if ($user_perm["xs_salesmanage"] || $user_perm["xs_limited_salesmanage"]) {
			$xaxis_str = array(gettext("per salesman"), gettext("per month"), gettext("per quarter"));
			$yaxis_str = array(gettext("# quotes"), gettext("quotes sum"), gettext("forecast"), gettext("realized"), gettext("total"));

			$venster->start_javascript();
				$venster->addCode("ddtabmenu.definemenu('general_tabs', 0)");
			$venster->end_javascript();

			$venster->addTag("div", array("class" => "borderall"));
				$venster->addTag("div", array("class"=>"showhide"));
					if ($hidden["general_div"]) {
						$show_img = "";
						$hide_img = "display: none";
					} else {
						$show_img = "display: none";
						$hide_img = "";
					}
					$venster->addTag("span", array("id"=>"show_img_general_div", "style"=>$show_img));
					$venster->insertAction("arrowShow", gettext("show"), "javascript: show('general_div');");
					$venster->endTag("span");
					$venster->addTag("span", array("id"=>"hide_img_general_div", "style"=>$hide_img));
					$venster->insertAction("arrowHide", gettext("hide"), "javascript: show('general_div');");
					$venster->endTag("span");
					$venster->addSpace(1);
					$venster->addTag("strong");
						$venster->addCode(gettext("General Information"));
					$venster->endTag("strong");
				$venster->endTag("div");

				$venster->addTag("div", array("class"=>"clear"));
				$venster->endTag("div");

				$general_style = ($hidden["general_div"] ? "display: none" : "" );
				$venster->addTag("div", array("id"=>"general_div", "class"=>"generaldiv", "style"=>$general_style));
					$venster->addTag("div", array("id"=>"general_tabs", "class"=>"ddcolortabs"));
						$venster->addTag("ul", array("class"=>"tab"));
							$venster->addTag("li");
								$venster->addTag("a", array("href"=>"javascript: void(0)", "rel"=>"ct1", "onclick"=>"ddtabmenu.showsubmenu('general_tabs', ddtabmenu['general_tabs'+'-dselected']);"));
									$venster->addTag("span");
										$venster->addCode(gettext("dynamic graph"));
									$venster->endTag("span");
								$venster->endTag("a");
							$venster->endTag("li");
							$venster->addTag("li");
								$venster->addTag("a", array("href"=>"javascript: void(0)", "rel"=>"ct2"));
									$venster->addTag("span");
										$venster->addCode(gettext("statuses"));
									$venster->endTag("span");
								$venster->endTag("a");
							$venster->endTag("li");
						$venster->endTag("ul");
					$venster->endTag("div");
					$venster->addTag("div", array("class"=>"tabcontainer"));
						$venster->addTag("div", array("id"=>"ct1", "class"=>"tabcontent"));
							/* BOF Bar Chart */
							$venster->addTag("div");
								$venster->addTag("div", array("class"=>"fl_left"));
									$axis_table = new Layout_table(array("class"=>"axis_tbl"));
										$axis_table->addTableRow();
											$axis_table->addTableData();
												$axis_table->addTag("strong");
													$axis_table->addCode(gettext("x-axis"));
												$axis_table->endTag("strong");
												$axis_table->addTag("br");
												$axis_table->addTag("br");
											$axis_table->endTableData();
										$axis_table->endTableRow();

										if ($search_values) {
											$xAxis = $search_values["xaxis"];
											$options["search_xaxis"] = $xAxis;
										}
										foreach ($xaxis_str as $k=>$v) {
											$axis_table->addTableRow();
												$axis_table->addTableData();
													$axis_table->addRadioField("xaxis", gettext($v), $k, $xAxis, "", "javascript: reload_barchart();");
												$axis_table->endTableData();
											$axis_table->endTableRow();
										}
										$axis_table->addTableRow();
											$axis_table->addTableData();
												$axis_table->addTag("br");
												$axis_table->addTag("strong");
													$axis_table->addCode(gettext("y-axis"));
												$axis_table->endTag("strong");
												$axis_table->addTag("br");
												$axis_table->addTag("br");
											$axis_table->endTableData();
										$axis_table->endTableRow();

										if ($search_values) {
											$yAxis = explode(",", $search_values["yaxis"]);
											$options["search_yaxis"] = $search_values["yaxis"] ;
										} else {
											$yAxis = array();
										}
										foreach ($yaxis_str as $k=>$v) {
											$axis_table->addTableRow();
												$axis_table->addTableData();
													$check = ( ($k==0 || in_array($k, $yAxis)) ? 1 : 0);
													$axis_table->addCheckBox("yaxis[]", $k, $check);
													$axis_table->addSpace(3);
													$axis_table->addCode(gettext($v));
												$axis_table->endTableData();
											$axis_table->endTableRow();
										}
									$axis_table->endTable();
									$venster->addCode($axis_table->generate_output());

								$venster->endTag("div");
								$venster->addTag("div", array("id"=>"barchart_td", "class"=>"fl_right chart_div"));
									$venster->addCode($this->generate_bar_chart($options));
								$venster->endTag("div");
							$venster->endTag("div");
							$venster->addTag("div", array("class"=>"clear"));
							$venster->endTag("div");
							/* EOF Bar Chart */
						$venster->endTag("div");
						$venster->addTag("div", array("id"=>"ct2", "class"=>"tabcontent"));
							/* BOF Pie Chart */
							$venster->addTag("div");
								$venster->addTag("div", array("class"=>"fl_right chart_div"));
									$venster->addHiddenField("sales[status]", "");
									$options["all_salesmen_pie"] = 1;
									$venster->addCode($this->generate_pie_chart($options));
								$venster->endTag("div");
							$venster->endTag("div");
							$venster->addTag("div", array("class"=>"clear"));
							$venster->endTag("div");
							/* EOF Pie Chart */
						$venster->endTag("div");
					$venster->endTag("div");

					$venster->addTag("br");

					/* BOF Bottom Table Start */
					$bottomTbl = new Layout_table(array("id"=>"totals_div_wrap", "width"=>"100%", "cellspacing"=>"1"));
						$bottomTbl->addTableRow();
							$bottomTbl->addTableData(array("id"=>"totals_div", "width"=>"50%"), "header");
								$ystr = array(0, 1, 2, 3, 4);
								$chart_data = $sales_data->getBarChartData(0, $ystr, $options, &$highest_amount, &$xScale);
								$table = new Layout_table(array("width"=>"100%"));
									$table->addTableRow();
										foreach ($chart_data as $k=>$v) {
											$value = split(",", $v);
											$table->addTableData();
											if ($k == 0) {
												$table->addCode(gettext("items").": ".array_sum($value));
											} else {
												$table->addCode(gettext($yaxis_str[$ystr[$k]]).": ".array_sum($value));
											}
											$table->endTableData();
										}
									$table->endTableRow();
								$table->endTable();
								$bottomTbl->addCode($table->generate_output());
							$bottomTbl->endTableData();
							$bottomTbl->addTableData(array("id"=>"totals_pie_div", "width"=>"50%"), "header");
							$bottomTbl->endTableData();
						$bottomTbl->endTableRow();
					$bottomTbl->endTable();
					$venster->addCode($bottomTbl->generate_output());
					/* EOF Bottom Table End */

				$venster->endTag("div");
				$venster->addTag("div", array("class"=>"clear"));
				$venster->endTag("div");

			$venster->endTag("div");
			$venster->addHiddenField("sales[options]", serialize($options));
		}
		/* EOF General Information Block */

		/* BOF Users sales items Block */
		$count = count($people);
		$options["all_salesmen_pie"] = 0;

		for ($i = 0; $i < $count; $i++) {
			$venster->addTag("br");
			$venster->addTag("br");

			$column_id = $people[$i]["id"]."tableid";
			$venster->addTag("div", array("class" => "borderall"));
				if (!$user_perm["xs_salesmanage"] && !$user_perm["xs_limited_salesmanage"] && $people[$i]["total_sales"]==0) {
					$venster->addTag("div", array("class"=>"nosales_div"));
						$venster->addCode(gettext("No sales items added."));
					$venster->endTag("div");
				} else {
					$venster->addTag("div", array("class"=>"showhide"));
						if ($hidden["user_div".$people[$i]["id"]]) {
							$show_img = "";
							$hide_img = "display: none";
						} else {
							$show_img = "display: none";
							$hide_img = "";
						}
						$venster->addTag("span", array("id"=>"show_img_user_div".$people[$i]["id"], "style"=>$show_img));
						$venster->insertAction("arrowShow", gettext("show"), "javascript: show('user_div".$people[$i]["id"]."');");
						$venster->endTag("span");
						$venster->addTag("span", array("id"=>"hide_img_user_div".$people[$i]["id"], "style"=>$hide_img));
						$venster->insertAction("arrowHide", gettext("hide"), "javascript: show('user_div".$people[$i]["id"]."');");
						$venster->endTag("span");
						$venster->addSpace(1);
						$venster->addTag("strong");
						$venster->addCode(gettext($people[$i]['name']));
						$venster->endTag("strong");
					$venster->endTag("div");

					$user_style = ($hidden["user_div".$people[$i]["id"]] ? "display: none" : "");

					$venster->addTag("div", array("id"=>"user_div".$people[$i]["id"], "class" => "user_div", "style"=>$user_style));
						$output->addHiddenField("sort_column".$people[$i]["id"], "");
						$output->addHiddenField("sort_status".$people[$i]["id"], "");

            $usertable = new Layout_table(array("width"=>"100%", "border"=>"0", "class"=>"salesmen_table", "cellspacing"=>"0", "cellpadding"=>"0"));
						  $usertable->addTableRow();
  						  $usertable->addTableData(array("rowspan"=>"2", "valign" => "top"));
  						  $usertable->addTag("div",array( "class" => "scrolltable", "id"=> "scrolltable".$people[$i]["id"])); // extra div around the scrolltable
                  $usertable->addCode($this->generate_sales_list($people[$i]["id"], $options));
               $usertable->endTag("div"); // end div .scrolltable
  						  $usertable->endTableData();
  						  //$usertable->addTableHeader(array("class"=>"list_header_center text_center"));
  						  $usertable->addTableHeader(array("class"=>"list_header_center quotes_statuses_header"));
                  $usertable->addCode(gettext("Quotes Statuses"));
  						  $usertable->endTableHeader();
						  $usertable->endTableRow();
						  $usertable->addTableRow();
  						  $usertable->addTableData(array("valign"=>"top", "class"=>"salesman_piechart"));
      						//pie charts
                		$options["sort"]        = $_REQUEST["sort"];
                		$options["status"]      = $_REQUEST["status"];
                		$options["user_id"]     = $people[$i]["id"];
                		$options["salesman_id"] = $people[$i]["id"];
      						$usertable->addCode($this->generate_pie_chart($options));
      					$usertable->endTableData();
						  $usertable->endTableRow();
						  $usertable->addTableRow();
  						  $usertable->addTableData(array("id" => "totals".$people[$i]["id"], "colspan"=>"2", "class"=>"list_data_clean valign_top list_header_center totals"));
                  //Totals
              		$sales_data = new Sales_data();
              		$data = $sales_data->getSalesBySearch($options, $_REQUEST["start"], $_REQUEST["sort"]);
              		$totals = $sales_data->getTotals();
                  $usertable->addTag("ul", array("class"=>"totals"));
                  	$usertable->addTag("li");
                  		$usertable->addCode(gettext("items")." : ".$data["count"]);
                  	$usertable->endTag("li");
                  	$usertable->addTag("li");
                  		$usertable->addCode(gettext("avg. score")." : ".(($data["count"] > 0) ? ($totals["score"]) : 0)."&#037;");
                  	$usertable->endTag("li");
                  	$usertable->addTag("li");
                  		$usertable->addCode(gettext("quote sum")." : &euro; ".$totals["sum"]);
                  	$usertable->endTag("li");
                  	$usertable->addTag("li");
                  		$usertable->addCode(gettext("forecast")." : &euro; ".$totals["forecast"]);
                  	$usertable->endTag("li");
                  	$usertable->addTag("li");
                  		$usertable->addCode(gettext("realised")." : &euro; ".$totals["realised"]);
                  	$usertable->endTag("li");
                  	$usertable->addTag("li");
                  		$usertable->addCode(gettext("total")." : &euro; ".($totals["items_total"]));
                  	$usertable->endTag("li");
                  $usertable->endTag("ul");

      						$usertable->addTag("div", array("id"=>"totals_pie_div".$people[$i]["id"], "style"=>"float: right"));
      						$usertable->endTag("div");

    					  $usertable->endTableData();
						  $usertable->endTableRow();
						  $usertable->endTable();
						  $venster->addCode($usertable->generate_output());



          $venster->endTag("div");
				}
				$venster->addTag("div", array("class"=>"clear"));
				$venster->endTag("div");

			$venster->endTag("div");
		}
/*
    	$venster->load_javascript(self::include_dir."fixedHeaderTable1.js");
			$venster->start_javascript();
    			$venster->addCode("
    				$(document).ready(function() {
						$('.scrolltable').fixedHeaderTable({ autoResize:true, fixCol1:true});
					});
    			");
			$venster->end_javascript();
*/
		/* EOF Users sales items Block */
		$venster->addTag("br");
		$venster->endTag("div");

		$venster->endVensterData();
		$output->addCode($venster->generate_output());
		$output->load_javascript(self::include_dir."salesList.js");
		$output->layout_page_end();
		$output->exit_buffer();
	}
	/* }}} */
	/* generate_pie_chart {{{ */
	/**
	 * Display pie chart for all sales person in general information box and also individually for all sales men
	 *
	 * @param array $options To filter the result
	 *
	 * @return void
	 */
	public function generate_pie_chart($options = array()) {
		$output = new Layout_output();
		/* Draw the Pie Chart.  */
		$chart_title = gettext("Quotes Statuses");
		$amount = array();
		$sales_data = new Sales_data();
		$names = $sales_data->sales_status;
		$percent = array();

		$like = sql_syntax("like");
		/* sql query */
		$sql = "select status, COUNT(*) AS number from sales ";
		if ($options["starttime"] && $options["endtime"]) {
			$sql.= sprintf(" where ( timestamp_prospect between %1\$d and %2\$d
								or timestamp_proposal between %1\$d and %2\$d
								or timestamp_order between %1\$d and %2\$d
								or timestamp_invoice between %1\$d and %2\$d
								or timestamp_action between %1\$d and %2\$d ) "
							, $options["starttime"], $options["endtime"]);
		}
		/* active/in_active */
		if ($options["in_active"]) {
			$sql .= " and (sales.is_active IN (1, 0) or sales.is_active is null) ";
		} else {
			$sql .= " and (sales.is_active = 1) ";
		}
		/* user_id */
		if ($options["all_salesmen_pie"] == 1 && $options["user_id"]) {
		  $toUnset = array();
      $user_ids = explode(",", $options["user_id"]);
      for ($i = 0; $i < sizeof($user_ids); $i++) {
        if (empty($user_ids[$i])) {
          $toUnset[] = $i;
        }
      }
      foreach ($toUnset as $i) {
        unset($user_ids[$i]);
      }
			$sql .= sprintf(" and user_sales_id IN ( %s ) ", implode(",", $user_ids));
		} else if ($options["salesman_id"]) {
			$chart_title = "";
			$sql .= sprintf(" and user_sales_id = %d ", $options["salesman_id"]);
		}
		/* address_id */
		if ($options["address_id"] > 0) {
			$regex_syntax = sql_syntax("regex");
			$sql .= sprintf(" and (sales.address_id = %1\$d OR sales.multirel ".$regex_syntax." '(^|\\\\,)%1\$d(\\\\,|$)')", $options["address_id"]);
		}
		/* projectid */
		if ($options["project_id"] > 0) {
			$sql .= sprintf(" and sales.project_id = %d", $options["project_id"]);
		}
		/* classifications */
		if ($options["classification"]) {
			$regex_syntax = sql_syntax("regex");
			$regex_op = " and (";
			$classifications = explode("|", substr(substr($options["classification"], 1, strlen($options["classification"])), 0, -1));
			foreach ($classifications as $cla) {
				$sql .= $regex_op." (sales.classification ".$regex_syntax." '(^|\\\\|)".$cla."(\\\\||$)') ";
				$regex_op = "or";
			}
			$sql .= ") ";
		}
		/* text search */
		if ($options["text"]) {
			$sql .= sprintf(" and (subject %1\$s '%%%2\$s%%' or sales.description %1\$s '%%%2\$s%%') ", $like, $options["text"]);
		}
		$sql .= " group by status";

		$res = sql_query($sql);
		$total = 0;
		while ($row = sql_fetch_assoc($res)) {
			$percent[$row["status"]] = $row["number"];
			$total += $row["number"];
		}

		$pie_names = array();
		foreach ($percent as $k=>$v) {
			$pie_names[$k] = gettext($names[$k])." ".(int) (($v / $total) * 100)."%";
		}
		$tmparr = $pie_names;
		$val = $k;

		foreach ($names as $k=>$v) {
			$flag = 1;
			foreach ($tmparr as $vp) {
				if (strcmp($v, substr($vp, 0, strrpos($vp, " "))) == 0) {
					$flag = 0;
					break;
				}
			}
			if ($flag == 1) {
				$pie_names[++$val] = gettext($names[$k])." 0%";
				$percent[$k] = 0;
			}
		}

		if (!$options["salesman_id"]) {
			$legend_align = "r";
			$img_size = "1000x300";
			$legend_chma = "0|500,20";
		} else {
			$legend_align = "t";
			$img_size = "350x240";
			$legend_chma = "";
		}
		$pieChart = array("chs"=>$img_size, "cht"=>"p", "chco"=>"", "chtt"=>$chart_title, "chd"=>"t:".implode(",", $percent), "chdl"=>implode("|", $pie_names), "chdlp"=>$legend_align, "chma"=>$legend_chma );

		$gPieChartLink = "index.php?mod=google&action=chart";
		foreach ($pieChart as $k=>$v) {
			$gPieChartLink .= "&param[".$k."]=".urlencode($v);
		}
		/* End of Pie Chart Information */

		if (!$options["salesman_id"]) {
			$output->addHiddenField("pie_data", serialize($percent));
			$output->addTag("img", array("id"=>"pieChart", "src"=>$gPieChartLink, "align"=>"center", "onload"=>"display_totals('pie_chart');", "class"=>"piechart_img"));
			$output->endTag("img");
		} else {
			$output->addHiddenField("pie_data".$options["salesman_id"], serialize($percent));
			$output->addTag("img", array("id"=>"pieChart", "src"=>$gPieChartLink, "onload"=>"display_salesmen_totals(".$options["salesman_id"].");"));
			$output->endTag("img");
		}
		return $output->generate_output();
	}
	/* }}} */
	/* generate_bar_chart {{{ */
	/**
	 * Display bar chart for all salesmen
	 *
	 * @param array $options To filter the result
	 *
	 * @return array The data for each column on the bar chart
	 */
	public function generate_bar_chart($options = array()) {
		$yaxis_str = array(gettext("Quotes"), gettext("Quotes Sum"), gettext("Forecast"), gettext("Realized"), gettext("Total"));
		if ($_REQUEST["reload_flag"] == 1) {
			$options = unserialize(stripslashes($_REQUEST["options"]));
		}
		/* Get all sales people */
		$sales_data = new Sales_data();
		$options["generalinfo_flag"] = 1;
		$people = $sales_data->getAllSalesPerson($options);
		$output = new Layout_output();

		$xAxis = ( $_REQUEST["xaxis"] != "" ? $_REQUEST["xaxis"] : ($options["search_xaxis"] ? $options["search_xaxis"] : 0) );
		$xOptionNames = array(gettext("by salesman"), gettext("by month"), gettext("by quarter"));
		$xStr = $xOptionNames[$xAxis];

		if ($_REQUEST["reload_flag"] == 1) {
			$yAxis = explode(",", $_REQUEST["yaxis"]);
		} else if ($options["search_yaxis"]) {
			$yAxis = explode(",", $options["search_yaxis"]);
		}
		if (count($yAxis) == 0) {
			$yAxis[0] = 0;
		}
		$count = count($yAxis);
		$yStr = "";
		/* Get data from db. */
		$values = $old_column = array();
		$highest_amount = 10;
		if ($_REQUEST["status"] != "") {
			$options["status"] = $_REQUEST["status"];
		}

		$search = $sales_data->getSalesSavedSearch();
		if ($search != "") {
			$update_search_sql = sprintf("update sales_search set xaxis = %d, yaxis = '%s' where user_id = %d ", $xAxis, implode(", ", $yAxis), $_SESSION["user_id"]);
			sql_query($update_search_sql);
		}

		$values = $sales_data->getBarChartData($xAxis, $yAxis, $options, &$highest_amount, &$xScale);

		for ($i = 0; $i < $count; $i++) {
			/* Define computation columns. */
			switch ($yAxis[$i]) {
				case 1:
					$yStr_arr[] = $yaxis_str[1];
					break;
				case 2:
					$yStr_arr[] = $yaxis_str[2];
					break;
				case 3:
					$yStr_arr[] = $yaxis_str[3];
					break;
				case 4:
					$yStr_arr[] = $yaxis_str[4];
					break;
				case 0:
				default:
					$yStr_arr[] = $yaxis_str[0];
					break;
			}
		}

		switch ($count) {
			case 1:
				$colStr = "ff9900";
				break;
			case 2:
				$colStr = "ff9900,ffb444";
				break;
			case 3:
				$colStr = "ff9900,ffb444,ffc444";
				break;
			case 4:
				$colStr = "ff9900,ffb444,ffc444,ffd088";
				break;
			case 5:
				$colStr = "ff9900,ffb444,ffc444,ffd088,ffebcc";
				break;
		}

		$valueStr = "";
		$valueStr = implode("|", $values);
		$yStr = implode(", ", $yStr_arr);
		$chdl_str = implode("|", $yStr_arr);

		$where = explode(",", $values[0]);
		$barWidth = $count * count($where) * 50;
		$barWidth = ($barWidth > 1000) ? 1000 : 860;

		/* Draw the Bar Chart. */
		$barChart = array("chs"=>$barWidth."x300", "cht"=>"bvg", "chtt"=>$yStr." ".$xStr, "chd"=>"t:".$valueStr, "chdl"=>$chdl_str, "chxt"=>"x,y", "chds"=>"0,".$highest_amount, "chxl"=>"0:|".$xScale, "chxr"=>"1,0,".$highest_amount, "chbh"=>"a,5,15", "chco"=>$colStr, "chm"=>"N,727272,0,-1,13|N*f2,727272,1,-1,13|N*f2,727272,2,-1,13|N*f2,727272,3,-1,13|N*f2,727272,4,-1,13", );
		$gchartlink = "index.php?mod=google&action=chart";

		foreach ($barChart as $k=>$v) {
			$gchartlink .= "&param[".$k."]=".urlencode($v);
		}

		$output->addTag("div", array("class"=>"sales_quotes_salesman_container"));
		$output->addTag("div", array("style"=>"width: ".$barWidth."px;"));
		$output->addTag("img", array("id"=>"barChart", "src"=>$gchartlink, "align"=>"right"));
		$output->endTag("img");
		$output->endTag("div");
		$output->endTag("div");

		if ($_REQUEST["reload_flag"] == 1) {
			return $output->exit_buffer();
		} else {
			return $output->generate_output();
		}
	}
	/* }}} */
	/* display_pie_totals{{{ */
	/**
	 * Display the total sales item for each status of all salesmen in general information
	 *
	 * @return void
	 */
	public function display_pie_totals() {
		$sales_data = new Sales_data();
		$names = $sales_data->sales_status;
		$chart_data = unserialize(stripslashes($_REQUEST["chart_data"]));

		$output = new Layout_output();
		foreach ($names as $k=>$v) {
			if (!$chart_data[$k] ? $value = 0 : $value = $chart_data[$k])
				;
			$output->addTag("div", array("class"=>"sales_general_information_totals"));
			$output->addTag("a", array("href"=>"javascript: void(0);", "onclick"=>"javascript: $(\'#salesstatus\').val(".$k."); reload_barchart();"));
			$output->addCode(gettext($v).": ".$value);
			$output->endTag("a");
			$output->endTag("div");
		}
		/* Display total */
		$output->addTag("div", array("class"=>"sales_general_information_totals"));
		$output->addTag("a", array("href"=>"javascript: void(0);", "onclick"=>"javascript: $(\'#salesstatus\').val(\'total\'); reload_barchart();"));
		$output->addCode(gettext(total).": ".array_sum($chart_data));
		$output->endTag("a");
		$output->endTag("div");
		echo "document.getElementById('totals_pie_div').innerHTML = '".$output->generate_output()."'; ";
	}
	/* }}} */
	/* display_bar_totals{{{ */
	/**
	 * Display the totals for barchart in general information
	 *
	 * @return void
	 */
	public function display_bar_totals() {
		$sales_data = new Sales_data();
		$yaxis_str = array(gettext("# quotes"), gettext("quotes sum"), gettext("forecast"), gettext("realized"), gettext("total"));
		$ystr = array(0, 1, 2, 3, 4);
		$options = unserialize(stripslashes($_REQUEST["options"]));
		$options["status"] = $_REQUEST["status"];
		$chart_data = $sales_data->getBarChartData(0, $ystr, $options, &$highest_amount, &$xScale);
		$output = new Layout_output();
		$table = new Layout_table(array("width"=>"100%"));
		$table->addTableRow();
		foreach ($chart_data as $k=>$v) {
			$value = split(",", $v);
			$table->addTableData();
			if ($k == 0) {
        var_dump(array_sum($value));
				$table->addCode(gettext("items").": ".array_sum($value));
			} else {
				$table->addCode(gettext($yaxis_str[$ystr[$k]]).": ".array_sum($value));
			}
			$table->endTableData();
		}
		$table->endTableRow();
		$table->endTable();
		$output->addCode($table->generate_output());
		$output->exit_buffer();
	}
	/* }}} */
	/* selectSort{{{ */
	public function selectSort() {
		require (self::include_dir."selectSort.php");
	}
	/* }}} */
	/* display_salesmen_pie_totals{{{ */
	/**
	 * Display the total sales item for each status of individual salesmen
	 *
	 * @return void
	 */
	public function display_salesmen_pie_totals() {
		$sales_data = new Sales_data();
		$names = $sales_data->sales_status;
		$salesperson_id = $_REQUEST["salesperson_id"];
		$chart_data = unserialize(stripslashes($_REQUEST["chart_data"]));

		$output = new Layout_output();
		$output->addTag("ul", array("class"=>"totals"));
			foreach ($names as $k=>$v) {
				$value = (!$chart_data[$k] ? 0 : $chart_data[$k]);
				$output->addTag("li");
				if ($value != 0) {
					$output->addTag("a", array("href"=>"javascript: void(0);", "onclick"=>"javascript : reload_salestable('scrolltable".$salesperson_id."', '".$k."', '".$salesperson_id."');"));
						$output->addCode(gettext($v).": ".$value);
					$output->endTag("a");
				} else {
					$output->addCode(gettext($v).": ".$value);
				}
				$output->endTag("li");
			}
			/* Display total */
			$output->addTag("li");
				$output->addTag("a", array("href"=>"javascript: void(0);", "onclick"=>"javascript : reload_salestable('scrolltable".$salesperson_id."', '', '".$salesperson_id."');"));
					$output->addCode(gettext(total).": ".array_sum($chart_data));
				$output->endTag("a");
			$output->endTag("li");
		$output->endTag("ul");
		$output->exit_buffer();
	}
	/* }}} */
}
?>
