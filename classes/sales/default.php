<?php
/**
 * Covide Groupware-CRM Sales module
 *
 * Covide Groupware-CRM is the solutions for all groups off people
 * that want the most efficient way to work to together.
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @author Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @author Stephan vd Haar <svdhaar@users.sourceforge.net>
 * @copyright Copyright 2000-2009 Covide BV
 * @package Covide
 */
Class Sales {
	/* variables */
	/* methods */
	/* __construct {{{ */
	/**
	 * Route calls to correct method
	 *
	 * @return void
	 */
	public function __construct() {
		if (!$_SESSION["user_id"]) {
			$GLOBALS["covide"]->trigger_login();
		} else {
			$sql = sprintf("select is_active from users where id = %d", $_SESSION["user_id"]);
			$res = sql_query($sql);
			$row = sql_fetch_assoc($res);
			if ($row["is_active"] != 1) {
				unset($_SESSION["user_id"]);
				$GLOBALS["covide"]->trigger_login();
			}
		}
			
		//Check if this user has access to this module
		$user_data = new User_data();
		$user_data->getUserdetailsById($_SESSION["user_id"]);
		if (!$user_data->checkAccess("sales")) {
			$output = new Layout_output();
			$output->addCode(gettext("Sorry, you have been denied access to this module."));
			echo $output->generate_output();
			exit();
		}

		switch ($_REQUEST["action"]) {
		case "edit":
			$sales_output = new Sales_output();
			$sales_output->salesEdit();
			break;
		case "save":
			$sales_output = new Sales_output();
			$sales_output->salesSave();
			break;
		case "delete":
			$sales_data = new Sales_data();
			$sales_data->salesDelete($_REQUEST["id"]);
			$sales_output = new Sales_output();
			$sales_output->generate_chart();
			break;
		case "show_info":
			$sales_output = new Sales_output();
			$sales_output->show_info($_REQUEST["id"]);
			break;
		case "upload_list":
			$sales_output = new Sales_output();
			$sales_output->upload_list($_REQUEST["id"], $_REQUEST["var"]);
			break;
		case "generate_sales_list":
			$sales_output = new Sales_output();
			$sales_output->generate_sales_list($_REQUEST["salesperson_id"]);
			break;
		case "generate_bar_chart":
			$sales_output = new Sales_output();
			$sales_output->generate_bar_chart($_REQUEST["options"]);
			break;
		case "display_salesmen_pie_totals":
			$sales_output = new Sales_output();
			$sales_output->display_salesmen_pie_totals();
			break;
		case "display_pie_totals":
			$sales_output = new Sales_output();
			$sales_output->display_pie_totals();
			break;
		case "display_bar_totals":
			$sales_output = new Sales_output();
			$sales_output->display_bar_totals();
			break;
		case "save_hidden_status":
			$sales_data = new Sales_data();
			$sales_data->saveHiddenStatus();
			break;
		case "selectSort":
			$sales_output = new Sales_output();
			$sales_output->selectSort();
			break;
		case "saveSortAndSelect":
			$sales_data = new Sales_data();
			$sales_data->saveSortAndSelect($_REQUEST["sort"]);
			break;
		case "delete_search":
			$sales_data = new Sales_data();
			$sales_data->deleteSearch();
			break;
		case "search":
			$sales_data = new Sales_data();
			$sales_data->saveSearch();
		default:
			$sales_output = new Sales_output();
			$sales_output->generate_chart();
			break;
		}
	}
	/* }}} */
}
?>
