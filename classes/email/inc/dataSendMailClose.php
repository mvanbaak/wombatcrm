<?php
/**
 * Covide Email module
 *
 * @author Stephan vd Haar <svdhaar@users.sourceforge.net>
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @copyright Copyright 2000-2007 Covide BV
 * @package Covide
 */

$output = new Layout_output();
$output->layout_page(gettext("Email"), 1);
$output->start_javascript();
        $output->addCode("
                if (opener) {
                        var el = opener;
                } else {
                        var el = parent;
                }
                try {
                        if (el.document.getElementById('velden') && el.document.getElementById('velden').mod.value == 'email' && !el.document.getElementById('velden').action.value) {
                                el.document.getElementById('velden').submit();
                        }
                } catch(e) {
                        var err = 1;
                }
                if (el.document.getElementById('deze')) {
                        el.document.getElementById('deze').submit();
                } else {
                        el.document.location.href=el.document.location.href;
                }
                if (!opener) {
                        setTimeout('closepopup();', 100);
		}

        ");
$output->end_javascript();
$output->layout_page_end(1);
$output->exit_buffer();
?>
