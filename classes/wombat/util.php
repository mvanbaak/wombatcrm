<?php
/**
 * Wombat Util class
 *
 * @author Michiel van Baak <mvanbaak@wombatcrm.nl>
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @copyright Copyright 2011 Michiel van Baak
 * @package Covide
 */
class Wombat_Util
{
	/**
	 * Safe way to get an item from an array, with optional default value if array item does not exist
	 *
	 * @param array $array The array to grab the key from
	 * @param string $key Array key to return
	 * @param mixed $default Default value to return if the key is not found
	 *
	 * @return mixed The array key item if it exists, @param $default otherwise
	 */
	public static function arrayGet($array, $key, $default = null)
	{
		if(is_array($array))
		{
			if(array_key_exists($key, $array))
			{
				return $array[$key];
			}
		}
		return $default;
	}
}
?>
