<?php
/**
 * Covide Groupware-CRM Twitter_data
 *
 * Covide Groupware-CRM is the solutions for all groups off people
 * that want the most efficient way to work to together.
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @author Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright Copyright 2009 Covide BV
 * @package Covide
 */

require(dirname(__FILE__).'/twitteroauth/twitteroauth.php');

define('CONSUMER_KEY',    'BFqZFyfb1mjgi3NBauw3NA');
define('CONSUMER_SECRET', 'rbGjFUubdJtXR2wWhsJtc9QWECZrn062AovuTcmm4');
define('CALLBACK_URL',    $GLOBALS['covide']->webroot.'index.php?mod=twitter&action=processCallback');

class Twitter_data {
	/* variables */
	public $error;
	/* methods */
	/* __construct {{{ */
	/**
	 * Constructor to setup class
	 *
	 */
	public function __construct() {
	}
	/* }}} */
	/* getTweets {{{ */
	/**
	 * Get tweets from your timeline
	 *
	 * @param int $show_friends Not used at the moment,
	 * @param array $access_token array with oauth token info
	 *
	 * @return array collection of tweet objects
	 */
	public function getTweets($show_friends = 0, $access_token) {
		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
		$tweets = $connection->get('statuses/home_timeline');
		return $tweets;
	}
	/* }}} */
	/* processCallback {{{ */
	/**
	 * Callback function for linking twitter with covide using OAuth
	 *
	 * @param array $data The info we get from twitter
	 */
	public function processCallback($data) {
		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
		$access_token = $connection->getAccessToken($data['oauth_verifier']);

		/* Save the access tokens. Normally these would be saved in a database for future use. */
		$_SESSION['twitter_access_token'] = $access_token;
		//XXX should be done with userdata->save
		$sql = sprintf("UPDATE users SET twitter_accessdata = '%s' WHERE id = %d;", serialize($access_token), $_SESSION['user_id']);
		$res = sql_query($sql);

		/* Remove no longer needed request tokens */
		unset($_SESSION['oauth_token']);
		unset($_SESSION['oauth_token_secret']);
		echo "<script>window.close();</script>";
	}
	/* }}} */
	/* getConnected {{{ */
	/**
	 * Link covide and twitter using OAuth
	 */
	public function getConnected() {
		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
		$request_token = $connection->getRequestToken(CALLBACK_URL);
		$_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
		$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];


		/* If last connection failed don't display authorization link. */
		switch ($connection->http_code) {
			case 200:
				$url = $connection->getAuthorizeURL($token);
				/* Build authorize URL and redirect user to Twitter. */
				header('Location: ' . $url);
				break;
			default:
				/* Show notification if something went wrong. */
				echo 'Could not connect to Twitter. Refresh the page or try again later.';
		}
	}
	/* }}} */
	/* sendMessage {{{ */
	/**
	 * Sends message to Twitter.
	 *
	 * @param string $message message encoded in UTF-8
	 * @param array $access_token oauth access token info
	 */
	public function sendMessage($access_token, $message) {
		$message = stripslashes($message);
		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
		$res = $connection->post('statuses/update', array('status' => $message));
		if ($connection->http_code == 200) {
			return true;
		} else {
			return false;
		}
	}
	/* }}} */
}
