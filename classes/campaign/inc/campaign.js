function selectCampaign(u) {
	switch (u) {
		case '1':
			//document.getElementById('mod').value = 'newsletter';
			document.getElementById('velden').action.value = 'campaigndetails';
			document.getElementById('velden').type.value = '1';
			document.getElementById('velden').submit();
			break;
		default:
			window.resizeTo(960,600);
			var negative = document.getElementById('hidden_negative').value;
			document.location.href='?mod=address&action=zoekcla&classifications[negative]='+negative+'&campaign=' + u;
			break;
	}
}


function removeCampaign(id) {
	url = 'index.php?mod=campaign&history=1&action=delete&id='+id;
	if (confirm(gettext("Are you sure you want to remove this campaign?"))) {
		document.location.href=url;
	}
}

//show the selected chart
function showSelectedCharts (id_selectedCharts) {
	$(".campaign_charts").css("display", "none")
	$("#div_charts_" + id_selectedCharts).css("display", "inline")
}

function checkResponse() {
	var noAlert = true
	var inputResponse0 = $("#campresponse0response").val();
	var inputResponse1 = $("#campresponse1response").val();
	var inputResponse2 = $("#campresponse2response").val();
	var inputResponse3 = $("#campresponse3response").val();
	var inputReason0 = $("#campresponse0reason").val();
	var inputReason1 = $("#campresponse1reason").val();
	var inputReason2 = $("#campresponse2reason").val();
	var inputReason3 = $("#campresponse3reason").val();

	if (inputResponse0 == "" || inputResponse1 == "") {
		alert(gettext("please fill in at least two general responses and at least one reason per general response"));
		noAlert = false;
	} else if (inputReason0 == "" || inputReason1 == "") {
		alert(gettext("Please fill in at least one reason in response 1 or 2"));
		noAlert = false;
	}

	if (inputResponse2 != "") {
		if (inputReason2 == "") {
			alert(gettext("Please fill in at last one reason in response 3"));
			noAlert = false;
		}
	}

	if (inputResponse3 != "") {
		if (inputReason3 == "") {
			alert(gettext("Please fill in at last one reason in response 4"));
			noAlert = false;
		}
	}

	if (inputResponse3 != "") {
		if (inputResponse2 == "") {
			alert(gettext("Please first fill general response 3 before general response 4"));
			noAlert = false;
		}
	}
	if (noAlert) {
		$("#velden").submit();
	}
}