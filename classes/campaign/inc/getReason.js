//get the reason data from the database
function getDataReason (id_campaign, id_response) {
	jQuery.post("index.php?mod=campaign&action=getDataReason", {id_campaign: id_campaign, id_response: id_response, ajax: 1},
			function(data) {
				$("#div_reason table").remove();
				$("#div_reason").append(data);
				$("#div_reason table").fadeIn("fast");
			}, 'html'
	)
}


$(document).ready(function() {
	//IE hack for change function on radio box
	if ($.browser.msie) {
		$("input[name='response_option']:radio").click(function() {
			this.blur();
			this.focus();
		});
	}
	//when option radio response change, get the right reasons
    $("input[name='response_option']:radio").change(function() {
		var id_response = $("input[name='response_option']:checked").val();
		var id_campaign = $("#campaign_id").val();
		//call function to get the reason data
		getDataReason(id_campaign, id_response);
    });
});
