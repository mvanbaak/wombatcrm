<?php
/**
 * Covide Groupware-CRM Classification_data
 *
 * Covide Groupware-CRM is the solutions for all groups off people
 * that want the most efficient way to work to together.
 * @version %%VERSION%%
 * @license http://www.gnu.org/licenses/gpl.html GPL
 * @link http://www.covide.net Project home.
 * @author Michiel van Baak <mvanbaak@users.sourceforge.net>
 * @copyright Copyright 2000-2007 Covide BV
 * @package Covide
 */
if (!class_exists("Classification_output")) {
	die("no class definition found");
}
/* only allow global usermanagers and global address managers */
$user_data = new User_data();
$user_info = $user_data->getUserdetailsById($_SESSION["user_id"]);
if (!$user_info["xs_classmanage"])
	header("Location: index.php?mod=address");

$group_classification = array();
/* get classifications from db */
$classification_data = new Classification_data();
$classifications = $classification_data->getClassifications("", 1);

//get the data of the groups
$cla_groups = $classification_data->getGroups();
//sort groups in array per id
$group_desc = array();
if (is_array($cla_groups)) {
	foreach ($cla_groups as $key => $value) {
		$id = $value["id"];
		$group_desc[$id] = $value["description"];
	}
}

foreach($classifications as $key => $value) {
	$classifications[$key]["description_full"] = preg_replace("/(\\r\\n|\\r|\\n)/", " ", $classifications[$key]["description_full"]);
	$classifications[$key]["is_locked_check"] = ($classifications[$key]["is_locked"]) ? 0:1;
	/* if special classifiaction, add gettext */
	if ($classifications[$key]["is_locked"])
		$classifications[$key]["description"] = gettext($classifications[$key]["description"]);
	//sort classification per group id
	$group_id = $classifications[$key]["group_id"];
	$group_classification[$group_id][] = $classifications[$key];
}

/* init the output */
$output = new Layout_output();
$output->layout_page(gettext("classifications")." ".gettext("overview"));

/* make array with possible vars, for form generation */
$formitems = array(
	"mod"    => "classification",
	"action" => "show_classifications",
	"id"     => ""
);

/* generate nice window */
$venster = new Layout_venster(array(
	"title"    => gettext("classifications"),
	"subtitle" => gettext("overview")
));
/* menu items */
$venster->addMenuItem(gettext("new"), "javascript: cla_edit(0);");
$venster->addMenuItem(gettext("groups"), "index.php?mod=classification&action=showgroups");
$venster->generateMenuItems();
$venster->addVensterData();
	/* Set up the status hidden/show form */
	$venster->addTag("form", array("id"=>"statusform", "action"=>"index.php", "method"=>"post"));
	$venster->addHiddenField("mod", "classification");
	$venster->addHiddenField("action", "set_status", "actionStatus");
	/* view object for the data */
	$view = new Layout_view();
	$i = 0;
	//get data information of the logged user about the setting of the div(hide/show)
	$status_show = $classification_data->getStatusShowFields();

	foreach($group_classification as $key => $classifications) {
		$view->addData($classifications);
		$view->addMapping(gettext("classification"), "%description", array("class" => "classi_desc_column"));
		$view->addMapping(gettext("description"), "%description_full", array("class" => "classi_descfull_column"));
		$view->addMapping(gettext("active"), "%%complex_active", array("class" => "classi_active_column"));
		if ($GLOBALS["covide"]->license["has_cms"]) {
			$view->addMapping(gettext("cms"), "%%complex_cms", array("class" => "classi_cms_column"));
		}
		if ($GLOBALS["covide"]->license["has_hypo"]) {
			$view->addMapping(gettext("type"), "%%complex_subtype");
		}
		$view->addMapping(gettext("edit"), "%%complex_actions", array("class" => "classi_active_column"));
		/* define the complex mappings */
		$view->defineComplexMapping("complex_active", array(
			array(
				"type"  => "action",
				"src"   => "ok",
				"alt"   => gettext("active"),
				"check" => "%is_active"
			),
			array(
				"type"  => "action",
				"src"   => "cancel",
				"alt"   => gettext("non-active"),
				"check" => "%is_nonactive"
			)
		));
		$view->defineComplexMapping("complex_cms", array(
			array(
				"type"  => "action",
				"src"   => "ok",
				"alt"   => gettext("available in cms"),
				"check" => "%is_cms"
			)
		));
		$view->defineComplexMapping("complex_subtype", array(
			array(
				"type"  => "action",
				"src"   => "state_special",
				"check" => "%h_subtype"
			),
			array(
				"type"  => "text",
				"text"  => "%h_subtype"
			)
		));
		$view->defineComplexMapping("complex_actions", array(
			array(
				"type"  => "action",
				"src"   => "edit",
				"alt"   => gettext("change:"),
				"link"  => array("javascript: cla_edit(", "%id", ")")
			),
			array(
				"type"  => "action",
				"src"   => "delete",
				"alt"   => gettext("delete"),
				"link"  => array("javascript: cla_remove(", "%id", ")")
			)
		));
		//get the groupname
		$groupname = $classifications[0]["groupname"];
		//set border around table
		$venster->addTag("div", array("class" => "campaign_border", "style" => "width:97%;"));
		//add hiddenfield to store the status of a group(0 = hide and 1 = show)
		$venster->addHiddenField("status_show[]", ($status_show[$i] ? $status_show[$i] : 0), "status_show_group_".$i);
		if ($status_show[$i] == 1) {
			$visible = "display:none";
			$arrow = "arrowShow";
			$title = "show";
		} else {
			$visible = "";
			$arrow = "arrowHide";
			$title = "hide";
		}
		//set arrow icon
		$venster->insertAction($arrow, gettext($title), "javascript: set_group_class('group_".$i."')", 'arrow_group_'.$i);
		$venster->addSpace(1);
		$venster->addTag("span", array("style" => "font-size:12px;"));
		//get groupnames, if classification has no groupname set 'not classified'
		if ($classifications[0]["groupname"] != "") {
			$venster->addCode("<b>".$groupname ."</b> : ". $group_desc[$key]);
		} else {
			$venster->addCode("<b>".gettext("not classified") ."</b>");
		}
		$venster->endTag("span");
		$venster->addTag("div", array("id" => "group_".$i, "style" => "width:97%;". $visible));
		$venster->addCode($view->generate_output());
		$venster->endTag("div");
		$venster->endTag("div");
		$i++;
	}
$venster->endTag("form");
$venster->addCode('<br clear="all">&nbsp;');
$venster->endVensterData();
/* end window */

//set form the delete a classification
$output->addTag("form", array(
	"id"     => "claform",
	"method" => "get",
	"action" => "index.php"
));
foreach ($formitems as $item=>$value) {
	$output->addHiddenField($item, $value);
}
$output->endTag("form");
$output->addCode( $venster->generate_output() );


$output->load_javascript(self::include_dir."classification_actions.js");
$output->layout_page_end();
$output->exit_buffer();
?>
