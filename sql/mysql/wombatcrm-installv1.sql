-- MySQL dump 10.13  Distrib 5.1.49, for debian-linux-gnu (i486)
--
-- Host: localhost    Database: covide
-- ------------------------------------------------------
-- Server version	5.1.49-3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `active_calls`
--

DROP TABLE IF EXISTS `active_calls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `active_calls` (
  `name` varchar(255) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `invite` tinyint(3) NOT NULL,
  `user_id_src` int(11) DEFAULT NULL,
  `alert_done` tinyint(3) DEFAULT NULL,
  `ident` varchar(255) DEFAULT NULL,
  KEY `ident_index` (`ident`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `active_calls`
--

LOCK TABLES `active_calls` WRITE;
/*!40000 ALTER TABLE `active_calls` DISABLE KEYS */;
/*!40000 ALTER TABLE `active_calls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(255) DEFAULT NULL,
  `givenname` varchar(255) DEFAULT NULL,
  `companyname` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `zipcode` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `phone_nr` varchar(255) DEFAULT NULL,
  `fax_nr` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_company` smallint(3) DEFAULT NULL,
  `link` int(11) DEFAULT NULL,
  `is_public` smallint(3) DEFAULT NULL,
  `mobile_nr` varchar(255) DEFAULT NULL,
  `debtor_nr` varchar(50) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `company_type` smallint(3) DEFAULT NULL,
  `comment` mediumtext,
  `website` varchar(255) DEFAULT NULL,
  `relation_type` smallint(3) DEFAULT NULL,
  `tav` varchar(255) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `is_customer` smallint(3) DEFAULT NULL,
  `is_supplier` smallint(3) DEFAULT NULL,
  `is_partner` smallint(3) DEFAULT NULL,
  `is_prospect` smallint(3) DEFAULT NULL,
  `is_other` smallint(3) DEFAULT NULL,
  `warning` varchar(255) DEFAULT NULL,
  `pobox` varchar(255) DEFAULT NULL,
  `pobox_zipcode` varchar(255) DEFAULT NULL,
  `pobox_city` varchar(255) DEFAULT NULL,
  `classification` varchar(255) DEFAULT NULL,
  `account_manager` int(11) DEFAULT NULL,
  `is_active` smallint(3) DEFAULT '1',
  `contact_letterhead` smallint(3) DEFAULT NULL,
  `contact_commencement` smallint(3) DEFAULT NULL,
  `contact_initials` varchar(255) DEFAULT NULL,
  `contact_givenname` varchar(255) DEFAULT NULL,
  `contact_infix` varchar(255) DEFAULT NULL,
  `contact_surname` varchar(255) DEFAULT NULL,
  `e4lid` varchar(255) DEFAULT NULL,
  `title` int(11) DEFAULT NULL,
  `relname` varchar(255) DEFAULT NULL,
  `relpass` varchar(255) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `contact_birthday` int(11) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `suffix` int(11) NOT NULL,
  `pobox_state` varchar(255) NOT NULL,
  `pobox_country` varchar(255) NOT NULL,
  `is_person` tinyint(3) DEFAULT NULL,
  `jobtitle` varchar(255) NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `bankaccount` varchar(255) NOT NULL,
  `giro` varchar(255) DEFAULT NULL,
  `bsn` varchar(255) DEFAULT NULL,
  `branche` varchar(255) DEFAULT NULL,
  `duplicate_with` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `address_classification` (`classification`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (8,'','','initX','Bouwheerstraat 1b','3772 AL','Barneveld','0342-490364','0342-423577','info@terrazur.nl',3,1,NULL,1,'','348711222',NULL,NULL,NULL,'http://www.terrazur.nl',0,'Dhr. W.  Massier','Beste Willem',0,1,NULL,NULL,NULL,'','','','',NULL,0,1,1,1,'W.','Willem','','Massier','',0,NULL,NULL,0,NULL,NULL,NULL,0,'','',0,'',NULL,'','',NULL,'IT',0),(9,NULL,NULL,'MvBCoding',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,1,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,0,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'','',0,'',NULL,'','',NULL,'',0);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_birthdays`
--

DROP TABLE IF EXISTS `address_birthdays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_birthdays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bcard_id` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `address_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bcard_id` (`bcard_id`),
  KEY `address_birthdays_address_id` (`address_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_birthdays`
--

LOCK TABLES `address_birthdays` WRITE;
/*!40000 ALTER TABLE `address_birthdays` DISABLE KEYS */;
/*!40000 ALTER TABLE `address_birthdays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_businesscards`
--

DROP TABLE IF EXISTS `address_businesscards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_businesscards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) NOT NULL DEFAULT '0',
  `givenname` varchar(255) DEFAULT NULL,
  `initials` varchar(255) DEFAULT NULL,
  `infix` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `timestamp_birthday` int(11) DEFAULT NULL,
  `mobile_nr` varchar(255) DEFAULT NULL,
  `phone_nr` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `memo` mediumtext,
  `commencement` smallint(3) DEFAULT '3',
  `classification` varchar(255) DEFAULT NULL,
  `letterhead` smallint(3) DEFAULT '2',
  `business_address` varchar(255) DEFAULT NULL,
  `business_zipcode` varchar(255) DEFAULT NULL,
  `business_city` varchar(255) DEFAULT NULL,
  `business_mobile_nr` varchar(255) DEFAULT NULL,
  `business_phone_nr` varchar(255) DEFAULT NULL,
  `business_email` varchar(255) DEFAULT NULL,
  `business_fax_nr` varchar(255) DEFAULT NULL,
  `personal_address` varchar(255) DEFAULT NULL,
  `personal_zipcode` varchar(255) DEFAULT NULL,
  `personal_city` varchar(255) DEFAULT NULL,
  `personal_mobile_nr` varchar(255) DEFAULT NULL,
  `personal_phone_nr` varchar(255) DEFAULT NULL,
  `personal_email` varchar(255) DEFAULT NULL,
  `personal_fax_nr` varchar(255) DEFAULT NULL,
  `e4lid` varchar(255) DEFAULT NULL,
  `title` int(11) DEFAULT NULL,
  `alternative_name` varchar(255) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `businessunit` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `locationcode` varchar(255) DEFAULT NULL,
  `multirel` varchar(255) DEFAULT NULL,
  `business_state` varchar(255) DEFAULT NULL,
  `personal_state` varchar(255) DEFAULT NULL,
  `suffix` int(11) NOT NULL,
  `jobtitle` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `business_phone_nr_2` varchar(255) NOT NULL,
  `business_country` varchar(255) NOT NULL,
  `business_car_phone` varchar(255) NOT NULL,
  `personal_phone_nr_2` varchar(255) NOT NULL,
  `personal_country` varchar(255) NOT NULL,
  `other_address` varchar(255) NOT NULL,
  `other_zipcode` varchar(255) NOT NULL,
  `other_city` varchar(255) NOT NULL,
  `other_state` varchar(255) NOT NULL,
  `other_phone_nr` varchar(255) NOT NULL,
  `other_phone_nr_2` varchar(255) NOT NULL,
  `other_fax_nr` varchar(255) NOT NULL,
  `other_mobile_nr` varchar(255) NOT NULL,
  `other_email` varchar(255) NOT NULL,
  `pobox` varchar(255) NOT NULL,
  `pobox_country` varchar(255) NOT NULL,
  `pobox_state` varchar(255) NOT NULL,
  `pobox_zipcode` varchar(255) NOT NULL,
  `pobox_city` varchar(255) NOT NULL,
  `other_country` varchar(255) NOT NULL,
  `opt_assistant_name` varchar(255) NOT NULL,
  `opt_assistant_phone_nr` varchar(255) NOT NULL,
  `opt_callback_phone_nr` varchar(255) NOT NULL,
  `opt_company_phone_nr` varchar(255) NOT NULL,
  `opt_company_name` varchar(255) NOT NULL,
  `opt_manager_name` varchar(255) NOT NULL,
  `opt_pager_number` varchar(255) NOT NULL,
  `opt_profession` varchar(255) NOT NULL,
  `opt_radio_phone_nr` varchar(255) NOT NULL,
  `opt_telex_number` varchar(255) NOT NULL,
  `rcbc` tinyint(2) DEFAULT '0',
  `ssn` varchar(255) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_private` smallint(2) DEFAULT '0',
  `user_id` int(11) DEFAULT '0',
  `duplicate_with` int(11) DEFAULT '0',
  `business_skype` varchar(50) DEFAULT NULL,
  `personal_skype` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cvd_address_businesscards_address_id` (`address_id`),
  KEY `addressbcards_classification` (`classification`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_businesscards`
--

LOCK TABLES `address_businesscards` WRITE;
/*!40000 ALTER TABLE `address_businesscards` DISABLE KEYS */;
INSERT INTO `address_businesscards` VALUES (1,8,'Rembrandt','R.S.','','Leliveld-Noort',0,NULL,NULL,NULL,'Installation & support Wombat CRM',1,'',1,'Postbus 61','2410 AB','Bodegraven','','+31 348 711222','rembrandt.leliveld@initx.nl','','','','','','','','',NULL,0,'',1300797431,NULL,'','','','','','',0,'','http://www.initx.nl','','NL','','','XX','','','','','','','','','info@initx.nl','61','NL','ZH','2410 AB','Bodegraven','XX','','','','','','','','','','',1,'',2,0,0,0,'',''),(2,0,'Wombat','','','CRM',0,NULL,NULL,NULL,'',3,'',0,'','','','','','','','','','','','','','',NULL,0,'',1300797655,NULL,'','','','','','',0,'','','','XX','','','XX','','','','','','','','','','','XX','','','','XX','','','','','','','','','','',0,'',1,0,2,0,'',''),(3,9,'Michiel','M.','van','Baak',0,NULL,NULL,NULL,'MvBCoding is a small company that provides programming, hosting and consultancy\r\nKvK: 52158918 ',1,'',1,'Cartesiuststraat 306','2562 SV','Den Haag','','','mvanbaak@vanbaak.info','','','','','','','','',NULL,0,'',1300797386,NULL,'','','','','','',0,'','','','NL','','','XX','','','','','','','','','','','XX','','','','XX','','','','','','','','','','',1,'',2,0,0,0,'','');
/*!40000 ALTER TABLE `address_businesscards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_businesscards_info`
--

DROP TABLE IF EXISTS `address_businesscards_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_businesscards_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) NOT NULL,
  `bcard_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `address_id` (`address_id`),
  KEY `bcard_id` (`bcard_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_businesscards_info`
--

LOCK TABLES `address_businesscards_info` WRITE;
/*!40000 ALTER TABLE `address_businesscards_info` DISABLE KEYS */;
INSERT INTO `address_businesscards_info` VALUES (3,8,1);
/*!40000 ALTER TABLE `address_businesscards_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_classifications`
--

DROP TABLE IF EXISTS `address_classifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_classifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `is_active` smallint(3) DEFAULT '0',
  `is_locked` smallint(3) DEFAULT NULL,
  `subtype` smallint(3) DEFAULT NULL,
  `access` varchar(255) NOT NULL,
  `access_read` varchar(255) NOT NULL,
  `description_full` varchar(255) DEFAULT NULL,
  `is_cms` smallint(2) DEFAULT NULL,
  `group_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_classifications`
--

LOCK TABLES `address_classifications` WRITE;
/*!40000 ALTER TABLE `address_classifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `address_classifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_commencement`
--

DROP TABLE IF EXISTS `address_commencement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_commencement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_commencement`
--

LOCK TABLES `address_commencement` WRITE;
/*!40000 ALTER TABLE `address_commencement` DISABLE KEYS */;
INSERT INTO `address_commencement` VALUES (1,'Dhr.'),(2,'Mevr.'),(3,'---'),(4,'Mr.'),(5,'Mrs.'),(6,'Ms.');
/*!40000 ALTER TABLE `address_commencement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_info`
--

DROP TABLE IF EXISTS `address_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) NOT NULL DEFAULT '0',
  `comment` mediumtext,
  `classification` varchar(255) DEFAULT NULL,
  `warning` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `provision_perc` decimal(8,2) DEFAULT NULL,
  `md5` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `addressinfo_classification` (`classification`),
  KEY `addressinfo_addressid` (`address_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_info`
--

LOCK TABLES `address_info` WRITE;
/*!40000 ALTER TABLE `address_info` DISABLE KEYS */;
INSERT INTO `address_info` VALUES (8,8,'','','',NULL,NULL,NULL),(9,9,NULL,'','',NULL,NULL,NULL);
/*!40000 ALTER TABLE `address_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_letterhead`
--

DROP TABLE IF EXISTS `address_letterhead`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_letterhead` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_letterhead`
--

LOCK TABLES `address_letterhead` WRITE;
/*!40000 ALTER TABLE `address_letterhead` DISABLE KEYS */;
INSERT INTO `address_letterhead` VALUES (1,'Beste'),(2,'Geachte'),(3,'Dear');
/*!40000 ALTER TABLE `address_letterhead` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_multivers`
--

DROP TABLE IF EXISTS `address_multivers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_multivers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(255) DEFAULT NULL,
  `givenname` varchar(255) DEFAULT NULL,
  `companyname` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `zipcode` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `phone_nr` varchar(255) DEFAULT NULL,
  `fax_nr` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_company` smallint(3) DEFAULT NULL,
  `link` int(11) DEFAULT NULL,
  `is_public` smallint(3) DEFAULT NULL,
  `mobile_nr` varchar(255) DEFAULT NULL,
  `debtor_nr` int(11) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `company_type` smallint(3) DEFAULT NULL,
  `comment` mediumtext,
  `website` varchar(255) DEFAULT NULL,
  `relation_type` smallint(3) DEFAULT NULL,
  `tav` varchar(255) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `is_customer` smallint(3) DEFAULT NULL,
  `is_supplier` smallint(3) DEFAULT NULL,
  `is_partner` smallint(3) DEFAULT NULL,
  `is_prospect` smallint(3) DEFAULT NULL,
  `is_other` smallint(3) DEFAULT NULL,
  `warning` varchar(255) DEFAULT NULL,
  `pobox` varchar(255) DEFAULT NULL,
  `pobox_zipcode` varchar(255) DEFAULT NULL,
  `pobox_city` varchar(255) DEFAULT NULL,
  `classification` varchar(255) DEFAULT NULL,
  `account_manager` int(11) DEFAULT NULL,
  `is_active` smallint(3) DEFAULT '1',
  `contact_letterhead` smallint(3) DEFAULT '2',
  `contact_commencement` smallint(3) DEFAULT '2',
  `contact_initials` varchar(255) DEFAULT NULL,
  `contact_givenname` varchar(255) DEFAULT NULL,
  `contact_infix` varchar(255) DEFAULT NULL,
  `contact_surname` varchar(255) DEFAULT NULL,
  `e4lid` varchar(255) DEFAULT NULL,
  `title` int(11) DEFAULT NULL,
  `relname` varchar(255) DEFAULT NULL,
  `relpass` varchar(255) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL,
  `sync_modified` int(11) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_multivers`
--

LOCK TABLES `address_multivers` WRITE;
/*!40000 ALTER TABLE `address_multivers` DISABLE KEYS */;
/*!40000 ALTER TABLE `address_multivers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_other`
--

DROP TABLE IF EXISTS `address_other`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_other` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyname` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `givenname` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `zipcode` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `phone_nr` varchar(255) DEFAULT NULL,
  `fax_nr` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_company` smallint(3) DEFAULT NULL,
  `is_public` smallint(3) DEFAULT NULL,
  `mobile_nr` varchar(255) DEFAULT NULL,
  `comment` mediumtext,
  `website` varchar(255) DEFAULT NULL,
  `pobox` varchar(255) DEFAULT NULL,
  `pobox_zipcode` varchar(255) DEFAULT NULL,
  `pobox_city` varchar(255) DEFAULT NULL,
  `is_active` smallint(3) DEFAULT '1',
  `is_companylocation` smallint(3) DEFAULT '0',
  `arbo_kantoor` smallint(3) DEFAULT '0',
  `arbo_bedrijf` int(11) DEFAULT NULL,
  `arbo_code` varchar(255) DEFAULT NULL,
  `arbo_team` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `infix` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_other`
--

LOCK TABLES `address_other` WRITE;
/*!40000 ALTER TABLE `address_other` DISABLE KEYS */;
/*!40000 ALTER TABLE `address_other` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_private`
--

DROP TABLE IF EXISTS `address_private`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_private` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(255) DEFAULT NULL,
  `givenname` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `zipcode` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `phone_nr` varchar(255) DEFAULT NULL,
  `fax_nr` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_company` smallint(3) DEFAULT NULL,
  `is_public` smallint(3) DEFAULT NULL,
  `mobile_nr` varchar(255) DEFAULT NULL,
  `comment` mediumtext,
  `website` varchar(255) DEFAULT NULL,
  `pobox` varchar(255) DEFAULT NULL,
  `pobox_zipcode` varchar(255) DEFAULT NULL,
  `pobox_city` varchar(255) DEFAULT NULL,
  `is_active` smallint(3) DEFAULT '1',
  `country` varchar(255) DEFAULT NULL,
  `e4lid` varchar(255) DEFAULT NULL,
  `modified` int(11) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `infix` varchar(255) DEFAULT NULL,
  `tav` varchar(255) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `contact_letterhead` smallint(3) DEFAULT NULL,
  `contact_commencement` smallint(3) DEFAULT NULL,
  `contact_initials` varchar(255) DEFAULT NULL,
  `title` int(11) DEFAULT NULL,
  `sync_added` int(11) NOT NULL,
  `state` varchar(255) DEFAULT NULL,
  `jobtitle` varchar(255) NOT NULL,
  `locationcode` varchar(255) NOT NULL,
  `businessunit` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `business_address` varchar(255) NOT NULL,
  `business_phone_nr` varchar(255) NOT NULL,
  `business_city` varchar(255) NOT NULL,
  `business_phone_nr_2` varchar(255) NOT NULL,
  `business_state` varchar(255) NOT NULL,
  `business_fax_nr` varchar(255) NOT NULL,
  `business_zipcode` varchar(255) NOT NULL,
  `business_mobile_nr` varchar(255) NOT NULL,
  `business_country` varchar(255) NOT NULL,
  `business_car_phone` varchar(255) NOT NULL,
  `business_email` varchar(255) NOT NULL,
  `phone_nr_2` varchar(255) NOT NULL,
  `other_address` varchar(255) NOT NULL,
  `other_phone_nr` varchar(255) NOT NULL,
  `other_city` varchar(255) NOT NULL,
  `other_phone_nr_2` varchar(255) NOT NULL,
  `other_state` varchar(255) NOT NULL,
  `other_fax_nr` varchar(255) NOT NULL,
  `other_zipcode` varchar(255) NOT NULL,
  `other_mobile_nr` varchar(255) NOT NULL,
  `other_country` varchar(255) NOT NULL,
  `alternative_name` varchar(255) NOT NULL,
  `timestamp_birthday` varchar(255) NOT NULL,
  `suffix` varchar(255) NOT NULL,
  `pobox_state` varchar(255) NOT NULL,
  `pobox_country` varchar(255) NOT NULL,
  `other_email` varchar(255) NOT NULL,
  `opt_assistant_name` varchar(255) NOT NULL,
  `opt_assistant_phone_nr` varchar(255) NOT NULL,
  `opt_callback_phone_nr` varchar(255) NOT NULL,
  `opt_company_phone_nr` varchar(255) NOT NULL,
  `opt_company_name` varchar(255) NOT NULL,
  `opt_manager_name` varchar(255) NOT NULL,
  `opt_pager_number` varchar(255) NOT NULL,
  `opt_profession` varchar(255) NOT NULL,
  `opt_radio_phone_nr` varchar(255) NOT NULL,
  `opt_telex_number` varchar(255) NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `skype` varchar(50) DEFAULT NULL,
  `business_skype` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_private`
--

LOCK TABLES `address_private` WRITE;
/*!40000 ALTER TABLE `address_private` DISABLE KEYS */;
/*!40000 ALTER TABLE `address_private` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_selections`
--

DROP TABLE IF EXISTS `address_selections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_selections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `datetime` int(11) NOT NULL,
  `info` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_selections`
--

LOCK TABLES `address_selections` WRITE;
/*!40000 ALTER TABLE `address_selections` DISABLE KEYS */;
INSERT INTO `address_selections` VALUES (5,1,1300797615,'a:15:{s:11:\"addresstype\";s:9:\"relations\";s:12:\"bcard_export\";b:1;s:3:\"top\";i:0;s:6:\"action\";N;s:1:\"l\";N;s:3:\"sub\";N;s:6:\"and_or\";N;s:6:\"search\";N;s:9:\"specified\";N;s:10:\"landSelect\";N;s:15:\"classifications\";N;s:13:\"selectiontype\";N;s:4:\"sort\";N;s:13:\"funambol_user\";N;s:8:\"cmsforms\";N;}'),(4,2,1300796868,'a:15:{s:11:\"addresstype\";s:9:\"relations\";s:12:\"bcard_export\";b:1;s:3:\"top\";i:0;s:6:\"action\";N;s:1:\"l\";N;s:3:\"sub\";N;s:6:\"and_or\";N;s:6:\"search\";N;s:9:\"specified\";N;s:10:\"landSelect\";N;s:15:\"classifications\";N;s:13:\"selectiontype\";N;s:4:\"sort\";N;s:13:\"funambol_user\";N;s:8:\"cmsforms\";N;}'),(6,1,1300797622,'a:15:{s:11:\"addresstype\";s:5:\"users\";s:12:\"bcard_export\";b:1;s:3:\"top\";i:0;s:6:\"action\";N;s:1:\"l\";N;s:3:\"sub\";N;s:6:\"and_or\";N;s:6:\"search\";N;s:9:\"specified\";N;s:10:\"landSelect\";N;s:15:\"classifications\";N;s:13:\"selectiontype\";N;s:4:\"sort\";N;s:13:\"funambol_user\";s:0:\"\";s:8:\"cmsforms\";N;}'),(7,1,1300797655,'a:15:{s:11:\"addresstype\";s:5:\"users\";s:12:\"bcard_export\";b:1;s:3:\"top\";i:0;s:6:\"action\";N;s:1:\"l\";N;s:3:\"sub\";N;s:6:\"and_or\";N;s:6:\"search\";N;s:9:\"specified\";N;s:10:\"landSelect\";N;s:15:\"classifications\";N;s:13:\"selectiontype\";N;s:4:\"sort\";N;s:13:\"funambol_user\";s:0:\"\";s:8:\"cmsforms\";N;}'),(8,2,1300799918,'a:15:{s:11:\"addresstype\";s:9:\"relations\";s:12:\"bcard_export\";b:1;s:3:\"top\";i:0;s:6:\"action\";N;s:1:\"l\";N;s:3:\"sub\";N;s:6:\"and_or\";N;s:6:\"search\";N;s:9:\"specified\";N;s:10:\"landSelect\";N;s:15:\"classifications\";N;s:13:\"selectiontype\";N;s:4:\"sort\";N;s:13:\"funambol_user\";N;s:8:\"cmsforms\";N;}');
/*!40000 ALTER TABLE `address_selections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_suffix`
--

DROP TABLE IF EXISTS `address_suffix`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_suffix` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_suffix`
--

LOCK TABLES `address_suffix` WRITE;
/*!40000 ALTER TABLE `address_suffix` DISABLE KEYS */;
INSERT INTO `address_suffix` VALUES (1,'I'),(2,'II'),(3,'III'),(4,'Jr.'),(5,'Sr.');
/*!40000 ALTER TABLE `address_suffix` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_titles`
--

DROP TABLE IF EXISTS `address_titles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_titles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_titles`
--

LOCK TABLES `address_titles` WRITE;
/*!40000 ALTER TABLE `address_titles` DISABLE KEYS */;
INSERT INTO `address_titles` VALUES (1,'Dr.'),(2,'Drs.'),(3,'Ing.'),(4,'Ir.'),(5,'Mr.'),(6,'Prof.'),(7,'Prof. Dr.'),(8,'BSc.'),(9,'MSc.'),(10,'Drs. Ing.');
/*!40000 ALTER TABLE `address_titles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `announcements`
--

DROP TABLE IF EXISTS `announcements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `body` mediumtext,
  `is_popup` smallint(3) DEFAULT NULL,
  `is_active` smallint(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `announcements`
--

LOCK TABLES `announcements` WRITE;
/*!40000 ALTER TABLE `announcements` DISABLE KEYS */;
/*!40000 ALTER TABLE `announcements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `arbo_arbo`
--

DROP TABLE IF EXISTS `arbo_arbo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arbo_arbo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regiokantoor` varchar(255) DEFAULT NULL,
  `regiofax` varchar(255) DEFAULT NULL,
  `regiotel` varchar(255) DEFAULT NULL,
  `regioteam` varchar(255) DEFAULT NULL,
  `werkgever` varchar(255) DEFAULT NULL,
  `adres` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `aansluitcode` varchar(255) DEFAULT NULL,
  `contactpers` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `arbo_arbo`
--

LOCK TABLES `arbo_arbo` WRITE;
/*!40000 ALTER TABLE `arbo_arbo` DISABLE KEYS */;
/*!40000 ALTER TABLE `arbo_arbo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `arbo_verslag`
--

DROP TABLE IF EXISTS `arbo_verslag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arbo_verslag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `manager` int(11) DEFAULT NULL,
  `soort` int(11) DEFAULT NULL,
  `datum` int(11) DEFAULT NULL,
  `omschrijving` mediumtext,
  `acties` mediumtext,
  `betrokkenen` mediumtext,
  `datum_invoer` int(11) DEFAULT NULL,
  `ziekmelding` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `arbo_verslag`
--

LOCK TABLES `arbo_verslag` WRITE;
/*!40000 ALTER TABLE `arbo_verslag` DISABLE KEYS */;
/*!40000 ALTER TABLE `arbo_verslag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `arbo_ziek`
--

DROP TABLE IF EXISTS `arbo_ziek`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arbo_ziek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `werkgever_id` int(11) DEFAULT NULL,
  `arbo_id` int(11) DEFAULT NULL,
  `datum` int(11) DEFAULT NULL,
  `datum_ziek` int(11) DEFAULT NULL,
  `datum_melding` int(11) DEFAULT NULL,
  `datum_herstel` int(11) DEFAULT NULL,
  `herstel` int(11) DEFAULT NULL,
  `herstel_loon` int(11) DEFAULT NULL,
  `zwanger` smallint(3) DEFAULT '0',
  `zwanger_ziek` smallint(3) DEFAULT '0',
  `orgaandonatie` smallint(3) DEFAULT '0',
  `ongeval` smallint(3) DEFAULT '0',
  `ontvangt_wao` smallint(3) DEFAULT '0',
  `wao_perc` int(11) DEFAULT NULL,
  `herintr_wao` smallint(3) DEFAULT '0',
  `herintr_perc` int(11) DEFAULT NULL,
  `bijzonderheden` mediumtext,
  `ziekmelding` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `arbo_ziek`
--

LOCK TABLES `arbo_ziek` WRITE;
/*!40000 ALTER TABLE `arbo_ziek` DISABLE KEYS */;
/*!40000 ALTER TABLE `arbo_ziek` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asterisk_settings`
--

DROP TABLE IF EXISTS `asterisk_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asterisk_settings` (
  `server_hostname` varchar(255) DEFAULT 'localhost',
  `server_port` varchar(6) DEFAULT '5038',
  `server_timeout` int(11) DEFAULT '60',
  `manager_login` varchar(255) DEFAULT 'covide',
  `manager_pwd` varchar(255) DEFAULT 'covidepw'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asterisk_settings`
--

LOCK TABLES `asterisk_settings` WRITE;
/*!40000 ALTER TABLE `asterisk_settings` DISABLE KEYS */;
INSERT INTO `asterisk_settings` VALUES ('localhost','5038',60,'covide','covidepw'),('localhost','5038',60,'covide','covidepw'),('localhost','5038',60,'covide','covidepw');
/*!40000 ALTER TABLE `asterisk_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bugs`
--

DROP TABLE IF EXISTS `bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bugs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `module` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bugs`
--

LOCK TABLES `bugs` WRITE;
/*!40000 ALTER TABLE `bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar`
--

DROP TABLE IF EXISTS `calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'unique id for an appointment.',
  `timestamp_start` int(11) unsigned NOT NULL COMMENT 'UNIX timestamp of start date and time for the appointment',
  `timestamp_end` int(11) unsigned NOT NULL COMMENT 'UNIX timestamp of end date and time for the appointment',
  `alldayevent` tinyint(2) NOT NULL COMMENT '1 if this is an all-day event',
  `subject` varchar(255) NOT NULL COMMENT 'Short description for the appointment',
  `body` text NOT NULL COMMENT 'Extended description for the appointment. Be sure to remove HTML tags when sending this to funambol.',
  `location` varchar(255) NOT NULL COMMENT 'The location for the appointment',
  `kilometers` int(11) unsigned DEFAULT NULL COMMENT 'Distance to the location of the appointment',
  `reminderset` tinyint(2) NOT NULL COMMENT '1 if a reminder should be sent to the user',
  `reminderminutesbeforestart` int(11) unsigned NOT NULL COMMENT 'Number of minutes before the start of the appointment a reminder should be sent',
  `busystatus` tinyint(4) NOT NULL COMMENT '0 for free, 1 for tentative, 2 for busy, 3 for outofoffice',
  `importance` tinyint(4) NOT NULL COMMENT '0 for low, 1 for normal, 2 for high',
  `address_id` int(11) unsigned NOT NULL COMMENT 'main contact id from address table for this appointment',
  `multirel` varchar(255) NOT NULL COMMENT 'pipe seperated list of additional contacts for this appointment',
  `project_id` int(11) unsigned DEFAULT NULL COMMENT 'main project id from project table for this appointment',
  `private_id` int(11) NOT NULL DEFAULT '0',
  `is_private` tinyint(2) NOT NULL COMMENT '1 if this is a private appointment that other users are not allowed to view/alter',
  `isrecurring` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'true if the appointment is a recurring appointment',
  `modified_by` int(11) unsigned NOT NULL COMMENT 'user id of the user that last modified this appointment',
  `modified` int(11) unsigned NOT NULL COMMENT 'UNIX timestamp when this appointment was last modified',
  `is_ill` tinyint(2) NOT NULL,
  `is_specialleave` tinyint(2) NOT NULL,
  `is_holiday` tinyint(2) NOT NULL,
  `is_dnd` tinyint(2) NOT NULL COMMENT 'With the voip module this will mean the phone wont ring',
  `multiprivate` varchar(255) NOT NULL,
  `deckm` smallint(3) DEFAULT NULL,
  `note_id` int(11) DEFAULT NULL,
  `external_id` int(11) DEFAULT NULL,
  `dimdim_meeting` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `address_id` (`address_id`),
  KEY `timestamp_start` (`timestamp_start`),
  KEY `timestamp_end` (`timestamp_end`),
  KEY `project_id` (`project_id`),
  KEY `isrecurring` (`isrecurring`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar`
--

LOCK TABLES `calendar` WRITE;
/*!40000 ALTER TABLE `calendar` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_exceptions`
--

DROP TABLE IF EXISTS `calendar_exceptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_exceptions` (
  `calendar_id` int(11) unsigned NOT NULL COMMENT 'id from calendar table',
  `user_id` int(11) unsigned NOT NULL COMMENT 'id from userstable',
  `timestamp_exception` int(11) unsigned NOT NULL COMMENT 'UNIX TIMESTAMP of exception date'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_exceptions`
--

LOCK TABLES `calendar_exceptions` WRITE;
/*!40000 ALTER TABLE `calendar_exceptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_exceptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_external`
--

DROP TABLE IF EXISTS `calendar_external`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_external` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_external`
--

LOCK TABLES `calendar_external` WRITE;
/*!40000 ALTER TABLE `calendar_external` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_external` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_notifications`
--

DROP TABLE IF EXISTS `calendar_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_notifications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `template` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_notifications`
--

LOCK TABLES `calendar_notifications` WRITE;
/*!40000 ALTER TABLE `calendar_notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_old`
--

DROP TABLE IF EXISTS `calendar_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_old` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp_start` int(11) DEFAULT NULL,
  `timestamp_end` int(11) DEFAULT NULL,
  `description` mediumtext,
  `user_id` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `private_id` int(11) NOT NULL DEFAULT '0',
  `is_private` smallint(3) DEFAULT NULL,
  `note_id` int(11) NOT NULL DEFAULT '0',
  `is_important` smallint(3) DEFAULT '0',
  `is_registered` smallint(3) DEFAULT '0',
  `notifytime` int(11) DEFAULT NULL,
  `notified` smallint(3) DEFAULT '0',
  `location` varchar(255) DEFAULT NULL,
  `human_start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `human_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_group` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `kilometers` int(11) DEFAULT NULL,
  `is_repeat` int(11) DEFAULT NULL,
  `multirel` varchar(255) DEFAULT NULL,
  `repeat_type` char(1) DEFAULT NULL,
  `is_alert` smallint(3) DEFAULT '0',
  `is_holiday` smallint(3) DEFAULT '0',
  `is_specialleave` smallint(3) DEFAULT '0',
  `is_ill` smallint(3) DEFAULT '0',
  `e4l_id` varchar(255) DEFAULT NULL,
  `is_dnd` smallint(3) DEFAULT '0',
  `deckm` smallint(3) DEFAULT '0',
  `modified` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `extra_users` varchar(255) DEFAULT NULL,
  `is_event` tinyint(4) DEFAULT '0',
  `multiprivate` varchar(255) NOT NULL,
  `external_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cvd_calendar_user_id` (`user_id`),
  KEY `cvd_calendar_user_timestamp_start` (`timestamp_start`),
  KEY `cvd_calendar_user_timestamp_end` (`timestamp_end`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_old`
--

LOCK TABLES `calendar_old` WRITE;
/*!40000 ALTER TABLE `calendar_old` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_old` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_permissions`
--

DROP TABLE IF EXISTS `calendar_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_id_visitor` int(11) DEFAULT NULL,
  `permissions` varchar(255) DEFAULT 'RO',
  PRIMARY KEY (`id`),
  KEY `cvd_calendar_permissions_user_id` (`user_id`),
  KEY `cvd_calendar_permissions_user_id_visitor` (`user_id_visitor`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_permissions`
--

LOCK TABLES `calendar_permissions` WRITE;
/*!40000 ALTER TABLE `calendar_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_repeats`
--

DROP TABLE IF EXISTS `calendar_repeats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_repeats` (
  `calendar_id` int(11) unsigned NOT NULL,
  `repeat_type` int(3) unsigned DEFAULT NULL,
  `timestamp_end` int(11) DEFAULT NULL,
  `repeat_frequency` int(11) unsigned DEFAULT NULL,
  `repeat_days` char(7) DEFAULT NULL,
  KEY `calendar_id` (`calendar_id`),
  KEY `repeat_type` (`repeat_type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_repeats`
--

LOCK TABLES `calendar_repeats` WRITE;
/*!40000 ALTER TABLE `calendar_repeats` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_repeats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_user`
--

DROP TABLE IF EXISTS `calendar_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_user` (
  `calendar_id` int(11) unsigned NOT NULL COMMENT 'appointment id',
  `user_id` int(11) unsigned NOT NULL COMMENT 'user id as found in the users table',
  `status` int(11) NOT NULL COMMENT 'status of evenvt for this user: 1 for accepted, 2 for rejected, 3 for waiting',
  `google_id` varchar(255) DEFAULT '0',
  KEY `user_id` (`user_id`),
  KEY `calendar_id` (`calendar_id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_user`
--

LOCK TABLES `calendar_user` WRITE;
/*!40000 ALTER TABLE `calendar_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign`
--

DROP TABLE IF EXISTS `campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `classifications` text NOT NULL,
  `datetime` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `tracker_id` int(11) NOT NULL,
  `is_active` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign`
--

LOCK TABLES `campaign` WRITE;
/*!40000 ALTER TABLE `campaign` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_records`
--

DROP TABLE IF EXISTS `campaign_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_records` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `businesscard_id` int(11) NOT NULL,
  `is_called` int(11) NOT NULL,
  `answer` varchar(30) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `note_id` int(11) NOT NULL,
  `email_id` int(11) NOT NULL,
  `appointment_id` int(11) NOT NULL,
  `call_again` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_records`
--

LOCK TABLES `campaign_records` WRITE;
/*!40000 ALTER TABLE `campaign_records` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaign_records` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdr`
--

DROP TABLE IF EXISTS `cdr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cdr` (
  `calldate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `clid` varchar(255) DEFAULT NULL,
  `src` varchar(255) DEFAULT NULL,
  `dst` varchar(255) DEFAULT NULL,
  `dconmediumtext` varchar(255) DEFAULT NULL,
  `channel` varchar(255) DEFAULT NULL,
  `dstchannel` varchar(255) DEFAULT NULL,
  `lastapp` varchar(255) DEFAULT NULL,
  `lastdata` varchar(255) DEFAULT NULL,
  `duration` bigint(20) DEFAULT NULL,
  `billsec` bigint(20) DEFAULT NULL,
  `disposition` varchar(255) DEFAULT NULL,
  `amaflags` bigint(20) DEFAULT NULL,
  `accountcode` varchar(255) DEFAULT NULL,
  `uniqueid` varchar(255) DEFAULT NULL,
  `userfield` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdr`
--

LOCK TABLES `cdr` WRITE;
/*!40000 ALTER TABLE `cdr` DISABLE KEYS */;
/*!40000 ALTER TABLE `cdr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat_rooms`
--

DROP TABLE IF EXISTS `chat_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat_rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `users` mediumtext,
  `topic` varchar(255) DEFAULT NULL,
  `is_active` smallint(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat_rooms`
--

LOCK TABLES `chat_rooms` WRITE;
/*!40000 ALTER TABLE `chat_rooms` DISABLE KEYS */;
/*!40000 ALTER TABLE `chat_rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat_text`
--

DROP TABLE IF EXISTS `chat_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room` int(11) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat_text`
--

LOCK TABLES `chat_text` WRITE;
/*!40000 ALTER TABLE `chat_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `chat_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classification_groups`
--

DROP TABLE IF EXISTS `classification_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classification_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classification_groups`
--

LOCK TABLES `classification_groups` WRITE;
/*!40000 ALTER TABLE `classification_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `classification_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_abbreviations`
--

DROP TABLE IF EXISTS `cms_abbreviations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_abbreviations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `abbreviation` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `lang` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_abbreviations`
--

LOCK TABLES `cms_abbreviations` WRITE;
/*!40000 ALTER TABLE `cms_abbreviations` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_abbreviations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_alias_history`
--

DROP TABLE IF EXISTS `cms_alias_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_alias_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pageid` int(11) NOT NULL,
  `datetime` int(11) NOT NULL,
  `alias` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_alias_history`
--

LOCK TABLES `cms_alias_history` WRITE;
/*!40000 ALTER TABLE `cms_alias_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_alias_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_banner_views`
--

DROP TABLE IF EXISTS `cms_banner_views`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_banner_views` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_id` int(11) NOT NULL,
  `datetime` int(11) NOT NULL,
  `visited` int(11) NOT NULL,
  `clicked` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_banner_views`
--

LOCK TABLES `cms_banner_views` WRITE;
/*!40000 ALTER TABLE `cms_banner_views` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_banner_views` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_banners`
--

DROP TABLE IF EXISTS `cms_banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `image` text,
  `rating` int(11) DEFAULT NULL,
  `url` text,
  `internal_stat` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_banners`
--

LOCK TABLES `cms_banners` WRITE;
/*!40000 ALTER TABLE `cms_banners` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_banners_log`
--

DROP TABLE IF EXISTS `cms_banners_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_banners_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bannerid` int(11) DEFAULT NULL,
  `datetime` int(11) DEFAULT NULL,
  `visitor` varchar(255) DEFAULT NULL,
  `clicked` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bannerid` (`bannerid`),
  KEY `datum` (`datetime`),
  KEY `bezoeker` (`visitor`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_banners_log`
--

LOCK TABLES `cms_banners_log` WRITE;
/*!40000 ALTER TABLE `cms_banners_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_banners_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_banners_summary`
--

DROP TABLE IF EXISTS `cms_banners_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_banners_summary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bannerid` int(11) NOT NULL DEFAULT '0',
  `datum` int(11) NOT NULL DEFAULT '0',
  `bezoekers` int(11) NOT NULL DEFAULT '0',
  `uniek` int(11) NOT NULL DEFAULT '0',
  `kliks` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `bannerid` (`bannerid`),
  KEY `datum` (`datum`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_banners_summary`
--

LOCK TABLES `cms_banners_summary` WRITE;
/*!40000 ALTER TABLE `cms_banners_summary` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_banners_summary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_cache`
--

DROP TABLE IF EXISTS `cms_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_cache` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` int(11) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `data` mediumblob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `timestamp` (`timestamp`),
  KEY `ident` (`ident`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_cache`
--

LOCK TABLES `cms_cache` WRITE;
/*!40000 ALTER TABLE `cms_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_counters`
--

DROP TABLE IF EXISTS `cms_counters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_counters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `counter1` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_counters`
--

LOCK TABLES `cms_counters` WRITE;
/*!40000 ALTER TABLE `cms_counters` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_counters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_data`
--

DROP TABLE IF EXISTS `cms_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentPage` int(11) NOT NULL DEFAULT '0',
  `pageTitle` varchar(255) DEFAULT NULL,
  `pageLabel` varchar(255) DEFAULT NULL,
  `datePublication` int(11) DEFAULT '0',
  `pageData` mediumtext,
  `pageRedirect` varchar(255) DEFAULT NULL,
  `isPublic` tinyint(3) DEFAULT '1',
  `isActive` tinyint(3) DEFAULT '1',
  `isMenuItem` tinyint(3) DEFAULT '1',
  `keywords` varchar(255) DEFAULT NULL,
  `apEnabled` int(11) DEFAULT '0',
  `isTemplate` tinyint(3) DEFAULT '0',
  `isList` tinyint(3) DEFAULT '0',
  `useMetaData` tinyint(3) DEFAULT '0',
  `isSticky` tinyint(3) DEFAULT '0',
  `search_fields` varchar(255) DEFAULT NULL,
  `search_descr` varchar(255) DEFAULT NULL,
  `isForm` tinyint(3) DEFAULT '0',
  `date_start` int(11) DEFAULT '0',
  `date_end` int(11) DEFAULT '0',
  `date_changed` int(11) DEFAULT '0',
  `notifyManager` varchar(50) DEFAULT '0',
  `isGallery` tinyint(3) DEFAULT '0',
  `pageRedirectPopup` tinyint(3) DEFAULT NULL,
  `popup_data` varchar(255) DEFAULT NULL,
  `new_code` mediumtext,
  `new_state` char(2) DEFAULT NULL,
  `search_title` varchar(255) DEFAULT NULL,
  `search_language` varchar(255) DEFAULT NULL,
  `search_override` tinyint(3) DEFAULT NULL,
  `pageAlias` varchar(255) DEFAULT NULL,
  `isSpecial` varchar(1) DEFAULT NULL,
  `date_last_action` int(11) DEFAULT NULL,
  `google_changefreq` varchar(255) DEFAULT 'monthly',
  `google_priority` varchar(255) DEFAULT '0.5',
  `autosave_info` varchar(255) NOT NULL,
  `autosave_data` mediumtext NOT NULL,
  `address_ids` varchar(255) DEFAULT NULL,
  `address_level` tinyint(3) DEFAULT NULL,
  `isProtected` int(11) NOT NULL,
  `useSSL` tinyint(3) NOT NULL,
  `useInternal` tinyint(3) NOT NULL,
  `isFeedback` tinyint(3) DEFAULT NULL,
  `pageHeader` text NOT NULL,
  `autosave_header` text NOT NULL,
  `isShop` tinyint(3) NOT NULL,
  `shopPrice` float(16,2) NOT NULL,
  `isSource` tinyint(3) DEFAULT NULL,
  `conversion_script` text,
  PRIMARY KEY (`id`),
  KEY `parentPage` (`parentPage`),
  KEY `pageTitle` (`pageTitle`),
  KEY `pageLabel` (`pageLabel`),
  KEY `pageData` (`pageData`(255)),
  KEY `search_fields` (`search_fields`),
  KEY `search_descr` (`search_descr`),
  KEY `search_title` (`search_title`),
  KEY `pageAlias` (`pageAlias`),
  FULLTEXT KEY `ftdata` (`pageData`,`pageTitle`,`pageLabel`,`pageAlias`,`search_title`,`search_fields`,`search_descr`,`keywords`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_data`
--

LOCK TABLES `cms_data` WRITE;
/*!40000 ALTER TABLE `cms_data` DISABLE KEYS */;
INSERT INTO `cms_data` VALUES (1,0,'site root','',0,'','',0,0,0,'',1,0,0,0,0,NULL,NULL,0,0,0,0,'',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'R',NULL,'monthly','0.5','','',NULL,NULL,0,0,0,NULL,'','',0,0.00,NULL,NULL),(2,0,'deleted items','',0,NULL,'',1,0,1,'',0,0,0,0,0,NULL,NULL,0,0,0,0,'0',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'D',NULL,'monthly','0.5','','',NULL,NULL,0,0,0,NULL,'','',0,0.00,NULL,NULL),(3,0,'protected items','',0,NULL,'',1,1,1,'',0,0,0,0,0,NULL,NULL,0,0,0,0,'0',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'X',NULL,'monthly','0.5','','',NULL,NULL,0,0,0,NULL,'','',0,0.00,NULL,NULL),(4,1,'Home','0001',1204664400,'<p>Welcome to your new home in Cyberspace.</p><p>If you can see this, your CMS is correctly installed.<br />Login to covide and select the most left button in the top menubar to access the CMS backend.</p>',NULL,1,1,0,NULL,1,0,0,0,0,NULL,NULL,0,0,0,1204664732,'0',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'home',NULL,NULL,'monthly','0.5','','',NULL,NULL,0,0,0,NULL,'','',0,0.00,NULL,NULL);
/*!40000 ALTER TABLE `cms_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_data_revisions`
--

DROP TABLE IF EXISTS `cms_data_revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_data_revisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentPage` int(11) NOT NULL DEFAULT '0',
  `pageTitle` varchar(255) DEFAULT NULL,
  `pageLabel` varchar(255) DEFAULT NULL,
  `datePublication` int(11) DEFAULT '0',
  `pageData` mediumtext,
  `pageRedirect` varchar(255) DEFAULT NULL,
  `isPublic` tinyint(3) DEFAULT '1',
  `isActive` tinyint(3) DEFAULT '1',
  `isMenuItem` tinyint(3) DEFAULT '1',
  `keywords` varchar(255) DEFAULT NULL,
  `apEnabled` int(11) DEFAULT '0',
  `isTemplate` tinyint(3) DEFAULT '0',
  `isList` tinyint(3) DEFAULT '0',
  `useMetaData` tinyint(3) DEFAULT '0',
  `isSticky` tinyint(3) DEFAULT '0',
  `search_fields` varchar(255) DEFAULT NULL,
  `search_descr` varchar(255) DEFAULT NULL,
  `isForm` tinyint(3) DEFAULT '0',
  `date_start` int(11) DEFAULT '0',
  `date_end` int(11) DEFAULT '0',
  `date_changed` int(11) DEFAULT '0',
  `notifyManager` varchar(50) DEFAULT '0',
  `isGallery` tinyint(3) DEFAULT '0',
  `pageRedirectPopup` tinyint(3) DEFAULT NULL,
  `popup_data` varchar(255) DEFAULT NULL,
  `new_code` mediumtext,
  `new_state` char(2) DEFAULT NULL,
  `search_title` varchar(255) DEFAULT NULL,
  `search_language` varchar(255) DEFAULT NULL,
  `search_override` tinyint(3) DEFAULT NULL,
  `pageAlias` varchar(255) DEFAULT NULL,
  `isSpecial` varchar(1) DEFAULT NULL,
  `date_last_action` int(11) DEFAULT NULL,
  `google_changefreq` varchar(255) DEFAULT 'monthly',
  `google_priority` varchar(255) DEFAULT '0.5',
  `autosave_info` varchar(255) NOT NULL,
  `autosave_data` mediumtext NOT NULL,
  `address_ids` varchar(255) DEFAULT NULL,
  `address_level` tinyint(3) DEFAULT NULL,
  `isProtected` int(11) NOT NULL,
  `useSSL` tinyint(3) NOT NULL,
  `useInternal` tinyint(3) NOT NULL,
  `isFeedback` tinyint(3) DEFAULT NULL,
  `pageHeader` text NOT NULL,
  `autosave_header` text NOT NULL,
  `isShop` tinyint(3) NOT NULL,
  `shopPrice` float(16,2) NOT NULL,
  `isSource` tinyint(3) DEFAULT NULL,
  `page_id` int(11) NOT NULL,
  `versiondate` int(11) unsigned NOT NULL,
  `editor` int(11) NOT NULL,
  `conversion_script` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_data_revisions`
--

LOCK TABLES `cms_data_revisions` WRITE;
/*!40000 ALTER TABLE `cms_data_revisions` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_data_revisions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_date`
--

DROP TABLE IF EXISTS `cms_date`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_date` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pageid` int(11) NOT NULL DEFAULT '0',
  `date_begin` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `date_end` int(11) DEFAULT NULL,
  `repeating` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `unique` (`pageid`,`date_begin`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_date`
--

LOCK TABLES `cms_date` WRITE;
/*!40000 ALTER TABLE `cms_date` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_date` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_date_index`
--

DROP TABLE IF EXISTS `cms_date_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_date_index` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pageid` int(11) NOT NULL DEFAULT '0',
  `dateid` int(11) NOT NULL DEFAULT '0',
  `datetime` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `unique` (`pageid`,`datetime`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_date_index`
--

LOCK TABLES `cms_date_index` WRITE;
/*!40000 ALTER TABLE `cms_date_index` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_date_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_feedback`
--

DROP TABLE IF EXISTS `cms_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `datetime` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `is_visitor` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_feedback`
--

LOCK TABLES `cms_feedback` WRITE;
/*!40000 ALTER TABLE `cms_feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_files`
--

DROP TABLE IF EXISTS `cms_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT 'application/octet-stream',
  `size` varchar(255) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_files`
--

LOCK TABLES `cms_files` WRITE;
/*!40000 ALTER TABLE `cms_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_form_results`
--

DROP TABLE IF EXISTS `cms_form_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_form_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pageid` int(11) NOT NULL,
  `field_name` varchar(255) NOT NULL,
  `user_value` varchar(255) NOT NULL,
  `visitor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_form_results`
--

LOCK TABLES `cms_form_results` WRITE;
/*!40000 ALTER TABLE `cms_form_results` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_form_results` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_form_results_visitors`
--

DROP TABLE IF EXISTS `cms_form_results_visitors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_form_results_visitors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pageid` int(11) NOT NULL,
  `visitor_hash` varchar(255) NOT NULL,
  `datetime_start` int(11) NOT NULL,
  `datetime_end` int(11) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_form_results_visitors`
--

LOCK TABLES `cms_form_results_visitors` WRITE;
/*!40000 ALTER TABLE `cms_form_results_visitors` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_form_results_visitors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_form_settings`
--

DROP TABLE IF EXISTS `cms_form_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_form_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pageid` int(11) NOT NULL,
  `mode` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_form_settings`
--

LOCK TABLES `cms_form_settings` WRITE;
/*!40000 ALTER TABLE `cms_form_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_form_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_formsettings`
--

DROP TABLE IF EXISTS `cms_formsettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_formsettings` (
  `form_id` int(11) DEFAULT NULL,
  `settingsname` varchar(255) DEFAULT NULL,
  `settingsvalue` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_formsettings`
--

LOCK TABLES `cms_formsettings` WRITE;
/*!40000 ALTER TABLE `cms_formsettings` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_formsettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_formulieren`
--

DROP TABLE IF EXISTS `cms_formulieren`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_formulieren` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pageid` int(11) NOT NULL DEFAULT '0',
  `field_name` varchar(255) NOT NULL,
  `field_type` varchar(255) NOT NULL,
  `field_value` text NOT NULL,
  `is_required` tinyint(3) NOT NULL DEFAULT '0',
  `is_mailto` tinyint(3) NOT NULL DEFAULT '0',
  `is_mailfrom` tinyint(3) NOT NULL DEFAULT '0',
  `is_mailsubject` tinyint(3) NOT NULL DEFAULT '0',
  `is_redirect` tinyint(3) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_formulieren`
--

LOCK TABLES `cms_formulieren` WRITE;
/*!40000 ALTER TABLE `cms_formulieren` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_formulieren` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_gallery`
--

DROP TABLE IF EXISTS `cms_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pageid` int(11) NOT NULL DEFAULT '0',
  `gallerytype` smallint(3) NOT NULL DEFAULT '0',
  `cols` int(11) NOT NULL DEFAULT '0',
  `rows` int(11) NOT NULL DEFAULT '0',
  `thumbsize` int(11) NOT NULL DEFAULT '0',
  `bigsize` int(11) NOT NULL DEFAULT '0',
  `fullsize` tinyint(3) NOT NULL,
  `font` varchar(50) DEFAULT NULL,
  `font_size` int(11) DEFAULT NULL,
  `last_update` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pageid` (`pageid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_gallery`
--

LOCK TABLES `cms_gallery` WRITE;
/*!40000 ALTER TABLE `cms_gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_gallery_photos`
--

DROP TABLE IF EXISTS `cms_gallery_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_gallery_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pageid` int(11) NOT NULL DEFAULT '0',
  `file` text NOT NULL,
  `description` text NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `cachefile` varchar(255) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `rating` int(11) DEFAULT '0',
  `url` text,
  `internal_stat` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `internal_stat` (`internal_stat`),
  KEY `rating` (`rating`),
  KEY `pageid` (`pageid`)
) ENGINE=MyISAM AUTO_INCREMENT=152 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_gallery_photos`
--

LOCK TABLES `cms_gallery_photos` WRITE;
/*!40000 ALTER TABLE `cms_gallery_photos` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_gallery_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_image_cache`
--

DROP TABLE IF EXISTS `cms_image_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_image_cache` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img_id` int(11) NOT NULL,
  `datetime` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `use_original` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_image_cache`
--

LOCK TABLES `cms_image_cache` WRITE;
/*!40000 ALTER TABLE `cms_image_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_image_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_images`
--

DROP TABLE IF EXISTS `cms_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL DEFAULT '0',
  `path` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_images`
--

LOCK TABLES `cms_images` WRITE;
/*!40000 ALTER TABLE `cms_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_keys`
--

DROP TABLE IF EXISTS `cms_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `crypt_key` varchar(255) NOT NULL,
  `datetime` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_keys`
--

LOCK TABLES `cms_keys` WRITE;
/*!40000 ALTER TABLE `cms_keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_languages`
--

DROP TABLE IF EXISTS `cms_languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) DEFAULT NULL,
  `text_nl` varchar(255) DEFAULT NULL,
  `text_uk` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_languages`
--

LOCK TABLES `cms_languages` WRITE;
/*!40000 ALTER TABLE `cms_languages` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_license`
--

DROP TABLE IF EXISTS `cms_license`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_license` (
  `cms_name` varchar(255) NOT NULL,
  `cms_meta` tinyint(3) NOT NULL DEFAULT '0',
  `cms_license` varchar(255) NOT NULL,
  `cms_date` int(11) NOT NULL DEFAULT '0',
  `cms_external` text,
  `search_fields` text,
  `search_descr` varchar(255) DEFAULT NULL,
  `cms_forms` tinyint(3) NOT NULL DEFAULT '0',
  `cms_list` tinyint(3) NOT NULL DEFAULT '0',
  `cms_linkchecker_url` varchar(255) DEFAULT NULL,
  `search_author` varchar(255) DEFAULT NULL,
  `search_copyright` varchar(255) DEFAULT NULL,
  `search_email` varchar(255) DEFAULT NULL,
  `cms_changelist` tinyint(3) NOT NULL DEFAULT '0',
  `cms_banners` tinyint(3) NOT NULL DEFAULT '0',
  `cms_searchengine` tinyint(3) NOT NULL DEFAULT '0',
  `db_version` int(11) NOT NULL DEFAULT '0',
  `cms_gallery` tinyint(3) NOT NULL DEFAULT '0',
  `website_url` text,
  `site_stylesheet` text,
  `cms_versioncontrol` tinyint(3) DEFAULT NULL,
  `search_use_pagetitle` tinyint(3) DEFAULT NULL,
  `search_language` varchar(255) DEFAULT NULL,
  `cms_page_elements` tinyint(3) NOT NULL DEFAULT '1',
  `cms_permissions` tinyint(3) NOT NULL DEFAULT '1',
  `multiple_sitemaps` tinyint(3) DEFAULT NULL,
  `google_verify` varchar(255) DEFAULT NULL,
  `cms_linkchecker` tinyint(3) NOT NULL,
  `cms_hostnames` text NOT NULL,
  `cms_defaultpage` int(11) NOT NULL,
  `cms_favicon` varchar(255) DEFAULT NULL,
  `cms_logo` varchar(255) DEFAULT NULL,
  `cms_mailings` tinyint(3) DEFAULT NULL,
  `cms_protected` tinyint(3) DEFAULT NULL,
  `cms_address` tinyint(3) DEFAULT NULL,
  `cms_feedback` tinyint(3) DEFAULT NULL,
  `cms_user_register` tinyint(3) DEFAULT NULL,
  `cms_manage_hostname` varchar(255) DEFAULT NULL,
  `google_analytics` varchar(255) NOT NULL,
  `letsstat_analytics` varchar(255) NOT NULL,
  `cms_shop` tinyint(3) NOT NULL,
  `cms_shop_page` int(11) NOT NULL,
  `cms_shop_results` int(11) NOT NULL,
  `cms_use_strict_mode` tinyint(3) NOT NULL,
  `ideal_type` varchar(50) NOT NULL,
  `ideal_merchant_id` varchar(50) NOT NULL,
  `ideal_last_order` int(11) NOT NULL,
  `ideal_currency` varchar(10) NOT NULL,
  `ideal_secret_key` varchar(255) NOT NULL,
  `ideal_test_mode` tinyint(3) NOT NULL,
  `yahoo_key` varchar(255) DEFAULT NULL,
  `custom_401` int(11) NOT NULL,
  `custom_feedback` int(11) NOT NULL,
  `custom_403` int(11) NOT NULL,
  `custom_404` int(11) NOT NULL,
  `custom_602` int(11) NOT NULL,
  `custom_shop_cancel` int(11) NOT NULL,
  `custom_shop_error` int(11) NOT NULL,
  `custom_loginprofile` int(11) NOT NULL,
  `piwik_analytics` int(11) NOT NULL,
  `recaptcha_private` varchar(255) NOT NULL,
  `recaptcha_public` varchar(255) NOT NULL,
  `recaptcha_theme` varchar(255) NOT NULL,
  `bing_key` varchar(255) NOT NULL,
  PRIMARY KEY (`cms_license`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_license`
--

LOCK TABLES `cms_license` WRITE;
/*!40000 ALTER TABLE `cms_license` DISABLE KEYS */;
INSERT INTO `cms_license` VALUES ('Welcome to your new home in cyberspace',0,'covide',0,NULL,'Covide, CRM, CMS, Groupware, VoIP','Covide combines great Groupware (shared email, calendars, files) and CRM (sales and support) in CRM-groupware. The most efficient way to work together. Integrate it with VoIP PBX Asterisk and OpenOffice and you can create a complete Virtual Office.',0,0,'','Covide','Covide','',0,0,0,0,0,'','',0,0,'on',0,0,0,'',0,'localhost',4,'','',NULL,NULL,NULL,NULL,NULL,'','','',0,0,0,0,'','',0,'','',0,'',0,0,0,0,0,0,0,0,0,'','','','');
/*!40000 ALTER TABLE `cms_license` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_license_siteroots`
--

DROP TABLE IF EXISTS `cms_license_siteroots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_license_siteroots` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pageid` int(11) NOT NULL,
  `search_fields` text,
  `search_descr` varchar(255) DEFAULT NULL,
  `search_author` varchar(255) DEFAULT NULL,
  `search_copyright` varchar(255) DEFAULT NULL,
  `search_email` varchar(255) DEFAULT NULL,
  `search_use_pagetitle` tinyint(3) DEFAULT NULL,
  `search_language` varchar(255) DEFAULT NULL,
  `google_verify` varchar(255) DEFAULT NULL,
  `cms_hostnames` text NOT NULL,
  `cms_defaultpage` int(11) NOT NULL,
  `cms_name` varchar(255) NOT NULL,
  `cms_favicon` varchar(255) NOT NULL,
  `cms_logo` varchar(255) NOT NULL,
  `google_analytics` varchar(255) NOT NULL,
  `letsstat_analytics` varchar(255) NOT NULL,
  `yahoo_key` varchar(255) DEFAULT NULL,
  `custom_401` int(11) NOT NULL,
  `custom_feedback` int(11) NOT NULL,
  `custom_403` int(11) NOT NULL,
  `custom_404` int(11) NOT NULL,
  `custom_602` int(11) NOT NULL,
  `custom_loginprofile` varchar(255) DEFAULT NULL,
  `piwik_analytics` int(11) NOT NULL,
  `recaptcha_private` varchar(255) NOT NULL,
  `recaptcha_public` varchar(255) NOT NULL,
  `recaptcha_theme` varchar(255) NOT NULL,
  `bing_key` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_license_siteroots`
--

LOCK TABLES `cms_license_siteroots` WRITE;
/*!40000 ALTER TABLE `cms_license_siteroots` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_license_siteroots` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_list`
--

DROP TABLE IF EXISTS `cms_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pageid` int(11) NOT NULL DEFAULT '0',
  `query` text NOT NULL,
  `fields` text NOT NULL,
  `order` varchar(255) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `listposition` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pageid` (`pageid`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_list`
--

LOCK TABLES `cms_list` WRITE;
/*!40000 ALTER TABLE `cms_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_login_log`
--

DROP TABLE IF EXISTS `cms_login_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_login_log` (
  `user_id` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `uri` text,
  `timestamp` int(11) DEFAULT NULL,
  `remote_addr` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_login_log`
--

LOCK TABLES `cms_login_log` WRITE;
/*!40000 ALTER TABLE `cms_login_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_login_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_logins_log`
--

DROP TABLE IF EXISTS `cms_logins_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_logins_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0',
  `datetime` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=248 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_logins_log`
--

LOCK TABLES `cms_logins_log` WRITE;
/*!40000 ALTER TABLE `cms_logins_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_logins_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_mailings`
--

DROP TABLE IF EXISTS `cms_mailings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_mailings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pageid` int(11) NOT NULL,
  `datetime` int(11) NOT NULL,
  `email` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_mailings`
--

LOCK TABLES `cms_mailings` WRITE;
/*!40000 ALTER TABLE `cms_mailings` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_mailings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_metadata`
--

DROP TABLE IF EXISTS `cms_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pageid` int(11) NOT NULL DEFAULT '0',
  `fieldid` int(11) NOT NULL DEFAULT '0',
  `value` text NOT NULL,
  `isDefault` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `pageid` (`pageid`),
  KEY `fieldid` (`fieldid`),
  KEY `value` (`value`(255))
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_metadata`
--

LOCK TABLES `cms_metadata` WRITE;
/*!40000 ALTER TABLE `cms_metadata` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_metadef`
--

DROP TABLE IF EXISTS `cms_metadef`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_metadef` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(255) NOT NULL,
  `field_type` varchar(20) NOT NULL,
  `field_value` text NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `group` varchar(255) DEFAULT NULL,
  `fphide` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_metadef`
--

LOCK TABLES `cms_metadef` WRITE;
/*!40000 ALTER TABLE `cms_metadef` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_metadef` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_permissions`
--

DROP TABLE IF EXISTS `cms_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0',
  `uid` varchar(50) NOT NULL DEFAULT '0',
  `editRight` int(11) NOT NULL DEFAULT '0',
  `viewRight` int(11) NOT NULL DEFAULT '0',
  `deleteRight` int(11) NOT NULL DEFAULT '0',
  `manageRight` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=330 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_permissions`
--

LOCK TABLES `cms_permissions` WRITE;
/*!40000 ALTER TABLE `cms_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_siteviews`
--

DROP TABLE IF EXISTS `cms_siteviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_siteviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `view` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_siteviews`
--

LOCK TABLES `cms_siteviews` WRITE;
/*!40000 ALTER TABLE `cms_siteviews` DISABLE KEYS */;
INSERT INTO `cms_siteviews` VALUES (2,2,'a:4:{s:3:\"cmd\";s:0:\"\";s:8:\"siteroot\";s:1:\"R\";s:6:\"buffer\";a:0:{}s:9:\"toonpages\";a:0:{}}');
/*!40000 ALTER TABLE `cms_siteviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_temp`
--

DROP TABLE IF EXISTS `cms_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userkey` varchar(255) NOT NULL,
  `ids` text NOT NULL,
  `datetime` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userkey` (`userkey`)
) ENGINE=MyISAM AUTO_INCREMENT=113 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_temp`
--

LOCK TABLES `cms_temp` WRITE;
/*!40000 ALTER TABLE `cms_temp` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_temp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_templates`
--

DROP TABLE IF EXISTS `cms_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `data` longtext,
  `category` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_templates`
--

LOCK TABLES `cms_templates` WRITE;
/*!40000 ALTER TABLE `cms_templates` DISABLE KEYS */;
INSERT INTO `cms_templates` VALUES (10,'main template','<?php\r\n// load default stylesheet\r\n$template->load_css(11);\r\n// start html etc including full head information\r\n$template->start_html(1);\r\n// echo pagetitle in <h1> tag\r\n$template->getPageTitle($template->pageid);\r\n// echo page content\r\n$template->getPageData();\r\n// close page\r\n$template->getPageFooter();\r\n// close html and flush to client\r\n$template->end_html();\r\n?>','main'),(11,'Main CSS','html, body {\r\n	background-color: #FFFFFF;\r\n	color: #000000;\r\n}','css');
/*!40000 ALTER TABLE `cms_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cms_users`
--

DROP TABLE IF EXISTS `cms_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cms_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_enabled` tinyint(3) NOT NULL,
  `email` varchar(255) NOT NULL,
  `registration_date` int(11) NOT NULL,
  `is_active` tinyint(3) NOT NULL,
  `confirm_hash` varchar(255) DEFAULT NULL,
  `address_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cms_users`
--

LOCK TABLES `cms_users` WRITE;
/*!40000 ALTER TABLE `cms_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `cms_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dimdim`
--

DROP TABLE IF EXISTS `dimdim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dimdim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `room` varchar(255) NOT NULL,
  `attendees` varchar(255) NOT NULL,
  `external_attendees` varchar(255) NOT NULL,
  `startdate` datetime NOT NULL,
  `enddate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dimdim`
--

LOCK TABLES `dimdim` WRITE;
/*!40000 ALTER TABLE `dimdim` DISABLE KEYS */;
/*!40000 ALTER TABLE `dimdim` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees_info`
--

DROP TABLE IF EXISTS `employees_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `social_security_nr` varchar(255) DEFAULT NULL,
  `timestamp_started` int(11) DEFAULT NULL,
  `timestamp_birthday` int(11) DEFAULT NULL,
  `gender` smallint(3) NOT NULL DEFAULT '0',
  `contract_type` mediumtext,
  `function` varchar(255) DEFAULT NULL,
  `functionlevel` varchar(255) DEFAULT NULL,
  `contract_hours` int(11) DEFAULT NULL,
  `contract_holidayhours` int(11) DEFAULT NULL,
  `timestamp_stop` int(11) DEFAULT NULL,
  `evaluation` mediumtext,
  `gross_wage` decimal(16,2) DEFAULT NULL,
  `overhead` int(11) DEFAULT NULL,
  `kilometer_allowance` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees_info`
--

LOCK TABLES `employees_info` WRITE;
/*!40000 ALTER TABLE `employees_info` DISABLE KEYS */;
INSERT INTO `employees_info` VALUES (1,2,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `employees_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exactonline_settings`
--

DROP TABLE IF EXISTS `exactonline_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exactonline_settings` (
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `partnerkey` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exactonline_settings`
--

LOCK TABLES `exactonline_settings` WRITE;
/*!40000 ALTER TABLE `exactonline_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `exactonline_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq`
--

DROP TABLE IF EXISTS `faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `question` mediumtext,
  `answer` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq`
--

LOCK TABLES `faq` WRITE;
/*!40000 ALTER TABLE `faq` DISABLE KEYS */;
/*!40000 ALTER TABLE `faq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq_cat`
--

DROP TABLE IF EXISTS `faq_cat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faq_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq_cat`
--

LOCK TABLES `faq_cat` WRITE;
/*!40000 ALTER TABLE `faq_cat` DISABLE KEYS */;
/*!40000 ALTER TABLE `faq_cat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faxes`
--

DROP TABLE IF EXISTS `faxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` int(11) DEFAULT NULL,
  `sender` varchar(255) DEFAULT NULL,
  `receiver` varchar(255) DEFAULT NULL,
  `relation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cvd_faxes_relation_id` (`relation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faxes`
--

LOCK TABLES `faxes` WRITE;
/*!40000 ALTER TABLE `faxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `faxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filesys_files`
--

DROP TABLE IF EXISTS `filesys_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filesys_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `folder_id` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `timestamp` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  PRIMARY KEY (`id`),
  KEY `cvd_filesys_files_folder_id` (`folder_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filesys_files`
--

LOCK TABLES `filesys_files` WRITE;
/*!40000 ALTER TABLE `filesys_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `filesys_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filesys_folders`
--

DROP TABLE IF EXISTS `filesys_folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filesys_folders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `is_public` smallint(3) NOT NULL DEFAULT '0',
  `is_relation` smallint(3) NOT NULL DEFAULT '0',
  `address_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_shared` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  `hrm_id` int(11) DEFAULT NULL,
  `sticky` smallint(3) DEFAULT '0',
  `project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cvd_filesys_folders_parent_id` (`parent_id`)
) ENGINE=MyISAM AUTO_INCREMENT=113 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filesys_folders`
--

LOCK TABLES `filesys_folders` WRITE;
/*!40000 ALTER TABLE `filesys_folders` DISABLE KEYS */;
INSERT INTO `filesys_folders` VALUES (3,'openbare mappen',1,0,NULL,NULL,0,NULL,NULL,NULL,0,0),(12,'initX',1,1,8,NULL,4,NULL,NULL,NULL,1,0),(23,'mijn documenten',0,0,NULL,1,0,NULL,NULL,NULL,0,0),(20,'medewerkers',1,0,NULL,NULL,19,NULL,NULL,NULL,1,0),(21,'oud-medewerkers',1,0,NULL,NULL,19,NULL,NULL,NULL,1,0),(24,'projecten',1,0,NULL,NULL,0,NULL,NULL,NULL,1,0),(19,'hrm',1,0,NULL,NULL,0,NULL,NULL,NULL,1,0),(4,'relaties',1,0,NULL,NULL,0,NULL,NULL,NULL,1,0),(22,'covide',1,0,NULL,NULL,20,NULL,NULL,2,0,0),(108,'mijn documenten',0,0,NULL,2,0,NULL,NULL,NULL,0,0),(109,'mijn mappen',0,0,NULL,NULL,0,NULL,NULL,NULL,0,NULL),(110,'google mappen',0,0,NULL,NULL,0,NULL,NULL,NULL,0,NULL),(112,'cms',0,0,NULL,NULL,0,NULL,NULL,NULL,0,NULL);
/*!40000 ALTER TABLE `filesys_folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filesys_permissions`
--

DROP TABLE IF EXISTS `filesys_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filesys_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folder_id` int(11) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `permissions` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cvd_filesys_permissions_user_id` (`user_id`),
  KEY `cvd_filesys_permissions_folder_id` (`folder_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filesys_permissions`
--

LOCK TABLES `filesys_permissions` WRITE;
/*!40000 ALTER TABLE `filesys_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `filesys_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance_akties`
--

DROP TABLE IF EXISTS `finance_akties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_akties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) DEFAULT NULL,
  `omschrijving` mediumtext,
  `datum` int(11) DEFAULT NULL,
  `rekeningflow` decimal(16,2) DEFAULT NULL,
  `factuur_nr` int(11) DEFAULT NULL,
  `rekeningflow_btw` decimal(16,2) DEFAULT NULL,
  `grootboeknummer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance_akties`
--

LOCK TABLES `finance_akties` WRITE;
/*!40000 ALTER TABLE `finance_akties` DISABLE KEYS */;
/*!40000 ALTER TABLE `finance_akties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance_begin_standen_finance`
--

DROP TABLE IF EXISTS `finance_begin_standen_finance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_begin_standen_finance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grootboek_id` int(11) NOT NULL DEFAULT '0',
  `stand` decimal(16,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance_begin_standen_finance`
--

LOCK TABLES `finance_begin_standen_finance` WRITE;
/*!40000 ALTER TABLE `finance_begin_standen_finance` DISABLE KEYS */;
INSERT INTO `finance_begin_standen_finance` VALUES (1,1000,'0.00'),(2,1100,'0.00');
/*!40000 ALTER TABLE `finance_begin_standen_finance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance_boekingen`
--

DROP TABLE IF EXISTS `finance_boekingen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_boekingen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `credit` smallint(3) NOT NULL DEFAULT '0',
  `factuur` int(11) NOT NULL DEFAULT '0',
  `grootboek_id` int(11) DEFAULT NULL,
  `status` smallint(3) DEFAULT NULL,
  `datum` int(11) DEFAULT NULL,
  `koppel_id` int(11) DEFAULT NULL,
  `bedrag` decimal(16,2) DEFAULT NULL,
  `product` int(11) DEFAULT NULL,
  `inkoop` smallint(3) NOT NULL DEFAULT '0',
  `deb_nr` int(11) NOT NULL DEFAULT '0',
  `betaald` smallint(3) NOT NULL DEFAULT '0',
  `locked` smallint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance_boekingen`
--

LOCK TABLES `finance_boekingen` WRITE;
/*!40000 ALTER TABLE `finance_boekingen` DISABLE KEYS */;
/*!40000 ALTER TABLE `finance_boekingen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance_boekingen_20012003`
--

DROP TABLE IF EXISTS `finance_boekingen_20012003`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_boekingen_20012003` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `credit` smallint(3) NOT NULL DEFAULT '0',
  `factuur` int(11) NOT NULL DEFAULT '0',
  `grootboek_id` int(11) DEFAULT NULL,
  `status` smallint(3) DEFAULT NULL,
  `datum` int(11) DEFAULT NULL,
  `koppel_id` int(11) DEFAULT NULL,
  `bedrag` decimal(16,2) DEFAULT NULL,
  `product` int(11) DEFAULT NULL,
  `inkoop` smallint(3) NOT NULL DEFAULT '0',
  `deb_nr` int(11) NOT NULL DEFAULT '0',
  `betaald` smallint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance_boekingen_20012003`
--

LOCK TABLES `finance_boekingen_20012003` WRITE;
/*!40000 ALTER TABLE `finance_boekingen_20012003` DISABLE KEYS */;
/*!40000 ALTER TABLE `finance_boekingen_20012003` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance_grootboeknummers`
--

DROP TABLE IF EXISTS `finance_grootboeknummers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_grootboeknummers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nr` int(11) DEFAULT NULL,
  `titel` varchar(255) DEFAULT NULL,
  `debiteur` smallint(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance_grootboeknummers`
--

LOCK TABLES `finance_grootboeknummers` WRITE;
/*!40000 ALTER TABLE `finance_grootboeknummers` DISABLE KEYS */;
/*!40000 ALTER TABLE `finance_grootboeknummers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance_inkopen`
--

DROP TABLE IF EXISTS `finance_inkopen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_inkopen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datum` int(11) NOT NULL DEFAULT '0',
  `balanspost` int(11) NOT NULL DEFAULT '0',
  `boekstuknr` int(11) NOT NULL DEFAULT '1',
  `descr` varchar(255) DEFAULT NULL,
  `leverancier_nr` int(11) NOT NULL DEFAULT '0',
  `bedrag_ex` decimal(16,2) DEFAULT NULL,
  `bedrag_inc` decimal(16,2) DEFAULT NULL,
  `bedrag_btw` decimal(16,2) DEFAULT NULL,
  `betaald` decimal(16,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance_inkopen`
--

LOCK TABLES `finance_inkopen` WRITE;
/*!40000 ALTER TABLE `finance_inkopen` DISABLE KEYS */;
/*!40000 ALTER TABLE `finance_inkopen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance_jaar_afsluitingen`
--

DROP TABLE IF EXISTS `finance_jaar_afsluitingen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_jaar_afsluitingen` (
  `jaar` int(11) NOT NULL DEFAULT '0',
  `datum_afgesloten` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance_jaar_afsluitingen`
--

LOCK TABLES `finance_jaar_afsluitingen` WRITE;
/*!40000 ALTER TABLE `finance_jaar_afsluitingen` DISABLE KEYS */;
/*!40000 ALTER TABLE `finance_jaar_afsluitingen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance_klanten`
--

DROP TABLE IF EXISTS `finance_klanten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_klanten` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naam` varchar(255) DEFAULT NULL,
  `adres` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `land` varchar(255) DEFAULT NULL,
  `telefoonnummer` varchar(255) DEFAULT NULL,
  `faxnummer` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `soortbedrijf_id` int(11) DEFAULT NULL,
  `aantalwerknemers` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `contactpersoon` varchar(255) DEFAULT NULL,
  `contactpersoon_voorletters` varchar(255) DEFAULT NULL,
  `totaal_flow` int(11) DEFAULT NULL,
  `totaal_flow_12` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance_klanten`
--

LOCK TABLES `finance_klanten` WRITE;
/*!40000 ALTER TABLE `finance_klanten` DISABLE KEYS */;
/*!40000 ALTER TABLE `finance_klanten` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance_offertes`
--

DROP TABLE IF EXISTS `finance_offertes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_offertes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) DEFAULT NULL,
  `titel` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `uitvoerder` varchar(255) DEFAULT NULL,
  `producten_id_0` varchar(255) DEFAULT NULL,
  `producten_id_1` varchar(255) DEFAULT NULL,
  `producten_id_2` varchar(255) DEFAULT NULL,
  `producten_id_3` varchar(255) DEFAULT NULL,
  `html_0` mediumtext,
  `html_1` mediumtext,
  `html_2` mediumtext,
  `html_3` mediumtext,
  `datum_0` varchar(255) DEFAULT NULL,
  `datum_1` varchar(255) DEFAULT NULL,
  `datum_2` varchar(255) DEFAULT NULL,
  `datum_3` varchar(255) DEFAULT NULL,
  `bedrijfsnaam` varchar(255) DEFAULT NULL,
  `prec_betaald_0` int(11) DEFAULT NULL,
  `prec_betaald_1` int(11) DEFAULT NULL,
  `prec_betaald_2` int(11) DEFAULT NULL,
  `prec_betaald_3` int(11) DEFAULT NULL,
  `factuur_nr_0` int(11) DEFAULT NULL,
  `factuur_nr_1` int(11) DEFAULT NULL,
  `factuur_nr_2` int(11) DEFAULT NULL,
  `factuur_nr_3` int(11) DEFAULT NULL,
  `btw_tonen` smallint(3) DEFAULT NULL,
  `btw_prec` decimal(10,0) DEFAULT NULL,
  `factuur_nr` int(11) DEFAULT NULL,
  `datum` varchar(255) DEFAULT NULL,
  `definitief_2` smallint(3) NOT NULL DEFAULT '0',
  `definitief_3` smallint(3) NOT NULL DEFAULT '0',
  `template_id` int(11) NOT NULL,
  `font` varchar(255) NOT NULL,
  `fontsize` int(11) NOT NULL,
  `template_setting` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `bcard_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance_offertes`
--

LOCK TABLES `finance_offertes` WRITE;
/*!40000 ALTER TABLE `finance_offertes` DISABLE KEYS */;
/*!40000 ALTER TABLE `finance_offertes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance_omzet_akties`
--

DROP TABLE IF EXISTS `finance_omzet_akties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_omzet_akties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) DEFAULT NULL,
  `omschrijving` mediumtext,
  `datum` int(11) DEFAULT NULL,
  `datum_betaald` int(11) DEFAULT NULL,
  `rekeningflow` decimal(16,2) DEFAULT NULL,
  `rekeningflow_btw` decimal(16,2) DEFAULT NULL,
  `rekeningflow_ex` decimal(16,2) DEFAULT NULL,
  `factuur_nr` int(11) DEFAULT NULL,
  `grootboeknummer_id` int(11) DEFAULT NULL,
  `bedrag_betaald` decimal(16,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance_omzet_akties`
--

LOCK TABLES `finance_omzet_akties` WRITE;
/*!40000 ALTER TABLE `finance_omzet_akties` DISABLE KEYS */;
/*!40000 ALTER TABLE `finance_omzet_akties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance_omzet_totaal`
--

DROP TABLE IF EXISTS `finance_omzet_totaal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_omzet_totaal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_id` int(11) NOT NULL DEFAULT '0',
  `totaal_flow` int(11) DEFAULT NULL,
  `totaal_flow_btw` decimal(16,2) DEFAULT NULL,
  `totaal_flow_ex` decimal(16,2) DEFAULT NULL,
  `totaal_flow_12` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance_omzet_totaal`
--

LOCK TABLES `finance_omzet_totaal` WRITE;
/*!40000 ALTER TABLE `finance_omzet_totaal` DISABLE KEYS */;
/*!40000 ALTER TABLE `finance_omzet_totaal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance_overige_posten`
--

DROP TABLE IF EXISTS `finance_overige_posten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_overige_posten` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grootboek_id` int(11) NOT NULL DEFAULT '0',
  `omschrijving` mediumtext NOT NULL,
  `debiteur` int(11) NOT NULL DEFAULT '0',
  `datum` int(11) NOT NULL DEFAULT '0',
  `bedrag` decimal(16,2) NOT NULL DEFAULT '0.00',
  `tegenrekening` int(11) NOT NULL DEFAULT '59',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance_overige_posten`
--

LOCK TABLES `finance_overige_posten` WRITE;
/*!40000 ALTER TABLE `finance_overige_posten` DISABLE KEYS */;
/*!40000 ALTER TABLE `finance_overige_posten` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance_producten`
--

DROP TABLE IF EXISTS `finance_producten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_producten` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titel` varchar(255) DEFAULT NULL,
  `html` mediumtext,
  `prijsperjaar` smallint(3) DEFAULT NULL,
  `categorie` varchar(255) DEFAULT NULL,
  `grootboeknummer_id` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `prijs` decimal(10,2) DEFAULT NULL,
  `btw_prec` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance_producten`
--

LOCK TABLES `finance_producten` WRITE;
/*!40000 ALTER TABLE `finance_producten` DISABLE KEYS */;
/*!40000 ALTER TABLE `finance_producten` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance_producten_in_offertes`
--

DROP TABLE IF EXISTS `finance_producten_in_offertes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_producten_in_offertes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producten_id` int(11) DEFAULT NULL,
  `omschrijving` mediumtext,
  `link_id` int(11) DEFAULT NULL,
  `aantal` int(11) DEFAULT NULL,
  `btw` decimal(10,0) DEFAULT NULL,
  `prijs` decimal(16,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance_producten_in_offertes`
--

LOCK TABLES `finance_producten_in_offertes` WRITE;
/*!40000 ALTER TABLE `finance_producten_in_offertes` DISABLE KEYS */;
/*!40000 ALTER TABLE `finance_producten_in_offertes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance_relatie_type`
--

DROP TABLE IF EXISTS `finance_relatie_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_relatie_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance_relatie_type`
--

LOCK TABLES `finance_relatie_type` WRITE;
/*!40000 ALTER TABLE `finance_relatie_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `finance_relatie_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance_soortbedrijf`
--

DROP TABLE IF EXISTS `finance_soortbedrijf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_soortbedrijf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `omschrijving` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance_soortbedrijf`
--

LOCK TABLES `finance_soortbedrijf` WRITE;
/*!40000 ALTER TABLE `finance_soortbedrijf` DISABLE KEYS */;
/*!40000 ALTER TABLE `finance_soortbedrijf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `finance_teksten`
--

DROP TABLE IF EXISTS `finance_teksten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finance_teksten` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `html` mediumtext,
  `description` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finance_teksten`
--

LOCK TABLES `finance_teksten` WRITE;
/*!40000 ALTER TABLE `finance_teksten` DISABLE KEYS */;
INSERT INTO `finance_teksten` VALUES (1,'14','betaling binnen',1),(8,'root@localhost','email',1),(6,'Betaling altijd strikt binnen&nbsp;<STRONG>14</STRONG> dagen.<BR>Bij niet tijdige betaling kan u rente en kosten in rekening worden gebracht.<BR>Op al onze leveringen zijn onze Algemene Voorwaarden van toepassing.','betaling',0),(7,'1111.22.333.a.44 ','btw nummer',1),(2,'1','laatste factuur nr',1);
/*!40000 ALTER TABLE `finance_teksten` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum`
--

DROP TABLE IF EXISTS `forum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `timestamp` int(11) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` mediumtext,
  `read` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum`
--

LOCK TABLES `forum` WRITE;
/*!40000 ALTER TABLE `forum` DISABLE KEYS */;
/*!40000 ALTER TABLE `forum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funambol_address_sync`
--

DROP TABLE IF EXISTS `funambol_address_sync`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funambol_address_sync` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) NOT NULL,
  `address_table` varchar(255) NOT NULL,
  `address_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `file_hash` varchar(255) NOT NULL,
  `datetime` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funambol_address_sync`
--

LOCK TABLES `funambol_address_sync` WRITE;
/*!40000 ALTER TABLE `funambol_address_sync` DISABLE KEYS */;
/*!40000 ALTER TABLE `funambol_address_sync` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funambol_calendar_sync`
--

DROP TABLE IF EXISTS `funambol_calendar_sync`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funambol_calendar_sync` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) NOT NULL,
  `calendar_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `file_hash` varchar(255) NOT NULL,
  `datetime` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `calendar_id` (`calendar_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funambol_calendar_sync`
--

LOCK TABLES `funambol_calendar_sync` WRITE;
/*!40000 ALTER TABLE `funambol_calendar_sync` DISABLE KEYS */;
/*!40000 ALTER TABLE `funambol_calendar_sync` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funambol_file_sync`
--

DROP TABLE IF EXISTS `funambol_file_sync`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funambol_file_sync` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) NOT NULL,
  `file_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `file_hash` varchar(255) NOT NULL,
  `datetime` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funambol_file_sync`
--

LOCK TABLES `funambol_file_sync` WRITE;
/*!40000 ALTER TABLE `funambol_file_sync` DISABLE KEYS */;
/*!40000 ALTER TABLE `funambol_file_sync` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funambol_stats`
--

DROP TABLE IF EXISTS `funambol_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funambol_stats` (
  `source` varchar(255) NOT NULL,
  `lasthash` varchar(255) NOT NULL,
  `synchash` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funambol_stats`
--

LOCK TABLES `funambol_stats` WRITE;
/*!40000 ALTER TABLE `funambol_stats` DISABLE KEYS */;
/*!40000 ALTER TABLE `funambol_stats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `funambol_todo_sync`
--

DROP TABLE IF EXISTS `funambol_todo_sync`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `funambol_todo_sync` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` varchar(255) NOT NULL,
  `todo_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `file_hash` varchar(255) NOT NULL,
  `datetime` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `funambol_todo_sync`
--

LOCK TABLES `funambol_todo_sync` WRITE;
/*!40000 ALTER TABLE `funambol_todo_sync` DISABLE KEYS */;
/*!40000 ALTER TABLE `funambol_todo_sync` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `functions`
--

DROP TABLE IF EXISTS `functions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `functions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `functions`
--

LOCK TABLES `functions` WRITE;
/*!40000 ALTER TABLE `functions` DISABLE KEYS */;
/*!40000 ALTER TABLE `functions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `google_address_sync`
--

DROP TABLE IF EXISTS `google_address_sync`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_address_sync` (
  `address_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `google_id` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `google_address_sync`
--

LOCK TABLES `google_address_sync` WRITE;
/*!40000 ALTER TABLE `google_address_sync` DISABLE KEYS */;
/*!40000 ALTER TABLE `google_address_sync` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hours_activities`
--

DROP TABLE IF EXISTS `hours_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hours_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity` varchar(255) DEFAULT NULL,
  `tarif` decimal(16,2) DEFAULT NULL,
  `department_id` int(11) DEFAULT '0',
  `purchase` decimal(16,2) DEFAULT NULL,
  `marge` decimal(16,2) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hours_activities`
--

LOCK TABLES `hours_activities` WRITE;
/*!40000 ALTER TABLE `hours_activities` DISABLE KEYS */;
/*!40000 ALTER TABLE `hours_activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hours_activities_groups`
--

DROP TABLE IF EXISTS `hours_activities_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hours_activities_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hours_activities_groups`
--

LOCK TABLES `hours_activities_groups` WRITE;
/*!40000 ALTER TABLE `hours_activities_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `hours_activities_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hours_registration`
--

DROP TABLE IF EXISTS `hours_registration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hours_registration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `timestamp_start` int(11) DEFAULT NULL,
  `timestamp_end` int(11) DEFAULT NULL,
  `activity_id` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  `is_billable` smallint(3) DEFAULT '0',
  `type` smallint(3) DEFAULT '0',
  `hours` decimal(16,2) DEFAULT NULL,
  `price` float(16,2) DEFAULT NULL,
  `overtime` int(11) DEFAULT '0',
  `marge` decimal(16,2) DEFAULT NULL,
  `tarif` decimal(16,2) DEFAULT NULL,
  `purchase` decimal(16,2) DEFAULT NULL,
  `date` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cvd_hours_registration_user_id` (`user_id`),
  KEY `cvd_hours_registration_project_id` (`project_id`),
  KEY `cvd_hours_registration_activity_id` (`activity_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hours_registration`
--

LOCK TABLES `hours_registration` WRITE;
/*!40000 ALTER TABLE `hours_registration` DISABLE KEYS */;
/*!40000 ALTER TABLE `hours_registration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `issues`
--

DROP TABLE IF EXISTS `issues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` int(11) DEFAULT NULL,
  `description` mediumtext,
  `solution` mediumtext,
  `project_id` int(11) DEFAULT NULL,
  `registering_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `priority` smallint(3) DEFAULT '0',
  `is_solved` smallint(3) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `email` mediumtext,
  `reference_nr` varchar(255) DEFAULT NULL,
  `execution_time` int(11) NOT NULL,
  `remarks` mediumtext NOT NULL,
  `supplies` text,
  `arrival` int(11) DEFAULT NULL,
  `departure` int(11) DEFAULT NULL,
  `internal_notes` text,
  `bcard_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cvd_issues_user_id` (`user_id`),
  KEY `cvd_issues_address_id` (`address_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `issues`
--

LOCK TABLES `issues` WRITE;
/*!40000 ALTER TABLE `issues` DISABLE KEYS */;
/*!40000 ALTER TABLE `issues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `license`
--

DROP TABLE IF EXISTS `license`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `license` (
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `timestamp` int(11) NOT NULL DEFAULT '0',
  `has_project` smallint(3) DEFAULT NULL,
  `has_faq` smallint(3) DEFAULT NULL,
  `has_forum` smallint(3) DEFAULT NULL,
  `has_issues` smallint(3) DEFAULT NULL,
  `has_chat` smallint(3) DEFAULT NULL,
  `has_announcements` smallint(3) DEFAULT NULL,
  `has_enquete` smallint(3) DEFAULT NULL,
  `has_emagazine` smallint(3) DEFAULT NULL,
  `has_finance` smallint(3) DEFAULT NULL,
  `has_external` smallint(3) DEFAULT NULL,
  `has_snack` smallint(3) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `has_snelstart` smallint(3) DEFAULT NULL,
  `plain` smallint(3) DEFAULT NULL,
  `latest_version` varchar(255) DEFAULT '0',
  `has_multivers` smallint(3) DEFAULT NULL,
  `multivers_path` varchar(255) DEFAULT NULL,
  `mail_interval` int(11) DEFAULT NULL,
  `has_salariscom` smallint(3) DEFAULT '1',
  `multivers_update` int(11) DEFAULT NULL,
  `has_hrm` smallint(3) DEFAULT NULL,
  `has_exact` smallint(3) DEFAULT '0',
  `finance_start_date` int(11) DEFAULT NULL,
  `max_upload_size` varchar(255) DEFAULT '24M',
  `has_e4l` smallint(3) DEFAULT '0',
  `dayquote` int(11) DEFAULT NULL,
  `dayquote_nr` text,
  `mail_shell` smallint(3) DEFAULT '0',
  `has_voip` smallint(3) DEFAULT '0',
  `has_sales` smallint(3) DEFAULT NULL,
  `filesyspath` varchar(255) DEFAULT NULL,
  `has_arbo` int(11) DEFAULT NULL,
  `disable_basics` int(11) DEFAULT NULL,
  `has_privoxy_config` int(11) DEFAULT NULL,
  `has_hypo` smallint(3) DEFAULT NULL,
  `mail_migrated` tinyint(3) NOT NULL DEFAULT '0',
  `has_cms` tinyint(3) DEFAULT NULL,
  `has_project_ext_samba` tinyint(3) DEFAULT NULL,
  `mail_force_server` varchar(255) DEFAULT NULL,
  `mail_lock_settings` tinyint(3) DEFAULT NULL,
  `has_project_ext` tinyint(3) DEFAULT NULL,
  `force_ssl` smallint(3) DEFAULT NULL,
  `default_lang` char(3) DEFAULT 'EN',
  `autopatcher_enable` tinyint(3) NOT NULL,
  `autopatcher_lastpatch` varchar(255) NOT NULL,
  `has_project_declaration` tinyint(3) DEFAULT NULL,
  `has_funambol` tinyint(3) NOT NULL,
  `address_strict_permissions` tinyint(3) DEFAULT NULL,
  `cms_lock_settings` tinyint(3) DEFAULT NULL,
  `address_migrated` tinyint(3) NOT NULL,
  `project_ext_share` varchar(255) DEFAULT NULL,
  `filesystem_checked` varchar(10) DEFAULT NULL,
  `disable_local_gzip` tinyint(3) NOT NULL,
  `has_radius` tinyint(2) DEFAULT '0',
  `funambol_server_version` int(11) NOT NULL DEFAULT '300',
  `enable_filestore_gzip` tinyint(3) NOT NULL,
  `use_project_global_reghour` tinyint(3) NOT NULL,
  `has_factuur` tinyint(3) NOT NULL,
  `postfixdsn` varchar(255) DEFAULT NULL,
  `has_postfixadmin` tinyint(2) DEFAULT NULL,
  `filesys_quota` varchar(255) NOT NULL,
  `has_campaign` tinyint(3) NOT NULL,
  `google_map_key` varchar(255) NOT NULL,
  `single_login` tinyint(2) DEFAULT '0',
  `calendar_migrated` int(11) DEFAULT '0',
  `has_onlineusers` tinyint(3) DEFAULT '1',
  `alt_logo` varchar(255) DEFAULT NULL,
  `concurrent_users` int(11) DEFAULT '0',
  `strict_project_permissions` smallint(2) DEFAULT '1',
  `has_exactonline` smallint(2) DEFAULT '0',
  `has_membersarea` smallint(2) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `license`
--

LOCK TABLES `license` WRITE;
/*!40000 ALTER TABLE `license` DISABLE KEYS */;
INSERT INTO `license` VALUES ('covide','covide',1045522860,1,1,1,1,1,1,0,1,0,0,5,'',0,0,'2.4.1',0,'',0,0,0,1,0,0,'24M',0,1300748400,'Tempt not a desperate man.\n		-- William Shakespeare, \"Romeo and Juliet\"',1,0,1,'',0,0,0,0,2,0,0,'',0,1,0,'EN',1,'2010062900',0,0,0,0,3,'','R',0,0,600,0,0,0,'',0,'',1,'',0,1,1,NULL,0,1,0,0);
/*!40000 ALTER TABLE `license` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_current`
--

DROP TABLE IF EXISTS `login_current`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_current` (
  `user_id` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  KEY `user_id` (`user_id`),
  KEY `time` (`time`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_current`
--

LOCK TABLES `login_current` WRITE;
/*!40000 ALTER TABLE `login_current` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_current` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_log`
--

DROP TABLE IF EXISTS `login_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(255) DEFAULT NULL,
  `time` int(11) NOT NULL DEFAULT '0',
  `day` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_log`
--

LOCK TABLES `login_log` WRITE;
/*!40000 ALTER TABLE `login_log` DISABLE KEYS */;
INSERT INTO `login_log` VALUES (1,2,'127.0.0.1',1300400676,1300316400),(2,2,'10.0.1.101',1300402993,1300402800),(3,2,'10.0.1.101',1300403441,1300402800),(4,2,'10.0.1.101',1300404963,1300402800),(5,2,'10.0.1.101',1300405858,1300402800),(6,2,'10.0.1.101',1300795641,1300748400),(7,1,'10.0.1.101',1300797611,1300748400),(8,2,'10.0.1.101',1300798013,1300748400),(9,2,'10.0.1.101',1300798412,1300748400),(10,2,'10.0.1.101',1300799385,1300748400),(11,2,'10.0.1.101',1300799901,1300748400);
/*!40000 ALTER TABLE `login_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_attachments`
--

DROP TABLE IF EXISTS `mail_attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `temp_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(255) DEFAULT 'application/octet-stream',
  `size` varchar(255) DEFAULT '0',
  `cid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cvd_mail_attachments_message_id` (`message_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_attachments`
--

LOCK TABLES `mail_attachments` WRITE;
/*!40000 ALTER TABLE `mail_attachments` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_attachments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_autoreply`
--

DROP TABLE IF EXISTS `mail_autoreply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_autoreply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `timestamp_start` int(11) DEFAULT NULL,
  `timestamp_end` int(11) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` text,
  `notified` text,
  `is_active` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_autoreply`
--

LOCK TABLES `mail_autoreply` WRITE;
/*!40000 ALTER TABLE `mail_autoreply` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_autoreply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_filters`
--

DROP TABLE IF EXISTS `mail_filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `sender` varchar(255) DEFAULT NULL,
  `recipient` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `to_mapid` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cvd_mail_filters_user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_filters`
--

LOCK TABLES `mail_filters` WRITE;
/*!40000 ALTER TABLE `mail_filters` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_filters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_folders`
--

DROP TABLE IF EXISTS `mail_folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_folders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cvd_mail_folders_user_id` (`user_id`),
  KEY `cvd_mail_folders_parent_id` (`parent_id`)
) ENGINE=MyISAM AUTO_INCREMENT=130 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_folders`
--

LOCK TABLES `mail_folders` WRITE;
/*!40000 ALTER TABLE `mail_folders` DISABLE KEYS */;
INSERT INTO `mail_folders` VALUES (4,'Archief',NULL,NULL),(5,'Postvak-IN',1,NULL),(6,'Verzonden-Items',1,NULL),(7,'Verwijderde-Items',1,NULL),(121,'Bounced berichten',1,NULL),(122,'Concepten',1,NULL),(123,'Concepten',2,NULL),(124,'Bounced berichten',2,NULL),(125,'Postvak-IN',2,NULL),(126,'Verzonden-Items',2,NULL),(127,'Verwijderde-Items',2,NULL),(128,'Sent-Items',3,NULL),(129,'Concepten',3,NULL);
/*!40000 ALTER TABLE `mail_folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_messages`
--

DROP TABLE IF EXISTS `mail_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` varchar(255) DEFAULT NULL,
  `folder_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) DEFAULT NULL,
  `private_id` int(11) NOT NULL DEFAULT '0',
  `sender` varchar(255) DEFAULT NULL,
  `subject` mediumtext,
  `header` mediumtext,
  `body` mediumtext,
  `date` varchar(255) DEFAULT NULL,
  `is_text` smallint(3) DEFAULT NULL,
  `is_public` smallint(3) DEFAULT NULL,
  `sender_emailaddress` varchar(255) DEFAULT NULL,
  `to` mediumtext,
  `cc` mediumtext,
  `description` mediumtext,
  `is_new` smallint(3) NOT NULL DEFAULT '0',
  `replyto` varchar(255) DEFAULT NULL,
  `status_pop` smallint(3) NOT NULL DEFAULT '0',
  `bcc` mediumtext,
  `date_received` int(11) NOT NULL DEFAULT '0',
  `template_id` int(11) NOT NULL DEFAULT '0',
  `askwichrel` smallint(3) DEFAULT '0',
  `indexed` int(11) DEFAULT NULL,
  `options` text,
  `bcard_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `flag_indexed` (`indexed`),
  KEY `cvd_mail_messages_folder_id` (`folder_id`),
  KEY `cvd_mail_messages_user_id` (`user_id`),
  KEY `cvd_mail_messages_address_id` (`address_id`),
  KEY `cvd_mail_messages_project_id` (`project_id`),
  KEY `cvd_mail_messages_message_id` (`message_id`),
  KEY `cvd_mail_messages_date` (`date`),
  KEY `cvd_mail_messages_date_received` (`date_received`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_messages`
--

LOCK TABLES `mail_messages` WRITE;
/*!40000 ALTER TABLE `mail_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_messages_data`
--

DROP TABLE IF EXISTS `mail_messages_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_messages_data` (
  `mail_id` int(11) NOT NULL DEFAULT '0',
  `body` longtext NOT NULL,
  `header` mediumtext NOT NULL,
  `mail_decoding` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`mail_id`),
  KEY `cvd_mail_messages_data_mail_id` (`mail_id`),
  KEY `body` (`body`(255)),
  KEY `header` (`header`(255))
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_messages_data`
--

LOCK TABLES `mail_messages_data` WRITE;
/*!40000 ALTER TABLE `mail_messages_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_messages_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_messages_data_archive`
--

DROP TABLE IF EXISTS `mail_messages_data_archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_messages_data_archive` (
  `mail_id` int(11) NOT NULL DEFAULT '0',
  `body` longtext NOT NULL,
  `header` mediumtext NOT NULL,
  `mail_decoding` varchar(255) NOT NULL,
  PRIMARY KEY (`mail_id`),
  KEY `cvd_mail_messages_archive_body` (`body`(255)),
  KEY `cvd_mail_messages_archive_header` (`header`(255))
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_messages_data_archive`
--

LOCK TABLES `mail_messages_data_archive` WRITE;
/*!40000 ALTER TABLE `mail_messages_data_archive` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_messages_data_archive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_permissions`
--

DROP TABLE IF EXISTS `mail_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `folder_id` int(11) NOT NULL,
  `users` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_permissions`
--

LOCK TABLES `mail_permissions` WRITE;
/*!40000 ALTER TABLE `mail_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_projects`
--

DROP TABLE IF EXISTS `mail_projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_projects` (
  `message_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_projects`
--

LOCK TABLES `mail_projects` WRITE;
/*!40000 ALTER TABLE `mail_projects` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_signatures`
--

DROP TABLE IF EXISTS `mail_signatures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_signatures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `signature` mediumtext,
  `subject` varchar(255) DEFAULT NULL,
  `realname` varchar(255) DEFAULT NULL,
  `companyname` varchar(255) DEFAULT NULL,
  `signature_html` text NOT NULL,
  `default` tinyint(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cvd_mail_signatures_user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_signatures`
--

LOCK TABLES `mail_signatures` WRITE;
/*!40000 ALTER TABLE `mail_signatures` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_signatures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_templates`
--

DROP TABLE IF EXISTS `mail_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header` mediumtext NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `width` varchar(255) NOT NULL DEFAULT '800',
  `repeat` smallint(3) NOT NULL DEFAULT '1',
  `footer` mediumtext,
  `use_complex_mode` tinyint(3) NOT NULL,
  `html_data` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_templates`
--

LOCK TABLES `mail_templates` WRITE;
/*!40000 ALTER TABLE `mail_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_templates_files`
--

DROP TABLE IF EXISTS `mail_templates_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_templates_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `temp_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(255) DEFAULT 'application/octet-stream',
  `size` varchar(255) DEFAULT '0',
  `position` char(1) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `cvd_mail_templates_files_template_id` (`template_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_templates_files`
--

LOCK TABLES `mail_templates_files` WRITE;
/*!40000 ALTER TABLE `mail_templates_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_templates_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_tracking`
--

DROP TABLE IF EXISTS `mail_tracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_tracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `timestamp_first` int(11) DEFAULT NULL,
  `timestamp_last` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `mail_id_2` int(11) DEFAULT NULL,
  `clients` mediumtext,
  `agents` mediumtext,
  `mailcode` varchar(255) DEFAULT NULL,
  `hyperlinks` mediumtext,
  `is_sent` smallint(6) DEFAULT NULL,
  `campaign_id` int(11) NOT NULL,
  `address_type` varchar(255) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cvd_mail_tracking_mail_id` (`mail_id`),
  KEY `cvd_mail_tracking_email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_tracking`
--

LOCK TABLES `mail_tracking` WRITE;
/*!40000 ALTER TABLE `mail_tracking` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_tracking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_unsubscription`
--

DROP TABLE IF EXISTS `mail_unsubscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_unsubscription` (
  `email` varchar(255) NOT NULL,
  `hashcode` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_unsubscription`
--

LOCK TABLES `mail_unsubscription` WRITE;
/*!40000 ALTER TABLE `mail_unsubscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_unsubscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_global`
--

DROP TABLE IF EXISTS `meta_global`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta_global` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meta_id` int(11) NOT NULL,
  `relation_id` int(11) NOT NULL,
  `value` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_global`
--

LOCK TABLES `meta_global` WRITE;
/*!40000 ALTER TABLE `meta_global` DISABLE KEYS */;
/*!40000 ALTER TABLE `meta_global` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_groups`
--

DROP TABLE IF EXISTS `meta_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_groups`
--

LOCK TABLES `meta_groups` WRITE;
/*!40000 ALTER TABLE `meta_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `meta_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_table`
--

DROP TABLE IF EXISTS `meta_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tablename` varchar(255) DEFAULT NULL,
  `fieldname` varchar(255) DEFAULT NULL,
  `fieldtype` int(11) DEFAULT NULL,
  `fieldorder` int(11) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `value` mediumtext,
  `group_id` int(11) DEFAULT NULL,
  `default_value` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cvd_meta_table_record_id` (`record_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_table`
--

LOCK TABLES `meta_table` WRITE;
/*!40000 ALTER TABLE `meta_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `meta_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `morgage`
--

DROP TABLE IF EXISTS `morgage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `morgage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` int(11) DEFAULT NULL,
  `total_sum` decimal(16,2) DEFAULT NULL,
  `investor` int(11) DEFAULT NULL,
  `insurancer` int(11) DEFAULT NULL,
  `year_payement` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_src` int(11) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  `type` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `is_active` smallint(3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cvd_morgage_address_id` (`address_id`),
  KEY `cvd_morgage_user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `morgage`
--

LOCK TABLES `morgage` WRITE;
/*!40000 ALTER TABLE `morgage` DISABLE KEYS */;
/*!40000 ALTER TABLE `morgage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notes`
--

DROP TABLE IF EXISTS `notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` int(11) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` mediumtext,
  `sender` int(11) DEFAULT NULL,
  `is_read` smallint(3) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `is_done` smallint(3) DEFAULT NULL,
  `delstatus` smallint(3) NOT NULL DEFAULT '0',
  `project_id` smallint(3) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `campaign_id` int(11) NOT NULL,
  `is_support` smallint(3) DEFAULT '0',
  `extra_recipients` varchar(255) DEFAULT NULL,
  `is_draft` int(11) DEFAULT '0',
  `phonecall` smallint(3) DEFAULT NULL,
  `phone_nr` varchar(255) DEFAULT NULL,
  `address_businesscards_id` int(11) DEFAULT NULL,
  `phonecall_start` int(11) DEFAULT NULL,
  `phonecall_end` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cvd_notes_user_id` (`user_id`),
  KEY `cvd_notes_sender` (`sender`),
  KEY `cvd_notes_timestamp` (`timestamp`),
  KEY `cvd_notes_project_id` (`project_id`),
  KEY `cvd_notes_address_id` (`address_id`),
  KEY `cvd_notes_delstatus` (`delstatus`),
  KEY `cvd_notes_is_done` (`is_done`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notes`
--

LOCK TABLES `notes` WRITE;
/*!40000 ALTER TABLE `notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payroll_exports`
--

DROP TABLE IF EXISTS `payroll_exports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payroll_exports` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(11) unsigned DEFAULT NULL,
  `timestamp_start` int(11) unsigned DEFAULT NULL,
  `timestamp_end` int(11) unsigned DEFAULT NULL,
  `km_items` text,
  `registration_items` text,
  `csv_data` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payroll_exports`
--

LOCK TABLES `payroll_exports` WRITE;
/*!40000 ALTER TABLE `payroll_exports` DISABLE KEYS */;
/*!40000 ALTER TABLE `payroll_exports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poll_answers`
--

DROP TABLE IF EXISTS `poll_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poll_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poll_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `answer` smallint(3) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poll_answers`
--

LOCK TABLES `poll_answers` WRITE;
/*!40000 ALTER TABLE `poll_answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `poll_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poll_items`
--

DROP TABLE IF EXISTS `poll_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poll_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `polls_id` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poll_items`
--

LOCK TABLES `poll_items` WRITE;
/*!40000 ALTER TABLE `poll_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `poll_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `polls`
--

DROP TABLE IF EXISTS `polls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `polls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` mediumtext,
  `is_active` smallint(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `polls`
--

LOCK TABLES `polls` WRITE;
/*!40000 ALTER TABLE `polls` DISABLE KEYS */;
/*!40000 ALTER TABLE `polls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  `manager` int(11) DEFAULT NULL,
  `group_id` smallint(3) NOT NULL DEFAULT '0',
  `is_active` smallint(3) DEFAULT NULL,
  `status` smallint(3) DEFAULT '0',
  `address_id` int(11) DEFAULT NULL,
  `lfact` int(11) DEFAULT NULL,
  `budget` int(11) DEFAULT NULL,
  `hours` int(11) DEFAULT NULL,
  `address_businesscard_id` int(11) DEFAULT NULL,
  `multirel` varchar(255) DEFAULT NULL,
  `users` varchar(255) DEFAULT NULL,
  `executor` int(11) DEFAULT '0',
  `kilometer_allowance` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cvd_project_address_id` (`address_id`),
  KEY `cvd_project_group_id` (`group_id`),
  KEY `cvd_project_is_active` (`is_active`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (3,'covide',NULL,2,1,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0);
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_costs`
--

DROP TABLE IF EXISTS `project_costs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_costs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cost` varchar(255) DEFAULT NULL,
  `tarif` decimal(16,2) DEFAULT NULL,
  `department_id` int(11) DEFAULT '0',
  `purchase` decimal(16,2) DEFAULT NULL,
  `marge` decimal(16,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_costs`
--

LOCK TABLES `project_costs` WRITE;
/*!40000 ALTER TABLE `project_costs` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_costs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_declaration_batchcounter`
--

DROP TABLE IF EXISTS `projects_declaration_batchcounter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_declaration_batchcounter` (
  `year` int(11) DEFAULT NULL,
  `batchcounter` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_declaration_batchcounter`
--

LOCK TABLES `projects_declaration_batchcounter` WRITE;
/*!40000 ALTER TABLE `projects_declaration_batchcounter` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_declaration_batchcounter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_declaration_extrainfo`
--

DROP TABLE IF EXISTS `projects_declaration_extrainfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_declaration_extrainfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `task_date` int(11) NOT NULL,
  `damage_date` int(11) NOT NULL,
  `accident_type` int(11) NOT NULL,
  `perc_liabilities_wished` float(16,2) NOT NULL,
  `perc_liabilities_recognised` float(16,2) NOT NULL,
  `constituent` int(11) NOT NULL,
  `tarif` int(11) NOT NULL,
  `is_NCNP` int(11) NOT NULL,
  `perc_NCNP` float(16,2) NOT NULL,
  `client` int(11) NOT NULL,
  `adversary` varchar(255) NOT NULL,
  `expertise` varchar(255) NOT NULL,
  `lesion` int(11) NOT NULL,
  `lesion_description` text NOT NULL,
  `hospitalisation` int(11) NOT NULL,
  `incapacity_for_work` int(11) NOT NULL,
  `profession` int(11) NOT NULL,
  `employment` varchar(255) NOT NULL,
  `identifier_adversary` varchar(255) NOT NULL,
  `identifier_expertise` varchar(255) NOT NULL,
  `agreements` text NOT NULL,
  `bcard_constituent` int(11) NOT NULL,
  `bcard_client` int(11) NOT NULL,
  `bcard_adversary` int(11) NOT NULL,
  `bcard_expertise` int(11) NOT NULL,
  `default_tarif` float(16,2) DEFAULT NULL,
  `identifier` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_declaration_extrainfo`
--

LOCK TABLES `projects_declaration_extrainfo` WRITE;
/*!40000 ALTER TABLE `projects_declaration_extrainfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_declaration_extrainfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_declaration_options`
--

DROP TABLE IF EXISTS `projects_declaration_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_declaration_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_declaration_options`
--

LOCK TABLES `projects_declaration_options` WRITE;
/*!40000 ALTER TABLE `projects_declaration_options` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_declaration_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_declaration_registration`
--

DROP TABLE IF EXISTS `projects_declaration_registration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_declaration_registration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `hour_tarif` float(16,2) NOT NULL,
  `declaration_type` int(11) NOT NULL,
  `perc_btw` int(11) NOT NULL,
  `price` float(16,2) NOT NULL,
  `description` text NOT NULL,
  `is_invoice` tinyint(3) NOT NULL,
  `is_paid` tinyint(3) NOT NULL,
  `batch_nr` int(11) NOT NULL,
  `user_id_input` int(11) NOT NULL,
  `time_units` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `kilometers` int(11) NOT NULL,
  `perc_NCNP` float(16,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_declaration_registration`
--

LOCK TABLES `projects_declaration_registration` WRITE;
/*!40000 ALTER TABLE `projects_declaration_registration` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_declaration_registration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_ext_activities`
--

DROP TABLE IF EXISTS `projects_ext_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_ext_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) NOT NULL,
  `activity` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `users` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_ext_activities`
--

LOCK TABLES `projects_ext_activities` WRITE;
/*!40000 ALTER TABLE `projects_ext_activities` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_ext_activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_ext_departments`
--

DROP TABLE IF EXISTS `projects_ext_departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_ext_departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `address_id` int(11) NOT NULL,
  `users` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_ext_departments`
--

LOCK TABLES `projects_ext_departments` WRITE;
/*!40000 ALTER TABLE `projects_ext_departments` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_ext_departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_ext_extrainfo`
--

DROP TABLE IF EXISTS `projects_ext_extrainfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_ext_extrainfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `project_year` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_ext_extrainfo`
--

LOCK TABLES `projects_ext_extrainfo` WRITE;
/*!40000 ALTER TABLE `projects_ext_extrainfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_ext_extrainfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_ext_metafields`
--

DROP TABLE IF EXISTS `projects_ext_metafields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_ext_metafields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(255) NOT NULL,
  `field_type` smallint(3) NOT NULL,
  `field_order` int(11) NOT NULL,
  `activity` int(11) NOT NULL,
  `show_list` tinyint(3) NOT NULL,
  `default_value` mediumtext,
  `large_data` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_ext_metafields`
--

LOCK TABLES `projects_ext_metafields` WRITE;
/*!40000 ALTER TABLE `projects_ext_metafields` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_ext_metafields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_ext_metavalues`
--

DROP TABLE IF EXISTS `projects_ext_metavalues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_ext_metavalues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `meta_id` int(11) NOT NULL,
  `meta_value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_ext_metavalues`
--

LOCK TABLES `projects_ext_metavalues` WRITE;
/*!40000 ALTER TABLE `projects_ext_metavalues` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_ext_metavalues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_master`
--

DROP TABLE IF EXISTS `projects_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  `manager` int(11) DEFAULT NULL,
  `is_active` smallint(3) DEFAULT NULL,
  `status` smallint(3) DEFAULT '0',
  `address_id` int(11) DEFAULT NULL,
  `address_businesscard_id` int(11) DEFAULT NULL,
  `users` varchar(255) DEFAULT NULL,
  `ext_department` int(11) DEFAULT NULL,
  `executor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_master`
--

LOCK TABLES `projects_master` WRITE;
/*!40000 ALTER TABLE `projects_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `radius_settings`
--

DROP TABLE IF EXISTS `radius_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `radius_settings` (
  `radius_server` varchar(255) DEFAULT NULL,
  `radius_port` int(11) DEFAULT NULL,
  `shared_secret` varchar(255) DEFAULT NULL,
  `nas_ip` varchar(255) DEFAULT NULL,
  `auth_type` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `radius_settings`
--

LOCK TABLES `radius_settings` WRITE;
/*!40000 ALTER TABLE `radius_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `radius_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rssfeeds`
--

DROP TABLE IF EXISTS `rssfeeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rssfeeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `homepage` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT '5',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rssfeeds`
--

LOCK TABLES `rssfeeds` WRITE;
/*!40000 ALTER TABLE `rssfeeds` DISABLE KEYS */;
INSERT INTO `rssfeeds` VALUES (1,'Laatste nieuws (Bron: NU.nl)','','http://www.nu.nl','feed://www.nu.nl/feeds/rss/algemeen.rss',0,5);
/*!40000 ALTER TABLE `rssfeeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rssitems`
--

DROP TABLE IF EXISTS `rssitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rssitems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feed` int(11) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `body` mediumtext,
  `link` varchar(255) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rssitems`
--

LOCK TABLES `rssitems` WRITE;
/*!40000 ALTER TABLE `rssitems` DISABLE KEYS */;
/*!40000 ALTER TABLE `rssitems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales`
--

DROP TABLE IF EXISTS `sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `expected_score` int(11) DEFAULT NULL,
  `total_sum` decimal(16,2) DEFAULT NULL,
  `timestamp_proposal` int(11) DEFAULT NULL,
  `timestamp_order` int(11) DEFAULT NULL,
  `timestamp_invoice` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `description` mediumtext,
  `is_active` int(11) DEFAULT NULL,
  `timestamp_prospect` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `user_sales_id` int(11) DEFAULT NULL,
  `user_id_create` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT '0',
  `classification` varchar(255) DEFAULT NULL,
  `users` varchar(255) DEFAULT NULL,
  `multirel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales`
--

LOCK TABLES `sales` WRITE;
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `session_id` varchar(255) NOT NULL DEFAULT '0',
  `session_data` longtext,
  `expire` int(1) unsigned NOT NULL DEFAULT '0',
  `user_agent` varchar(255) DEFAULT NULL,
  KEY `session_id` (`session_id`) USING BTREE,
  KEY `expire` (`expire`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_settings`
--

DROP TABLE IF EXISTS `sms_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_settings` (
  `companyid` varchar(255) DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `sender` varchar(255) DEFAULT NULL,
  `request_uri` varchar(255) NOT NULL,
  `default_prefix` varchar(10) NOT NULL,
  `trans` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_settings`
--

LOCK TABLES `sms_settings` WRITE;
/*!40000 ALTER TABLE `sms_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `snack_items`
--

DROP TABLE IF EXISTS `snack_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `snack_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `snack_items`
--

LOCK TABLES `snack_items` WRITE;
/*!40000 ALTER TABLE `snack_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `snack_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `snack_order`
--

DROP TABLE IF EXISTS `snack_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `snack_order` (
  `snack_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `snack_order`
--

LOCK TABLES `snack_order` WRITE;
/*!40000 ALTER TABLE `snack_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `snack_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statistics`
--

DROP TABLE IF EXISTS `statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statistics` (
  `table` varchar(255) DEFAULT NULL,
  `updates` int(11) DEFAULT NULL,
  `vacuum` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statistics`
--

LOCK TABLES `statistics` WRITE;
/*!40000 ALTER TABLE `statistics` DISABLE KEYS */;
/*!40000 ALTER TABLE `statistics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_conn`
--

DROP TABLE IF EXISTS `status_conn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_conn` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `timestamp` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_conn`
--

LOCK TABLES `status_conn` WRITE;
/*!40000 ALTER TABLE `status_conn` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_conn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_list`
--

DROP TABLE IF EXISTS `status_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_id` varchar(255) NOT NULL DEFAULT '0',
  `mail_id` int(11) NOT NULL DEFAULT '0',
  `timestamp` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `mark_delete` smallint(3) NOT NULL DEFAULT '0',
  `mark_expunge` tinyint(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_list`
--

LOCK TABLES `status_list` WRITE;
/*!40000 ALTER TABLE `status_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `status_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `support`
--

DROP TABLE IF EXISTS `support`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `support` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` int(11) DEFAULT NULL,
  `body` mediumtext,
  `type` int(11) DEFAULT NULL,
  `relation_name` mediumtext,
  `email` mediumtext,
  `reference_nr` varchar(255) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `support`
--

LOCK TABLES `support` WRITE;
/*!40000 ALTER TABLE `support` DISABLE KEYS */;
/*!40000 ALTER TABLE `support` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templates`
--

DROP TABLE IF EXISTS `templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address_businesscard_id` smallint(3) NOT NULL DEFAULT '0',
  `font` varchar(255) DEFAULT NULL,
  `fontsize` int(11) NOT NULL DEFAULT '0',
  `body` mediumtext,
  `footer` mediumtext,
  `sender` mediumtext,
  `address_id` int(11) NOT NULL DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  `classification` varchar(255) DEFAULT NULL,
  `ids` mediumtext,
  `header` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `settings_id` int(11) NOT NULL DEFAULT '0',
  `date` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `negative_classification` varchar(255) DEFAULT NULL,
  `multirel` varchar(255) DEFAULT NULL,
  `save_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `and_or` varchar(255) DEFAULT 'AND',
  `fax_nr` smallint(3) DEFAULT NULL,
  `signature` smallint(3) DEFAULT NULL,
  `finance` int(11) NOT NULL,
  `businesscard_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templates`
--

LOCK TABLES `templates` WRITE;
/*!40000 ALTER TABLE `templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templates_files`
--

DROP TABLE IF EXISTS `templates_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templates_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `temp_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(255) DEFAULT 'application/octet-stream',
  `size` varchar(255) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cvd_mail_templates_files_template_id` (`template_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templates_files`
--

LOCK TABLES `templates_files` WRITE;
/*!40000 ALTER TABLE `templates_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `templates_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templates_settings`
--

DROP TABLE IF EXISTS `templates_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templates_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_left` decimal(16,2) NOT NULL DEFAULT '0.00',
  `page_top` decimal(16,2) NOT NULL DEFAULT '0.00',
  `page_right` decimal(16,2) NOT NULL DEFAULT '0.00',
  `address_left` decimal(16,2) NOT NULL DEFAULT '0.00',
  `address_width` decimal(16,2) NOT NULL DEFAULT '0.00',
  `address_top` decimal(16,2) NOT NULL DEFAULT '0.00',
  `address_position` smallint(3) NOT NULL DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  `footer_position` varchar(10) NOT NULL,
  `footer_text` text,
  `logo_position` tinyint(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templates_settings`
--

LOCK TABLES `templates_settings` WRITE;
/*!40000 ALTER TABLE `templates_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `templates_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `todo`
--

DROP TABLE IF EXISTS `todo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `todo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `is_done` smallint(3) DEFAULT '0',
  `subject` varchar(255) DEFAULT NULL,
  `body` mediumtext,
  `address_id` int(11) DEFAULT NULL,
  `timestamp_end` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `is_alert` smallint(3) DEFAULT '0',
  `is_customercontact` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `todo`
--

LOCK TABLES `todo` WRITE;
/*!40000 ALTER TABLE `todo` DISABLE KEYS */;
/*!40000 ALTER TABLE `todo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `twinfield_settings`
--

DROP TABLE IF EXISTS `twinfield_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twinfield_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `default_office` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `twinfield_settings`
--

LOCK TABLES `twinfield_settings` WRITE;
/*!40000 ALTER TABLE `twinfield_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `twinfield_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_groups`
--

DROP TABLE IF EXISTS `user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `members` text,
  `manager` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_groups`
--

LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userchangelog`
--

DROP TABLE IF EXISTS `userchangelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userchangelog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manager` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `change` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userchangelog`
--

LOCK TABLES `userchangelog` WRITE;
/*!40000 ALTER TABLE `userchangelog` DISABLE KEYS */;
/*!40000 ALTER TABLE `userchangelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `pers_nr` int(11) DEFAULT NULL,
  `xs_usermanage` smallint(3) DEFAULT '0',
  `xs_addressmanage` smallint(3) DEFAULT NULL,
  `xs_projectmanage` smallint(3) DEFAULT NULL,
  `xs_forummanage` smallint(3) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `xs_pollmanage` smallint(3) DEFAULT NULL,
  `xs_faqmanage` smallint(3) DEFAULT NULL,
  `xs_issuemanage` smallint(3) DEFAULT NULL,
  `xs_chatmanage` smallint(3) DEFAULT NULL,
  `xs_turnovermanage` smallint(3) DEFAULT NULL,
  `xs_notemanage` smallint(3) DEFAULT NULL,
  `xs_todomanage` smallint(3) DEFAULT '0',
  `comment` mediumtext,
  `is_active` smallint(3) NOT NULL DEFAULT '1',
  `style` smallint(3) DEFAULT NULL,
  `mail_server` varchar(255) DEFAULT NULL,
  `mail_user_id` varchar(255) DEFAULT NULL,
  `mail_password` varchar(255) DEFAULT NULL,
  `mail_email` varchar(255) DEFAULT NULL,
  `mail_email1` varchar(255) DEFAULT NULL,
  `mail_html` smallint(3) DEFAULT NULL,
  `mail_signature` mediumtext,
  `mail_showcount` smallint(3) DEFAULT '0',
  `mail_deltime` int(11) DEFAULT NULL,
  `days` smallint(3) DEFAULT '0',
  `htmleditor` smallint(3) NOT NULL DEFAULT '2',
  `addressaccountmanage` varchar(255) DEFAULT NULL,
  `calendarselection` varchar(255) DEFAULT NULL,
  `showhelp` int(11) NOT NULL DEFAULT '0',
  `showpopup` smallint(3) DEFAULT '1',
  `xs_salariscommanage` smallint(3) DEFAULT '0',
  `mail_server_deltime` int(11) DEFAULT '1',
  `xs_companyinfomanage` smallint(3) DEFAULT '0',
  `xs_hrmmanage` smallint(3) DEFAULT '0',
  `language` char(2) NOT NULL DEFAULT 'NL',
  `employer_id` int(11) DEFAULT NULL,
  `automatic_logout` smallint(3) DEFAULT '0',
  `mail_view_textmail_only` smallint(3) DEFAULT NULL,
  `e4l_update` int(11) DEFAULT NULL,
  `dayquote` int(11) DEFAULT NULL,
  `infowin_altmethod` int(11) DEFAULT NULL,
  `xs_e4l` smallint(3) DEFAULT '0',
  `xs_filemanage` int(11) DEFAULT NULL,
  `xs_limitusermanage` int(11) DEFAULT NULL,
  `change_theme` int(11) DEFAULT NULL,
  `xs_relationmanage` int(11) DEFAULT NULL,
  `xs_newslettermanage` int(11) DEFAULT NULL,
  `renderstatus` mediumtext,
  `mail_forward` int(11) DEFAULT NULL,
  `showvoip` smallint(3) DEFAULT '0',
  `signature` varchar(255) DEFAULT NULL,
  `voip_device` varchar(255) DEFAULT NULL,
  `xs_salesmanage` smallint(3) DEFAULT NULL,
  `e4l_username` varchar(255) DEFAULT NULL,
  `e4l_password` varchar(255) DEFAULT NULL,
  `xs_arbo` int(11) DEFAULT NULL,
  `xs_arbo_validated` int(11) DEFAULT NULL,
  `alternative_note_view_desktop` int(11) DEFAULT NULL,
  `calendarmode` int(11) DEFAULT '1',
  `rssnews` int(11) DEFAULT NULL,
  `mail_showbcc` int(11) DEFAULT NULL,
  `mail_imap` int(11) DEFAULT NULL,
  `xs_hypo` smallint(3) DEFAULT NULL,
  `mail_num_items` int(11) DEFAULT NULL,
  `xs_cms_level` int(11) DEFAULT NULL,
  `xs_funambol` tinyint(3) NOT NULL,
  `mail_signature_html` text NOT NULL,
  `xs_funambol_expunge` tinyint(3) NOT NULL,
  `addresssyncmanage` varchar(255) NOT NULL,
  `mail_shortview` tinyint(3) NOT NULL,
  `mail_default_private` tinyint(3) NOT NULL,
  `authmethod` varchar(255) DEFAULT NULL,
  `xs_funambol_version` tinyint(3) NOT NULL,
  `addressmode` int(11) DEFAULT '0',
  `xs_classmanage` varchar(255) DEFAULT NULL,
  `calendarinterval` int(11) NOT NULL DEFAULT '15',
  `hour_format` int(11) DEFAULT NULL,
  `mail_default_bcc` varchar(255) DEFAULT NULL,
  `google_username` varchar(255) DEFAULT NULL,
  `google_password` varchar(255) DEFAULT NULL,
  `voip_number` varchar(50) DEFAULT NULL,
  `xs_campaignmanage` tinyint(3) NOT NULL,
  `font` varchar(255) DEFAULT NULL,
  `fontsize` int(11) DEFAULT NULL,
  `mail_default_template` int(11) DEFAULT NULL,
  `google_startingpoint` varchar(255) DEFAULT NULL,
  `mail_hide_cmsforms` tinyint(3) DEFAULT '0',
  `showbdays` tinyint(2) DEFAULT '1',
  `default_address_fields` mediumtext,
  `default_address_fields_bcard` mediumtext,
  `dimdim_username` varchar(255) DEFAULT NULL,
  `dimdim_password` varchar(255) DEFAULT NULL,
  `dashboardlayout` text,
  `classificationrestriction` int(11) DEFAULT '0',
  `popup_newwindow` smallint(2) DEFAULT NULL,
  `twitter_username` varchar(255) DEFAULT NULL,
  `twitter_password` varchar(255) DEFAULT NULL,
  `xs_limited_projectmanage` smallint(2) DEFAULT '0',
  `default_address_fields_private` mediumtext,
  `default_address_fields_users` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (3,'archiefgebruiker','',NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,2,NULL,NULL,0,1,0,1,0,0,'NL',0,0,NULL,0,NULL,NULL,0,1,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,0,'',0,'',0,0,NULL,0,0,NULL,15,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0,1,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,0,NULL,NULL),(1,'administrator','0192023a7bbd73250516f069df18b500',2,1,1,1,0,0,0,0,1,NULL,0,1,1,NULL,1,0,'','','','','',0,NULL,0,1,23,2,'','',0,1,0,1,0,1,'EN',0,1,0,0,0,0,0,1,1,0,1,0,'',NULL,0,NULL,'',1,NULL,NULL,0,NULL,0,1,0,NULL,0,0,20,NULL,0,'',0,'',0,0,'database',0,0,'1',15,0,'','','','',1,'0',0,0,'',0,1,NULL,NULL,'','',NULL,0,0,'','',0,NULL,NULL),(2,'wombat','5858ea228cc2edf88721699b2c8638e5',1,1,1,1,0,2,0,0,1,NULL,0,1,1,NULL,1,0,'','','','','',0,'',0,2674801,24,2,'','',0,1,0,601201,0,1,'NL',0,1,0,0,1,0,0,1,1,0,1,0,'',0,0,NULL,'',1,NULL,NULL,0,NULL,0,1,0,NULL,0,0,20,3,0,'',0,'',0,0,'database',0,0,'1',15,0,'','','','',1,'0',0,0,'',0,1,NULL,NULL,'','',NULL,0,0,'','',0,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-03-22 14:37:18
