DROP TABLE IF EXISTS `campaign_response`;
CREATE TABLE `campaign_response` (
  `campaign_id` int(11) unsigned NOT NULL,
  `id` int(11) unsigned NOT NULL auto_increment,
  `response` varchar(80) NOT NULL,
  `id_response` int(4) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
);

DROP TABLE IF EXISTS `campaign_reason`;
CREATE TABLE `campaign_reason` (
  `campaign_id` int(11) unsigned NOT NULL,
  `id` int(11) unsigned NOT NULL auto_increment,
  `reason` varchar(80) NOT NULL,
  `response_id` int(2) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
);

ALTER TABLE campaign_records ADD (
`general_response` int(11) unsigned default NULL,
  `reason` varchar(255) default NULL);
