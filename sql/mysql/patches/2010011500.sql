ALTER TABLE `todo` ADD `sales_id` INT NOT NULL, ADD `is_sales_todo` SMALLINT( 3 ) NOT NULL ;
ALTER TABLE `sales` ADD `status` INT NOT NULL, ADD `proposal_file` INT NULL , ADD `invoice_file` INT NULL, ADD `timestamp_action` INT( 11 ) NOT NULL;
ALTER TABLE `users` ADD `xs_limited_salesmanage` SMALLINT( 3 ) NOT NULL , ADD `parent_sales_manager` INT( 11 ) NOT NULL, ADD `default_sales_fields` MEDIUMTEXT NOT NULL;

CREATE TABLE `sales_search` (
`search_id` INT( 11 ) NOT NULL AUTO_INCREMENT,
`user_id` INT( 11 ) NOT NULL ,
`from_timestamp` INT( 11 ) NOT NULL ,
`till_timestamp` INT( 11 ) NOT NULL ,
`search_user_id` INT( 11 ) NOT NULL ,
`search_key` TEXT NOT NULL ,
`address_id` INT( 11 ) NOT NULL ,
`project_id` INT( 11 ) NOT NULL ,
`classification` INT( 11 ) NOT NULL ,
PRIMARY KEY ( `search_id` )
) ENGINE = MYISAM;
ALTER TABLE license ADD COLUMN strict_project_permissions smallint(2) DEFAULT 1;
