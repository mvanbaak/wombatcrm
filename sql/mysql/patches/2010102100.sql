CREATE TABLE `actionqueue` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`user_id` INT NOT NULL ,
`action` INT NOT NULL ,
`mintime` VARCHAR( 15 ) NOT NULL ,
`data` TEXT NOT NULL ,
`state` INT NOT NULL ,
`msg` TEXT NOT NULL ,
`dependson` INT NOT NULL
)
