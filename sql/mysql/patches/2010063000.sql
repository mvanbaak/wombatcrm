ALTER TABLE `sales_search` ADD `xaxis` INT NOT NULL DEFAULT '0', ADD `yaxis` VARCHAR( 10 ) NOT NULL DEFAULT '0', ADD `inactive_flag` INT NOT NULL DEFAULT '0';
ALTER TABLE `sales_search` CHANGE `search_user_id` `search_user_id` VARCHAR( 255 ) NOT NULL;
